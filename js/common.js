
// 선택한 타이틀 이미지 파일명을 해당 요소에 입력
function title_select(obj){
  var selected_title = obj.getAttribute('file_name');
  // console.log(selected_title);
  $('#selected_title').val(selected_title) ;
}

// 메인 주제 및 하위 주제 체크 박스 체크 내용 반영
function subject_check(obj){
  var is_activate = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인
  var subject_code = $(obj).attr('id');
  var main_subject_code = document.getElementById('main_subject_code').value;
 
  // var host = location.host ; 
  // console.log(is_activate +','+ subject_code + ',' + host);
  // 실시간 처리를 위한 uri redirect (필요한 변수를 get방식으로 보낸다.)
  // window.location.href = '/setting/subjectcheck'+'?maincode='+main_subject_code+'&subject='+subject_code+'&activate='+is_activate;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/subjectcheck',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      main_subject_code : main_subject_code,
      subject_code : subject_code,
      is_activate : is_activate

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var main_subject_code = result_array.main_subject_code;
      console.log(main_subject_code);


    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 메인 주제 선택시 하위 주제를 가져와 리스트에 담는다.
function selected_subject(obj){
  var selected_subject = obj.outerText; // label 요소 텍스트 값 가져오기
  var selected_subject = obj.getAttribute('code');
  // console.log(selected_subject);

  // window.location.href = '/setting/getsubsubject'+'?subject='+selected_subject;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/getsubsubject',   //정보 처리를 할 서버 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      selected_subject : selected_subject
    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var sub_subject = data.extra;
      // console.log(sub_subject);

      //메인 주제 이름이 담긴 배열을 꺼낸다. (마지막 배열 제거 및 리턴)
      var main_subject_name_array = sub_subject.pop();
      var main_subject_name = main_subject_name_array.main_subject_name;
      // console.log('배열값:'+main_subject_name);
      if(sub_subject.length == 0) {
        alert('하위 주제가 없습니다.');
        $("#sub_subject_list").empty();
        $("#sub_subject_list").append('<span class="text" style="color:gray;">메인 주제를 선택해주세요.</span>');
      } else {
        $("#sub_subject_list").empty();
        for(i=0; i < sub_subject.length; i++) {
          var sub_subject_name = sub_subject[i].sub_subject_name;
          var sub_subject_code = sub_subject[i].sub_subject_code;
          var is_activate = sub_subject[i].is_activate;
         
          if(is_activate == 'Y') {
            $("#sub_subject_list").append('<li class="list-group-item border-bottom subject" style="padding:.2rem 1rem; ">'
                                          +  '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'
                                          +     '<div class="flex-grow-1">'
                                          +         '<label class="form-check-label w-100" for="" onclick="" code="'+ sub_subject_code +'" >'+ sub_subject_name +'</label>'
                                          +     '</div>'
                                          +     '<input class="form-check-input" type="checkbox" id="'+ sub_subject_code +'" checked onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;">'
                                          +   '</div>'
                                          +'</li>'                         
            );
          } else {
            $("#sub_subject_list").append('<li class="list-group-item border-bottom subject" style="padding:.2rem 1rem; ">'
                                          +  '<div class="form-check form-switch d-flex justify-content-between align-items-center p-0">'
                                          +     '<div class="flex-grow-1">'
                                          +         '<label class="form-check-label w-100" for="" onclick="" code="'+ sub_subject_code +'" >'+ sub_subject_name +'</label>'
                                          +     '</div>'
                                          +     '<input class="form-check-input" type="checkbox" id="'+ sub_subject_code +'" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;">'
                                          +   '</div>'
                                          +'</li>'
            );
          }
        }
      }

      // 선택한 메인 주제 코드를 넣어준다.
      $('#main_subject_code').val(selected_subject);

      // 선택한 메인 주제 이름을 넣어준다.
      $('#main_subject_name').val(main_subject_name);

      // 선택한 메인 주제의 배경색 처리를 해준다.
      $('#main_subject_list').find('li').each(function() {
        var li_name = $(this).attr('name');
        // console.log(li_name+','+main_subject_name);
        if(li_name == main_subject_name) {
          $(this).css('backgroundColor','#f8f9fa');
        } else {
          $(this).css('backgroundColor','');
        }

      });

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 계정 상태를 변경한다.
function id_status_change(obj) {
  var status = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인
  if (status == 'Y') {
    status = 'O'; // 정상
  } else {
    status = 'B'; // 중지
  }
  var id = $(obj).attr('id');
 
  // var host = location.host ; 
  // console.log(is_activate +','+ subject_code + ',' + host);
  // 실시간 처리를 위한 uri redirect (필요한 변수를 get방식으로 보낸다.)
  // window.location.href = '/setting/subjectcheck'+'?maincode='+main_subject_code+'&subject='+subject_code+'&activate='+is_activate;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/accountmanage/statusupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      id : id,
      status : status

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var admin_status = result_array.admin_status;
      console.log(id+' : '+admin_status);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 프로필 이미지 변경 요소를 토글한다.
function profile_change(obj) {
  var admin_id = obj.getAttribute('id');
  $('#'+admin_id+'_profile_change').slideToggle('fast');
}

// 계정 정보를 업데이트 한다.
function account_update(obj){
  
  var admin_id = obj.getAttribute('admin_id');
  var nickname = $('div[admin_id='+admin_id+']').find('input').eq(0).val();
  var level = $('div[admin_id='+admin_id+']').find('input').eq(1).val();
  // console.log('['+admin_id+'] nickname : '+nickname+', level : '+level);
 
  if( confirm('['+admin_id+']님의 정보를 업데이트 하시겠습니까?') == false){
    return;
  }

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/accountmanage/accountupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)
      admin_id : admin_id,
      nickname : nickname,
      level : level

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var new_nickname = result_array.nickname;
      var new_level = result_array.level;
      // console.log(new_nickname+' : '+new_level);

      alert('['+admin_id+']님의 계정 정보가 업데이트 되었습니다 \r'
            +' - nickname => '+new_nickname+' \r'
            +' - level    => '+new_level
      );

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// nickname 중복 여부를 확인한다.
function is_nickname() {
  var nickname = $('#nickname').val();
  // console.log(nickname);
  if (nickname == '') {
    alert('닉네임을 입력해주세요');
    return;
  } else {
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/accountmanage/nicknamecheck',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        nickname : nickname
  
      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);
  
        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var nickname_count = result_array.nickname_count;
        
        // console.log(nickname+' : '+nickname_count+'개');

        if (nickname_count > 0) {
          alert("해당 닉네임을 사용할 수 없습니다.");
          $('#nickname').empty();
        } else {
          alert("해당 닉네임은 사용 가능합니다.");
          $('#nickname_check').val('Y');
        }
      },
      error: function(err) {
        // $('#loading').fadeOut(200);
  
        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
  }
}

// 포스트 목록 설정 변경 사항을 적용한다.
function post_listing_setting() {
  var posts_per_page = $('#posts_per_page').val();
  var page_item_count = $('#page_item_count').val();

  // console.log('페이지 당 포스트 개수 : '+posts_per_page+'\n' // 줄바꿈 처리 '\n'
  //            +'페이지 아이템 표시 개수 : '+page_item_conunt
  // );
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/setting/postlistingsetting',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      posts_per_page : posts_per_page,
      page_item_count : page_item_count

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var posts_per_page = result_array.posts_per_page;
      var page_item_count = result_array.page_item_count;
      
      // console.log('페이지 당 포스트 개수 : '+posts_per_page+'\n' // 줄바꿈 처리 '\n'
      //        +'페이지 아이템 표시 개수 : '+page_item_count
      // );

      if (result_array != null) {
        alert("포스트 목록 설정 내용이 적용되었습니다.");
      } else {
        alert("포스트 목록 설정 내용이 적용되지 않았습니다.");
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });

}

// post 상태를 변경한다.
function post_status_change(obj) {
  var status = $(obj).is(':checked') ? 'Y' : 'N'; // 체크 여부 확인

  var slug = $(obj).attr('slug');
 
  // var host = location.host ; 
  // console.log(is_activate +','+ subject_code + ',' + host);
  // 실시간 처리를 위한 uri redirect (필요한 변수를 get방식으로 보낸다.)
  // window.location.href = '/setting/subjectcheck'+'?maincode='+main_subject_code+'&subject='+subject_code+'&activate='+is_activate;
  // $('#loading').fadeIn(200);
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/statusupdate',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      slug : slug,
      status : status

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      var post_status = result_array.post_status;
      // console.log(slug+' : '+post_status);

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });

}

// 줄바꿈 문자를 '<br />'로 바꿔준다.
function nl2br (str) {
  if (typeof str === 'undefined' || str === null) {
      return '';
  }
  return str.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
}

// 포스트 코멘트를 저장한다.
function save_comment(obj) {
  var post_slug = $(obj).attr('slug');
  var main_subject_name = $('#main_subject_name_'+post_slug).text();
  var sub_subject_name = $('#sub_subject_name_'+post_slug).text();
  var comment_nickname = $('#comment_nickname_'+post_slug).val();
  var comment_passwd = $('#comment_passwd_'+post_slug).val();
  var post_comment = $('#post_comment_'+post_slug).val();
  var is_secret = $('#is_secret_'+post_slug).is(':checked') ? 'Y' : 'N';

  // console.log(post_slug+','+comment_nickname+','+comment_passwd+','+is_secret+'\\r'+post_comment);

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/savecomment',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      post_slug : post_slug,
      main_subject_name : main_subject_name,
      sub_subject_name : sub_subject_name,
      comment_nickname : comment_nickname,
      comment_passwd : comment_passwd,
      post_comment : post_comment,
      is_secret : is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment = data.extra;
      // var new_comment_id = 'short_comment_'+comment.idx;
      // console.log(comment.post_comment);
      var comment_count = comment.count_all_count; // 코멘트 개수를 가져온다.
      var porotocol = window.location.protocol; // 프로토콜을 가져온다.
      var host = window.location.hostname; //호스트 네임을 가져온다.
      var href = porotocol+'//'+host+'/postmanage/commentlist/'+post_slug; // 댓글 목록을 갱신할 url을 생성한다.
      // console.log(href);
      if(comment != null) {
        
        $('#comment_nickname_'+post_slug).val(''); // 별명 요소값을 제거한다.
        $('#comment_passwd_'+post_slug).val('');  // 비밀번호 요소값을 제거한다.
        $('#is_secret').prop('checked',false);
        // if($('#is_secret').is(':checked')){
        //   $('#is_secret').click();
        // }
        ; // 체크박스를 해체 상태로 속성을 변경한다.
        $('#post_comment_'+post_slug).val(''); // 댓글 입력 요소값을 제거한다.
        ///////////////////////////////////////
        // 새로운 댓글 요소만 추가하는 방식
        ///////////////////////////////////////
        $('#comment_count_'+post_slug).text(comment_count); // 코멘트 개수를 업데이트 해준다.
        var status = $('#comment_list_'+post_slug).attr('status');
        if(status == 'close') {
          $('#comment_list_open_'+post_slug).click(); // 지연 시간을 0.5초를 준다.
          input_comment(comment,post_slug);
          document.getElementById("footer").scrollIntoView(false);
        } else {
          // $("#"+new_comment_id+"").click();
          input_comment(comment,post_slug);
          document.getElementById("footer").scrollIntoView(false); // 새로 추가된 댓글요소로 포커싱한다.
        }
        // alert('댓글이 추가 되었습니다.');
        // 토스트 메세지 요소를 추가한다.
        // $('#post_list').append('<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
        //                           '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
        //                             '<div class="d-flex justify-content-center">'+
        //                               '<div class="toast-body">'+
        //                                 '댓글이 추가 되었습니다.'+
        //                               '</div>'+
        //                               '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
        //                             '</div>'+
        //                           '</div>'+
        //                         '</div>'
        //                       );
        toast_init();
        
        ////////////////////////////////////////
        // 댓글 목록을 다시 로딩하는 방식
        ////////////////////////////////////////
        // $('#comment_list_'+post_slug).empty();
        //  // 로딩중 spinner 요소를 넣어준다.
        //   $('#comment_list_'+post_slug).append('<div class="d-flex justify-content-center align-items-center my-5" id="loading">'+
        //                           '<div class="spinner-border text-secondary" role="status">'+
        //                           '<span class="visually-hidden">Loading...</span>'+
        //                           '</div>'+
        //                         '</div>'
        //     );
        // $('#comment_list_'+post_slug).load(href,'comment_count='+comment.length,function(){
        //   $('#loading').remove(); // 로딩 요소를 제거한다.
        //   console.log(new_comment_id);
        //   $('#comment_list_open_'+post_slug).click(); // '댓글 보이기' 요소를 클릭한다.
        //   document.getElementById(new_comment_id).scrollIntoView({block:"center"}); // 새로 추가된 댓글요소로 포커싱한다.
        //   // $('#comment_save_btn_'+post_slug).css({"cursor":"none"}); // 저장 버튼 비활성화 처리를 위해 클래스를 다시 초기화 한다.
        // }); // 댓글 목록을 갱신한다.
        // alert('댓글이 추가 되었습니다.');
        update_ranking_point(post_slug)
      } else {
        alert('댓글 추가에 실패하였습니다.');
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 포스트 코멘트에 대한 댓글을 저장한다.
function save_comment_reply(obj) {
  var post_slug = $(obj).attr('slug');  // 포스트 url 식별 번호
  var org_idx = $(obj).attr('org_idx');
  var comment_reply_nickname = $('#comment_reply_nickname_'+org_idx).val();
  var comment_reply_passwd = $('#comment_reply_passwd_'+org_idx).val();
  var post_comment_reply = $('#post_comment_reply_'+org_idx).val();
  var comment_reply_is_secret = $('#comment_reply_is_secret_'+org_idx).is(':checked') ? 'Y' : 'N';

  // console.log(post_slug+','+org_idx+','+comment_reply_nickname+','+comment_reply_passwd+','+comment_reply_is_secret+'\n'+post_comment_reply);

  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/savecommentreply',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      org_idx : org_idx,
      post_slug : post_slug,
      comment_reply_nickname : comment_reply_nickname,
      comment_reply_passwd : comment_reply_passwd,
      post_comment_reply : post_comment_reply,
      comment_reply_is_secret : comment_reply_is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment_reply = data.extra;
      var comment_reply_id = 'comment_reply_'+comment_reply.idx;
      var comment_reply_count = comment_reply.comment_reply_count; // 대댓글 개수를 가져온다.
      $('#comment_reply_count_'+org_idx).val(comment_reply_count); // 대댓글 개수 표시 요소값을 업데이트 해준다.
      // console.log(comment_reply.post_comment_reply);
      var protocol = window.location.protocol; // 프로토콜을 가져온다.
      var host = window.location.hostname; //호스트 네임을 가져온다.
      var href = protocol+'//'+host+'/postmanage/commentlist/'+post_slug; // 댓글 목록을 갱신할 url을 생성한다.
      // console.log(href);
      if(comment_reply != null) {
        // alert('댓글에 댓글이 추가 되었습니다.');
        $('#comment_reply_nickname_'+org_idx).val(''); // 별명 요소값을 제거한다.
        $('#comment_reply_passwd_'+org_idx).val('');  // 비밀번호 요소값을 제거한다.
        $('#comment_reply_is_secret').prop('checked',false);
        // if($('#is_secret').is(':checked')){
        //   $('#is_secret').click();
        // }
        ; // 체크박스를 해체 상태로 속성을 변경한다.
        $('#post_comment_reply_'+org_idx).val(''); // 댓글 입력 요소값을 제거한다.
        $('#comment_reply_reg_'+org_idx).hide();
        input_comment_reply(comment_reply,org_idx); // 댓글의 댓글 요소를 추가한다.
        // $('#comment_list_'+post_slug).empty();
        // $('#comment_list_'+post_slug).load(href,'comment_count='+comment_reply.length,function(){
        //   console.log(comment_id);
        //   $('#comment_list_open_'+post_slug).click(); // '댓글 보이기' 요소를 클릭한다.
        document.getElementById(comment_reply_id).scrollIntoView({block:"center"}); // 새로 추가된 댓글요소로 포커싱한다.         
        // }); // 댓글 목록을 갱신한다.
        // $('#toast_message').remove(); // 기존의 토스트 메세지 요소를 제거한다.
        // // 토스트 메세지 요소를 추가한다.
        // $('#post_list').append('<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
        //                           '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
        //                             '<div class="d-flex justify-content-center">'+
        //                               '<div class="toast-body">'+
        //                                 '댓글에 대한 댓글이 추가 되었습니다.'+
        //                               '</div>'+
        //                               '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
        //                             '</div>'+
        //                           '</div>'+
        //                         '</div>'
        //                       );
        toast_init();
      } else {
        alert('댓글 추가에 실패하였습니다.');
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 코멘트 더보기
function comment_more(obj){
  var slug = $(obj).attr('slug');
  var hidden_comment = $('.hidden_comment');
  $.each(hidden_comment,function(index,item){
    if(index < comment_limit){
      $(item).removeClass('hidden_comment');
    }
  });
  var last_hidden_comment = $('.hidden_comment'); // 남은 숨겨진 코멘트
  if(last_hidden_comment.length < 1) { // 남은 숨겨진 코멘트가 없는 경우
    $('#comment_more_btn_'+slug+'').hide();
  }

}

// 코멘트 리스트 보이기/안보이기
function comment_list_toggle(obj){
  var $comment_list_status = $(obj).attr('status');
  var slug = $(obj).attr('slug');
  // console.log(slug+'번 포스트 댓글 리스트:'+$comment_list_status);
  if ($comment_list_status == 'open') {
    $('#comment_list_open_'+slug).hide();
    $('#comment_list_close_'+slug).show();
    $('#comment_list_item_'+slug).show(); 
    $('#comment_more_btn_'+slug).show();
    $('#comment_list_'+slug).attr({"status":"open"}); // 코멘트 리스트 status 속성값을 변경한다.

  } else {
    $('#comment_more_btn_'+slug).hide();
    $('#comment_list_close_'+slug).hide();
    $('#comment_list_open_'+slug).show();
    $('#comment_list_item_'+slug).hide();
    $('#comment_list_'+slug).attr({"status":"close"}); // 코멘트 리스트 status 속성값을 변경한다.
  }
}  

// 댓글 펼치기
function unfold_comment(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_id = 'short_comment_'+idx; // 댓글 요약 내용 요소
  var long_comment_id = 'long_comment_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_id = 'hidden_comment_'+idx // 댓글 수정폼 요소
  var comment_modify_save_btn_id = 'comment_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+short_comment_id+"").hide();
  $("#"+hidden_comment_id+"").hide();
  $("#"+comment_modify_save_btn_id+"").hide();
  $("#"+long_comment_id+"").show();
}

// 댓글에 대한 댓글 요소 펼치기
function unfold_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_reply_id = 'short_comment_reply_'+idx; // 댓글 요약 내용 요소
  var long_comment_reply_id = 'long_comment_reply_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_reply_id = 'hidden_comment_reply_'+idx // 댓글 수정폼 요소
  var comment_reply_modify_save_btn_id = 'comment_reply_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+short_comment_reply_id+"").hide();
  $("#"+hidden_comment_reply_id+"").hide();
  $("#"+comment_reply_modify_save_btn_id+"").hide();
  $("#"+long_comment_reply_id+"").show();
}

// 댓글 접기
function fold_comment(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_id = 'short_comment_'+idx;
  var long_comment_id = 'long_comment_'+idx;
  var hidden_comment_id = 'hidden_comment_'+idx // 댓글 수정폼 요소
  var comment_modify_save_btn_id = 'comment_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+long_comment_id+"").hide();
  $("#"+hidden_comment_id+"").hide();
  $("#"+comment_modify_save_btn_id+"").hide();
  $("#"+short_comment_id+"").show();
}

// 댓글에 대한 댓글 요소 접기
function fold_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_reply_id = 'short_comment_reply_'+idx; // 댓글 요약 내용 요소
  var long_comment_reply_id = 'long_comment_reply_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_reply_id = 'hidden_comment_reply_'+idx // 댓글 수정폼 요소
  var comment_reply_modify_save_btn_id = 'comment_reply_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  $("#"+long_comment_reply_id+"").hide();
  $("#"+hidden_comment_reply_id+"").hide();
  $("#"+comment_reply_modify_save_btn_id+"").hide();
  $("#"+short_comment_reply_id+"").show();
}

// 댓글 수정하기 버튼 이벤트
function modify_comment(obj) {
  var idx = $(obj).attr('idx');
  var input_div_id = 'passwd_check_'+idx;
  var input_id = 'modify_passwd_'+idx;
  // console.log(input_id);
  $("#"+input_div_id+"").slideToggle();
  $("#"+input_id+"").attr({"title":"modify"}).focus(); // title 속성값 변경 및 커서 이동
  // console.log(comment_passwd);
}

// 댓글에 대한 댓글 수정하기 버튼 이벤트
function modify_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var input_div_id = 'reply_passwd_check_'+idx;
  var input_id = 'modify_reply_passwd_'+idx;
  // console.log(input_id);
  $("#"+input_div_id+"").slideToggle();
  $("#"+input_id+"").attr({"title":"modify"}).focus(); // title 속성값 변경 및 커서 이동
  // console.log(comment_passwd);
}

// DOM 내의 모든 delete 버튼에 이벤트를 등록해준다.
function trash_init(){
  $('.del_btn').each(function(index,item){
    var mark = $(item).attr('mark');
    var del_btn = $(item).find('span').eq(1); // trash_body 부분 인스턴스 생성
    del_btn.css("content" , "url( '/img/trash_body.svg' )");
    // console.log(item);
    item.addEventListener('mouseover',function(){
      del_btn.css("content" , "url( '/img/trash_body_hover.svg' )");
    });
    item.addEventListener('mouseout',function(){
      del_btn.css("content" , "url( '/img/trash_body.svg' )");
    });
    item.addEventListener('click',function(){
      if(mark == 'comment'){
        del_comment(item);
      } else {
        del_comment_reply(item);
      }
    });
  });
  // console.log($('.del_btn').length+'개 삭제 버튼 요소에 이벤트가 등록되었습니다');
}

// 새로 추가된 delete 버튼에 이벤트를 등록해준다.
function trash_init_each(mark,idx){
  if(mark == 'comment'){
    var id = 'delete_button_'+idx; // 새로 추가된 댓글 삭제 버튼 id
  } else {
    var id = 'reply_delete_button_'+idx; // 새로 추가된 대댓글 삭제 버튼 id
  }
  var btn_el = document.getElementById(id); // 새로 추가된 버튼 요소를 변수에 담는다.
  var del_btn = $(btn_el).find('span').eq(1); // trash_body 부분 인스턴스 생성
  // console.log(del_btn);
  btn_el.addEventListener('mouseover',function(){
    del_btn.css("content" , "url( '/img/trash_body_hover.svg' )");
  });
  btn_el.addEventListener('mouseout',function(){
    del_btn.css("content" , "url( '/img/trash_body.svg' )");
  });
  btn_el.addEventListener('click',function(){
    if(mark == 'comment'){
      del_comment(btn_el);
    } else {
      del_comment_reply(btn_el);
    }
  });
  // console.log(id+'요소에 이벤트가 등록되었습니다');
}

// 댓글 삭제하기 버튼 이벤트
function del_comment(obj) {
  var idx = $(obj).attr('idx');
  var input_div_id = 'passwd_check_'+idx;
  var input_id = 'modify_passwd_'+idx;
  $("#"+input_div_id+"").slideToggle();
  $("#"+input_id+"").attr({"title":"delete"}).focus(); // title 속성값 변경 및 커서 이동
}

// 댓글에 대한 댓글 삭제하기 버튼 이벤트
function del_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var input_div_id = 'reply_passwd_check_'+idx;
  var input_id = 'modify_reply_passwd_'+idx;
  $("#"+input_div_id+"").slideToggle();
  $("#"+input_id+"").attr({"title":"delete"}).focus(); // title 속성값 변경 및 커서 이동
}

// 댓글 비밀번호값을 체크하여 비밀번호 수정 요소를 활성화 한다.
function check_value(obj,callback,callback_2,callback_3) {
  var passwd_value = $(obj).val();
  var post_slug = $(obj).attr('post_slug');
  var idx = $(obj).attr('idx');
  var title = $(obj).attr('title'); 
  
  var cur_length = passwd_value.length; // 현재 문자열 길이
  var last_char = passwd_value.charAt(cur_length - 1);
  // passwd_temp = passwd_value;
  // console.log(passwd_value+','+passwd_temp.length+','+cur_length+','+last_char);
  // 입력값 길이가 4자를 초과하는지 마지막 입력 문자가 숫자인지 확인
  if(isNaN(last_char) == true) {
    alert("비밀번호는 4자리 숫자만 입력가능합니다.");
    if(callback != undefined) {
      undo_input(obj,passwd_value); //숫자가 아닌 값을 입력했을 입력값 제거한다.
    }
  }

  if(passwd_value.length == 4 && isNaN(last_char) == false){
    // console.log(title);

    // 해당 댓글의 정보를 가져와 입력한 비밀번호와 해당 댓글의 비밀번호를 비교한다.
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/postmanage/getcomment',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        idx : idx,

      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);

        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var comment_passwd = result_array.comment_passwd;
        // console.log(passwd_value+','+comment_passwd);

        // 비밀번호 입력값이 댓글의 비밀번호와 같을 경우 댓글 수정 요소를 펼친다.
        if(passwd_value == comment_passwd) {
          if(title == 'modify'){
            if(callback_2 != undefined){
              unfold_hidden_comment(obj);
            }
          } else {
            if(confirm('댓글이 삭제됩니다.')){
              if(callback_3 != undefined){
                delete_comment(idx,post_slug);
              }
            }          
          }
        } else {
          alert("비밀번호를 정확히 입력해주세요.");
        }

      },
      error: function(err) {
        // $('#loading').fadeOut(200);

        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
    
  } 
}

// 댓글에 대한 댓글 비밀번호값을 체크하여 비밀번호 수정 요소를 활성화 한다.
function check_value_reply(obj,callback,callback_2,callback_3) {
  var passwd_value = $(obj).val();
  var idx = $(obj).attr('idx');
  var title = $(obj).attr('title'); 
  
  var cur_length = passwd_value.length; // 현재 문자열 길이
  var last_char = passwd_value.charAt(cur_length - 1);
  // passwd_temp = passwd_value;
  // console.log(passwd_value+','+passwd_temp.length+','+cur_length+','+last_char);
  // 입력값 길이가 4자를 초과하는지 마지막 입력 문자가 숫자인지 확인
  if(isNaN(last_char) == true) {
    alert("비밀번호는 4자리 숫자만 입력가능합니다.");
    if(callback != undefined) {
      undo_input(obj,passwd_value); //숫자가 아닌 값을 입력했을 입력값 제거한다.
    }
  }

  if(passwd_value.length == 4 && isNaN(last_char) == false){
    // console.log(title);

    // 해당 댓글의 정보를 가져와 입력한 비밀번호와 해당 댓글의 비밀번호를 비교한다.
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/postmanage/getcommentreply',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        idx : idx,

      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);

        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var comment_reply_passwd = result_array.comment_reply_passwd;
        // console.log(passwd_value+','+comment_reply_passwd);

        // 비밀번호 입력값이 댓글의 비밀번호와 같을 경우 댓글 수정 요소를 펼친다.
        if(passwd_value == comment_reply_passwd) {
          if(title == 'modify'){
            if(callback_2 != undefined){
              unfold_hidden_comment_reply(obj);
            }
          } else {
            if(confirm('댓글이 삭제됩니다.')){
              if(callback_3 != undefined){
                delete_comment_reply(idx);
              }
            }          
          }
        } else {
          alert("비밀번호를 정확히 입력해주세요.");
        }

      },
      error: function(err) {
        // $('#loading').fadeOut(200);

        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
    
  } 
}

// 댓글 비밀번호값을 체크한다.
function check_passwd_value(obj,callback) {
  var passwd_value = $(obj).val();
  var cur_length = passwd_value.length; // 현재 문자열 길이
  var last_char = passwd_value.charAt(cur_length - 1);
 
  // 입력값 길이가 4자를 초과하는지 마지막 입력 문자가 숫자인지 확인
  if(isNaN(last_char) == true) {
    alert("비밀번호는 4자리 숫자만 입력가능합니다.");
    if(callback != undefined) {
      undo_input(obj,passwd_value); //숫자가 아닌 값을 입력했을 입력값 제거한다.
    }
  }
}
// 비밀번호 입력값에서 숫자가 아닌값을 제외하고 다시 넣어준다.
function undo_input(obj, passwd_value) {
  var new_value = '';
  for(i=0; i < passwd_value.length; i++){
    if(isNaN(passwd_value.charAt(i)) == true){
      break;
    } else { // 숫자인 값만 새로운 변수에 담는다.
      new_value += passwd_value.charAt(i);
    }
  }
      
    $(obj).val(new_value); 
}

// 댓글 수정 요소 펼치기
function unfold_hidden_comment(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_id = 'short_comment_'+idx; // 댓글 요약 내용 요소
  var long_comment_id = 'long_comment_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_id = 'hidden_comment_'+idx; // 댓글 수정폼 요소
  var comment_modify_id = 'comment_modify_'+idx; // 댓글 수정 textarea 요소
  var comment_modify_save_btn_id = 'comment_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  // console.log(hidden_comment_id);
  $("#"+short_comment_id+"").hide();
  $("#"+long_comment_id+"").hide();
  $("#"+hidden_comment_id+"").show();
  $("#"+comment_modify_save_btn_id+"").show();
  $("#"+comment_modify_id+"").focus(); // 댓글 수정 textarea 요소로 커서를 옮긴다.
}

// 댓글에 대한 댓글 수정 요소 펼치기
function unfold_hidden_comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var short_comment_reply_id = 'short_comment_reply_'+idx; // 댓글 요약 내용 요소
  var long_comment_reply_id = 'long_comment_reply_'+idx; // 댓글 전체 내용 요소
  var hidden_comment_reply_id = 'hidden_comment_reply_'+idx; // 댓글 수정폼 요소
  var comment_reply_modify_id = 'comment_reply_modify_'+idx; // 댓글 수정 textarea 요소
  var comment_reply_modify_save_btn_id = 'comment_reply_modify_save_btn_'+idx; // 수정 댓글 저장 버튼 요소
  // console.log(hidden_comment_reply_id);
  $("#"+short_comment_reply_id+"").hide();
  $("#"+long_comment_reply_id+"").hide();
  $("#"+hidden_comment_reply_id+"").show();
  $("#"+comment_reply_modify_save_btn_id+"").show();
  $("#"+comment_reply_modify_id+"").focus(); // 댓글 수정 textarea 요소로 커서를 옮긴다.
}

// 댓글 내용 업데이트
function update_comment(obj){
  var idx = $(obj).attr('idx');
  // var short_comment_id = 'short_comment_'+idx;
  var long_comment_id = 'long_comment_'+idx;
  var new_is_secret_id = 'is_secret_'+idx;
  var new_comment_id = 'comment_modify_'+idx;
  var new_comment_passwd_id = 'modify_passwd_'+idx;  // 수정 요소의 비밀번호를 가져온다.
  var new_is_secret = $("#"+new_is_secret_id+"").is(':checked') ? 'Y' : 'N';
  var new_comment = $("#"+new_comment_id+"").val(); // 댓글 수정하기 textarea 요소 값을 가져온다.
  // new_comment = new_comment.replace(/(?:\r\n|\r|\n)/g,'<br/>');
  var new_comment_passwd = $("#"+new_comment_passwd_id+"").val();
  // console.log(new_comment+', 비밀글 : '+new_is_secret+', 비번 : '+new_comment_passwd);

  // 댓글 정보를 업데이트 한다.
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/updatecomment',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      idx : idx,
      new_comment : new_comment,
      new_comment_passwd : new_comment_passwd,
      new_is_secret : new_is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment = data.extra;
      var updated_comment = comment.post_comment;
      // console.log(updated_comment);

      if(comment != null) {
        alert('댓글이 수정 되었습니다.');
        // 수정된 댓글을 화면에 반영한다.
        $("#"+long_comment_id+"").empty().html(nl2br(updated_comment));

        // 엔터문자 개수를 센다.
        var count = count_str(updated_comment,'\n')
        
        // 코멘트 텍스트 단어 요소로된 배열로 만든다.
        var comment_text = text_to_word(updated_comment);

        // 단어의 개수가 4개 초과이거나 엔터문자가 1개 초과인 경우 요소를 넣어준다.
        if(comment_text.length > 4 || count > 0) {
          $("#"+long_comment_id+"").append('<div><span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+ idx +'" onclick="fold_comment(this)">간략히 보기</span></div>');
        }
        
        unfold_comment(obj);
        // console.log(comment_text);
        $("#"+new_comment_passwd_id+"").val(''); // 수정 댓글 비밀번호를 초기화 한다.
        $("#modify_button_"+idx+"").click();

      } else {
        alert('댓글 수정에 실패하였습니다.')
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 댓글에 대한 댓글 내용 업데이트
function update_comment_reply(obj){
  var idx = $(obj).attr('idx');
  // var short_comment_id = 'short_comment_'+idx;
  var long_comment_reply_id = 'long_comment_reply_'+idx;
  var new_reply_is_secret_id = 'reply_is_secret_'+idx;
  var new_comment_reply_id = 'comment_reply_modify_'+idx;
  var new_comment_reply_passwd_id = 'modify_reply_passwd_'+idx;  // 수정 요소의 비밀번호를 가져온다.
  var new_reply_is_secret = $("#"+new_reply_is_secret_id+"").is(':checked') ? 'Y' : 'N';
  var new_comment_reply = $("#"+new_comment_reply_id+"").val(); // 댓글 수정하기 textarea 요소 값을 가져온다.
  // new_comment = new_comment.replace(/(?:\r\n|\r|\n)/g,'<br/>');
  var new_comment_reply_passwd = $("#"+new_comment_reply_passwd_id+"").val();
  // console.log(new_comment_reply+', 비밀글 : '+new_reply_is_secret+', 비번 : '+new_comment_reply_passwd);

  // 댓글 정보를 업데이트 한다.
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/updatecommentreply',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      idx : idx,
      new_comment_reply : new_comment_reply,
      new_comment_reply_passwd : new_comment_reply_passwd,
      new_reply_is_secret : new_reply_is_secret

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var comment_reply = data.extra;
      var updated_comment_reply = comment_reply.post_comment_reply;
      // console.log(updated_comment_reply);

      if(comment_reply != null) {
        alert('댓글에 대한 댓글이 수정 되었습니다.');
        // 수정된 댓글을 화면에 반영한다.
        $("#"+long_comment_reply_id+"").empty().html(nl2br(updated_comment_reply));

        // 엔터문자 개수를 센다.
        var count = count_str(updated_comment_reply,'\n')

        // 코멘트 텍스트 단어 요소로된 배열로 만든다.
        var comment_text = text_to_word(updated_comment_reply);

        // 단어의 개수가 4개 초과이거나 엔터문자가 1개 초과인 경우 요소를 넣어준다.
        if(comment_text.length > 4 || count > 0) {
          $("#"+long_comment_reply_id+"").append('<div><span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+ idx +'" onclick="fold_comment_reply(this)">간략히 보기</span></div>');
        }

        unfold_comment_reply(obj);
        // console.log(comment_reply);
        $("#"+new_comment_reply_passwd_id+"").val(''); // 수정 댓글 비밀번호를 초기화 한다.
        $("#reply_modify_button_"+idx+"").click();

      } else {
        alert('댓글 수정에 실패하였습니다.')
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 문장을 단어 단위로 자른다.
function truncate(str, no_words) {
  return str.split(" ").splice(0,no_words).join(" ");
}

// 텍스트의 엔터기호를 삭제하고 각 요소를 '.'요소로 잘라 단어 단위 요소로 구성된 배열을 만든다.
function text_to_word(text) {
  var word_array = text.replace(/\n/g,'').split(' '); // 엔터기호를 삭제하고 공백문자 기준으로 자른다.
  // console.log(word_array);
  $.each(word_array,function(index,item){
    var word = item.split('.'); // 각 요소를 '.' 기준으로 자르고
    // console.log(word);
    if(word.length > 1) {
      $.each(word,function(index_2,item_2){
        word_array.splice(index,0,item_2) ; // 배열에 다시 넣어 준다.
        index++; // 다음 위치로 index를 변경해준다.
      });
      word_array.splice(index,1); // 원본 요소를 삭제한다.
    }
    
  }); // --end of each
  // console.log(word_array);
  return word_array;
}

// 특정문자의 개수를 센다.
function count_str(text,str){
  var count = 0;
  var searchChar = str; // 찾으려는 문자
  var pos = text.indexOf(searchChar); //pos는 str을 처음 만나는 위치의 index

  while (pos !== -1) {
    count++;
    pos = text.indexOf(searchChar, pos + 1); // 첫 번째 인덱스부터 str를 찾습니다.
  }

  // console.log('"\"'+ str + '"'+'의 개수 : '+ count);
  return count;
}

// 댓글을 삭제한다.
function delete_comment(idx,post_slug) {
  // 댓글에 대한 댓글 개수를 확인한다.
  var comment_reply_count = $("#comment_reply_count_"+idx+"").val();
  if(Number(comment_reply_count) == 0){
    $.ajax({
      // 서버와  ajax()함수 사이의 http 통신 정보
      url: '/postmanage/deletecomment',   //정보 처리를 할 서버 파일 위치
      type: 'post',                             //정보 전달 방식
      data: {                                   //서버에 전달될 정보 (변수이름 : 값)

        idx : idx

      },
      dataType: 'json',                         //처리 후 돌려받을 정보의 형식
      async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
      cache: false,                             //데이터 값을 캐싱 할건지 여부
      success: function(data) {
        // $('#loading').fadeOut(200);

        if (data.code != 0) {
          alert(data.message + '\n(오류코드 : ' + data.code + ')');
          return;
        }
        
        var result_array = data.extra;
        var comment_reply_count = $("#comment_reply_count_"+idx+"").val();
        // console.log(result_array.is_del+','+comment_reply_count);

        if(result_array != null) {
            alert('댓글이 삭제 되었습니다.');
            $("#comment_"+idx+"").remove();
            var new_comment_count = Number($('#comment_count_'+post_slug).text()) - 1; // 기존의 댓글 개수에서 삭제된 댓글 개수를 빼 새로운 개수를 구한다.
            $('#comment_count_'+post_slug).text(new_comment_count); // 코멘트 개수를 업데이트 해준다.
        } else {
          alert('댓글 삭제에 실패하였습니다.');
        }

      },
      error: function(err) {
        // $('#loading').fadeOut(200);

        var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
        alert('내부 오류가 발생하였습니다.' + error_message);
      }
    });
  }else {
    console.log(comment_reply_count);
    alert('댓글이 있는 댓글은 삭제할 수 없습니다.');
  }
}

// 댓글에 대한 댓글을 삭제한다.
function delete_comment_reply(idx) {
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/deletecommentreply',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      idx : idx

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      
      console.log(result_array.is_del);

      if(result_array != null) {
        alert('댓글이 삭제 되었습니다.');
        $("#comment_reply_"+idx+"").remove();
      } else {
        alert('댓글 삭제에 실패하였습니다.')
      }

    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// 대댓글 등록 관련 요소를 활성화/비활성화 한다.
function comment_reply(obj) {
  var idx = $(obj).attr('idx');
  var comment_reply_reg_id = 'comment_reply_reg_'+idx;
  // console.log(idx+'번 댓글에 댓글을 남김니다.');
  $("#"+comment_reply_reg_id+"").slideToggle();
}

// 포스트 목록을 갱신한다.
function post_list(obj) {

  var button_type = $(obj).attr('type');
  var url = $(obj).attr('url');
  var params = $(obj).attr('params'); // 파라메터값을 속성으로 가져온다.
  $("#side_content").hide(); // 사이드 컨텐츠를 감춘다.
  // params = encodeURI(params);
  // console.log(params);
  $("#side_content").hide(); // 사이드 컨텐츠를 감춘다.
  // console.log('포스트 목록을 갱신합니다.' + url);
  var protocol = window.location.protocol; // 프로토콜을 가져온다.
  var host = window.location.hostname; // 호스트 네임을 가져온다.
  var href = protocol+"//"+host+url+params; // 댓글 목록을 갱신할 url을 생성한다.
  // href = encodeURIComponent(href);
  var url_status = protocol+"//"+host+'/postmanage/index'+params;
  ///////////////////////////////////////////////////////////////////////////////
  history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
  ///////////////////////////////////////////////////////////////////////////////
  $("#title_images").show(); // 타이틀 이미지 요소를 보이게 한다.
  $("#head_title").text('포스트 목록'); // 상단 타이틀을 변경해준다.
  $("#search_form").show(); // 검색요소를 보이게 한다.
  $("#search_div").show(); // 검색요소를 보이게 한다.
  // console.log(typeof href);
  $("#post_list").empty();
  var inner_height = get_browser_height(); // 현재 브라우저의 높이를 구한다.
  var height = inner_height - 500;  // 상단 고정 요소의 height를 빼준다.
  // 로딩중 spinner 요소를 넣어준다.
  $("#post_list").append('<div class="d-flex justify-content-center align-items-center" style="height:'+height+'px;" id="loading">'+
                            '<div class="spinner-border text-secondary" role="status">'+
                            '<span class="visually-hidden">Loading...</span>'+
                            '</div>'+
                          '</div>'
  );
  $("#post_list").load(href,function(){   
  //  console.log(obj);
    $("#loading").remove(); // 로딩이 끝나면 요소를 제거한다.
    if( button_type === 'top_sub'){ // 상단 메뉴 하위 주제를 클릭하면
      $("#hamburger").click(); // 햄버거 메뉴 버튼을 클릭한다.
    }
    // 상단 메뉴와 사이드바 메뉴를 동기화 한다.
    sync_menu(obj)
  });
  
}

// 포스트 세부 내용을 가져온다.
function post_detail(obj) {

  var pathname = $(obj).attr('url');
  var pathname_array = pathname.split('/');
  var post_slug = pathname_array[pathname_array.length-1]; // 마지막 요소를 가져온다.
  var pathname_temp = '/postmanage/detaillist/'+post_slug; // 주소창에 보여줄 url 정보를 생성한다.
  var params = $(obj).attr('params'); // 파라메터값을 속성으로 가져온다.
  $("#side_content").hide(); // 사이드 컨텐츠를 감춘다.
  // console.log(url+' , '+params+' , '+url_temp);
  
  // console.log('포스트 목록을 갱신합니다.' + url);
  var protocol = window.location.protocol; // 프로토콜을 가져온다.
  var hostname = window.location.hostname; //호스트 네임을 가져온다.
  var href = protocol+"//"+hostname+pathname+params; // 댓글 목록을 갱신할 url을 생성한다.
  var url_status = protocol+"//"+hostname+pathname_temp+params; // 주소창에 보여줄 전체 url을 조합한다.
  $("#title_images").hide(); // 타이틀 이미지 요소를 감춘다.
  $("#head_title_div").addClass('pt-2'); // 클래스를 추가해준다.
  $("#head_title").text('포스트 보기'); // 상단 타이틀을 변경해준다.
  $("#search_form").hide(); // 검색요소를 안 보이게 한다.
  $("#search_div").hide(); // 검색요소를 안 보이게 한다.
  ///////////////////////////////////////////////////////////////////////////////
  history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
  ///////////////////////////////////////////////////////////////////////////////
  // console.log(typeof url_status);
  $("#post_list").empty();
  var inner_height = get_browser_height(); // 현재 브라우저의 높이를 구한다.
  var height = inner_height - 200;  // 상단 고정 요소의 height를 빼준다.
  // console.log('post_slug : '+post_slug+', height : '+height);
  // 로딩중 spinner 요소를 넣어준다.
  $("#post_list").append('<div class="d-flex justify-content-center align-items-center" style="height:'+height+'px;" id="loading">'+
                            '<div class="spinner-border text-secondary" role="status">'+
                            '<span class="visually-hidden">Loading...</span>'+
                            '</div>'+
                          '</div>'
  );
  $("#post_list").load(href,function(){
    // console.log(obj);
    $("#loading").remove(); // 로딩이 끝나면 요소를 제거한다.
    update_ranking_point(post_slug)
    sync_menu(obj)
   });
}

// 포스트 세부 내용을 가져온다.
function post_detail_swiper(type,post_slug) {

  var protocol = window.location.protocol; // 프로토콜을 가져온다.
  var hostname = window.location.hostname; // 호스트 네임을 가져온다.
  var pathname = window.location.pathname; // pathname 을 가져온다.
  // console.log('pathname : '+pathname);
  var params = window.location.search; // 파라메터값을 속성으로 가져온다.
  // var params_array = params.split('&');
  // params_array.pop(); // 마지막 변수를 제거하고
  // var new_params = params_array.join('&')+'&type='+type; // 다시 '&'기준으로 병합한다.
  // var post_slug = pathname_array[pathname_array.length-1]; // 마지막 요소를 가져온다.
  var pathname_temp = '/postmanage/detail/'+post_slug; // 주소창에 보여줄 url 정보를 생성한다.
  
  $("#side_content").hide(); // 사이드 컨텐츠를 감춘다.
  // console.log(pathname+' , '+params+' , '+pathname_temp);
  
  // console.log('포스트 목록을 갱신합니다.' + pathname);

  var href = protocol+"//"+hostname+pathname_temp+params; // 댓글 목록을 갱신할 url을 가져온다.
  var url_status = protocol+"//"+hostname+pathname_temp+params; // 주소창에 보여줄 전체 url을 조합한다.
  $("#head_title").text('포스트 보기'); // 상단 타이틀을 변경해준다.
  $("#search_form").hide(); // 검색요소를 안 보이게 한다.
  $("#search_div").hide(); // 검색요소를 안 보이게 한다.
  ///////////////////////////////////////////////////////////////////////////////
  history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
  ///////////////////////////////////////////////////////////////////////////////

  $("#post_list").empty();
  var inner_height = get_browser_height(); // 현재 브라우저의 높이를 구한다.
  var height = inner_height - 200;  // 상단 고정 요소의 height를 빼준다.
  // console.log('post_slug : '+post_slug+', height : '+height);
  // 로딩중 spinner 요소를 넣어준다.
  $("#post_list").append('<div class="d-flex justify-content-center align-items-center" style="height:'+height+'px;" id="loading">'+
                            '<div class="spinner-border text-secondary" role="status">'+
                            '<span class="visually-hidden">Loading...</span>'+
                            '</div>'+
                          '</div>'
  );
  $("#post_list").load(href,function(){
    // console.log(obj);
    $("#loading").remove(); // 로딩이 끝나면 요소를 제거한다.
    update_url(type,post_slug);
    update_ranking_point(post_slug)
    $("#side_content").show(); // 사이드 컨텐츠를 보이게 한다.
    // sync_menu(obj)
   });
   return height;
}

// 상단 메뉴, 사이드바 메뉴의 클릭 상태를 동기화 한다.
function sync_menu(obj) {
  $("#side_content").show(); // 사이드 컨텐츠를 보이게 한다.
  var parent_name = $(obj).attr('parent'); // 메인 주제 이름을 가져온다.
  var browser_width = window.innerWidth || document.body.clientWidth; //브라우저의 넓이를 가져온다.
  var is_main = $(obj).attr('is_main'); // 메인 주제 여부를 가져온다.
  var is_top = $(obj).attr('is_top'); // 상단 메뉴 여부를 가져온다.
  
  $('#sidebarMenu').find('span').each(function(index,parent){ // 사이드바 메뉴의 모든 메인주제 요소에 대해
    var parent_text = $(parent).text();
    var parent_text_arr = parent_text.split('(');
    // console.log(parent_text_arr[0]+' , '+parent_name+' , '+'browser width: '+browser_width);
    if(parent_name == parent_text_arr[0]){ // 사이드바 메인주제 이름과 클릭한 요소의 text 요소값이 같은 경우
      var btn_id = 'btn_'+parent_text_arr[0]; // 클릭할 사이드바 버튼의 id를 조합한다.
      var click = $("#"+btn_id+"").attr('click'); // 사이드바 메인 메뉴의 클릭 상태 여부를 가져온다.
      var status = $("#"+btn_id+"").attr('status'); // 사이드바 메인 메뉴의 펼쳐진 상태 여부를 가져온다.
      // console.log(parent_name+' 클릭 상태 before: '+$("#"+btn_id+"").attr('click'));
      if(browser_width > 992){ // 브라우저 넓이가 992px 이상이고
        if(is_top == 'Y' && is_main == 'Y' ) { // 상단 메뉴 메인 주제를 클릭했을 경우
          // $("#"+btn_id+"").click(); // 해당 id의 사이드바 버튼을 클릭한다.   
          if(click == 'Y'){ // 버튼 클릭 상태가 'Y'이면
            // $("#"+btn_id+"").attr({"click":'N'}); // 아무것도 하지 않는다.
          }else {
            $("#"+btn_id+"").attr({"click":'Y'});
          }
          // console.log(parent_name+' 클릭 상태 after: '+$("#"+btn_id+"").attr('click'));
        } else if(is_top == 'Y' && is_main == 'N' && status != 'O') { // 상단 메뉴 하위 주제를 클릭했을 경우 클릭할 사이드바 버튼 클릭 상태가 'N'일때
            if(click == 'Y'){
              $("#"+btn_id+"").click();  // 버튼을 클릭한다.
              $("#"+btn_id+"").attr({"status":'O'}); //상태값을 'O'(Open)로 바꿔준다.
            } else {
              // $("#"+btn_id+"").click();  // 아무것도 하지 않는다.
            }
          // console.log(parent_name+' 클릭 상태 after: '+$("#"+btn_id+"").attr('click'));
        } else if(is_top == 'N' && is_main == 'Y' ) { // 사이드바 메인 주제를 클릭했을 경우
          // $("#"+btn_id+"").click(); // 해당 id의 사이드바 버튼을 글릭한다.   
          if(click == 'Y'){ // 버튼 클릭 상태가 'Y'면
            $("#"+btn_id+"").attr({"click":'N'});
            $("#"+btn_id+"").attr({"status":'C'}); // 상태값을 'C'(Close)로 바꿔준다.
          }else {
            $("#"+btn_id+"").attr({"click":'Y'});
            $("#"+btn_id+"").attr({"status":'O'}); // 상태값을 'O'(Open)로 바꿔준다.
          }
          // console.log(parent_name+' 클릭 상태 after: '+$("#"+btn_id+"").attr('click'));
        } else if(is_main == 'N' && $("#"+btn_id+"").attr('click') != 'Y' && status != 'O') { // 하위 주제를 클릭했을 경우 클릭할 사이드바 버튼 클릭 상태가 'N'일때
          $("#"+btn_id+"").click();  // 버튼을 클릭하고
          $("#"+btn_id+"").attr({"click":'Y'}); // 버튼 클릭 상태를 변경한다.
          $("#"+btn_id+"").attr({"status":'O'}); // 상태값을 'O'(Open)로 바꿔준다.
          // console.log(parent_name+' 클릭 상태 after: '+$("#"+btn_id+"").attr('click'));
        } 
      }
    }
    // 사이드바 메뉴의 하위 메뉴를 클릭했을 경우 해당 요소를 활성화 한다.
    $('#sidebarMenu').find('a').each(function(index,item){
      // console.log($(obj).attr('name'));
      var name = $(item).text();
      var name_arr = name.split('(');
      // console.log(name_arr[0]);
      if(name_arr[0] != $(obj).attr('name')) {
        $(item).attr({"class":"link-dark rounded"}); // 클릭되지 않은 요소는 비활성화 한다.
      } else {
      
        $(item).attr({"class":"link-dark rounded selected"});
      }
    });
  });
  
}

// swiper를 초기화 한다.
function swiper_init() {
  
  const swiper = new Swiper('.swiper', {
    // Optional parameters
    // direction: 'horizontal',
    // loop: type == 'post' ? true : false,  // 포스트일때만 루프를 허용한다.
    autoHeight: type == 'post_list' ? true : false, // 슬라이드별로 높이를 동적으로 조정해준다. // 페이지 최초로딩시 한번 멈추는 현상이 있어 사용 보류.
    setWrapperSize: true,
    noSwipingClass : 'colorscripter-code', // 스와이프가 되지 않는 클래스를 설정한다.
    threshold: 5,
    // shortSwipes: false, // 짧은 스와이프로 슬라이드가 변경되는 걸 허용하지 않는다.
    spaceBetween: 20, // 슬라이드간의 간격을 설정해준다.
    lazy: true, // 로딩 지연 효과를 준다.
    simulateTouch : false, // 마우스 클릭 스와이퍼를 끈다.
    // // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      dynamicBullets: true, // 동적 페이징 요소 사용
      dynamicMainBullets: page_item_count, // 활성 페이징 요소 개수
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + "</span>";
      },
    },
    on: {
      // activeIndexChange: function () {
      //   alert((this.realIndex+1)+'번째 slide입니다.');
      // }
    }
  });

  // console.log('현재 슬라이드 수 : '+cur_slide+', 전체 슬라이드 수 : '+all_slide+', 슬라이드 유형 : '+type);
  
  var now = cur_slide; // 포스트의 시작 위치(순서) 및 포스트 목록 슬라이드 순서로 사용될 변수에 현재 슬라이드 인덱스를 담는다.

  // Now you can use all slider methods like
  if(cur_slide) { // 포스트 보기인 경우 현재 포스트의 순서로 이동한다. 슬라이딩 소요시간 없음
    swiper.slideTo((cur_slide-1),0); // 슬라이드 실제 인덱스가 0부터 시작하므로 -1을 해준다.
  }
  
  // 스와이퍼를 하면 콜백함수를 실행한다.
  swiper.on('transitionStart', function () { // 목적 슬라이드로 전환이 완료되면 함수를 실행한다.
    var active_index = swiper.activeIndex + 1; // 이동한 슬라이드의 인덱스(순서)를 확인한다.
    // console.log('active_index : '+active_index+', now : '+now+', translate : '+swiper.translate);
    // console.log(swiper.wrapperEl);
    var diff = 0;
    diff = active_index - now;
    if(Math.abs(diff) > 1 && type == 'post_list') { // 포스트 목록 슬라이드의 변동된 인덱스 차이가 1 보다 큰 경우(페이징 요소를 클릭 했을 경우)
      now = active_index;
    }
    
    if (swiper.touches.startX < swiper.touches.currentX){
      var position_diff = Math.abs(swiper.touches.diff);
      // console.log('position_diff : '+swiper.touches.diff+'  to left'); // 왼쪽 슬라이드로 이동
      if(now > 1) { // 현재 슬라이드 순서가 1번째 이상일 때
        var target = Number(now) - 1;
        // console.log('현재 슬라이드 : '+ now + '\r' +', 목적 슬라이드 : ' + target);
        if (now > 2 && type == 'post' && position_diff > 100 ){  // 포스트 보기 페이지에서 포스트 순서가 2번째 이상이고스와이프 좌표 차이가 150px을 초과한 경우
          // 목적 포스트의 조회수를 업데이트 한다.
          var slide_height = post_detail_swiper(type,pre_post_slug);
          now = post_order; // 현재 포스트 순서를 업데이트 한다.
          // console.log('현재 슬라이드 : '+ now + '\r' +', 목적 슬라이드 : ' + target+', 슬라이드 높이 : ' + slide_height+', 슬라이드 넓이 : ' + swiper.width);
        } else if(type == 'post_list') {
          // 변경된 슬라이의 index를 반영하여 url을 업데이트 한다.
          if(active_index != now){
            now--;
          }
          update_url(type,now);
          var slide_height = $("#slide_"+now+"").height();
          // $(swiper.wrapperEl).css('height',slide_height+"px");
          // console.log('현재 슬라이드 : '+ now + '\r' +', 목적 슬라이드 : ' + target+', 슬라이드 높이 : ' + slide_height+', 슬라이드 넓이 : ' + swiper.width);

        }

      } // --end of if
      
    } else if (swiper.touches.startX > swiper.touches.currentX) {
      var position_diff = Math.abs(swiper.touches.diff);
      console.log('position_diff : '+swiper.touches.diff+'  to right'); // 오른쪽 슬라이드로 이동
      if(now <= all_slide) {  // 현재 슬라이드가 전체 슬라이드의 마지막 슬라이드 이전인 경우 오른쪽으로 슬라이드를 허용한다.
        var target = Number(now) + 1;
        // console.log('현재 슬라이드 : '+ now + '\r' +', 목적 슬라이드 : ' + target);
        if (type == 'post' && position_diff > 100 ){  // 포스트 보기 페이지에서 스와이프 좌표 차이가 150px을 초과한 경우
          // 목적 포스트의 조회수를 업데이트 한다.
          var slide_height = post_detail_swiper(type,next_post_slug);
          // $('.swiper-wrapper').css('height',slide_height+"px");
          now = post_order;
          // console.log('현재 슬라이드 : '+ now + '\r' +', 목적 슬라이드 : ' + target+', 슬라이드 높이 : ' + slide_height+', 슬라이드 넓이 : ' + swiper.width);
        } else if(type == 'post_list') {
          // 변경된 슬라이의 index를 반영하여 url을 업데이트 한다.
          if(active_index != now){
            now++;
          }
          update_url(type,now);
          var slide_height = $("#slide_"+now+"").height();
          // $(swiper.wrapperEl).css('height',slide_height+"px");
          // console.log('현재 슬라이드 : '+ now + '\r' +', 목적 슬라이드 : ' + target+', 슬라이드 높이 : ' + slide_height+', 슬라이드 넓이 : ' + swiper.width);
        }

      } // --end of if
      
    } // --end of else
  });
}

// 페이지네이션 요소들에 속성을 추가한다.
function bullet_add_attr(type) {
  $('.swiper-pagination-bullet').each(function(index,item){
    $(item).attr({"onclick":"check_active_index(this)",  // 클릭 이벤트 메소드를 추가한다.
                  "type": type // 슬라이드 유형 속성을 추가한다.
                });
  });
}

// 페이징 요소를 클릭하면 해당 인덱스를 적용하여 url을 업데이트 한다.
function check_active_index(obj){
  var active_index = $(obj).text();  // 슬라이드 번호를 가져온다.
  var type = $(obj).attr('type'); // 슬라이드 유형 속성값을 가져온다.
  update_url(type,active_index);
  document.getElementById('header').scrollIntoView(true); // 화면 상단으로 스크롤 한다.
}

// 포스트의 조회수를 업데이트 한다.
function update_view_count(post_slug) {

      // console.log('목적 포스트 slug : '+post_slug);
      $.ajax({
        // 서버와  ajax()함수 사이의 http 통신 정보
        url: '/postmanage/updateviewcount',   //정보 처리를 할 서버 파일 위치
        type: 'post',                             //정보 전달 방식
        data: {                                   //서버에 전달될 정보 (변수이름 : 값)
    
          post_slug : post_slug
    
        },
        dataType: 'json',                         //처리 후 돌려받을 정보의 형식
        async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
        cache: false,                             //데이터 값을 캐싱 할건지 여부
        success: function(data) {
          // $('#loading').fadeOut(200);
    
          if (data.code != 0) {
            alert(data.message + '\n(오류코드 : ' + data.code + ')');
            return;
          }
          
          var result_array = data.extra;
          var new_count = result_array.view_count;
          // 업데이트된 view_count를 출력한다.
          // console.log('조회수가 업데이트 되었습니다. : '+new_count);
          // 조회수 표시 요소 내용을 업데이트 한다.
          $("#view_count_"+slug+"").text(" : "+new_count);
          var protocol = window.location.protocol; // 프로토콜을 가져온다.
          var host = window.location.hostname; //호스트 네임을 가져온다.
          var href = window.location.href; // 호스트의 전체 furl을 가져온다.
          var url_temp = '/postmanage/detaillist/'+slug; // 주소창에 보여줄 url 정보를 생성한다.
          var params = '?'+href.split('?')[1]; // 쿼리 스트링을 만들어 준다.
          var url_status = protocol+"//"+host+url_temp+params;
          ///////////////////////////////////////////////////////////////////////////////
          history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
          ///////////////////////////////////////////////////////////////////////////////
          // 랭킹 포인트를 업데이트 한다.
          update_ranking_point(post_slug)

        },
        error: function(err) {
          // $('#loading').fadeOut(200);    
          var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
          alert('내부 오류가 발생하였습니다.' + error_message);
        }
      }); // --end of ajax
  
}

// 슬라이드시 url을 변경해준다.
function update_url(type,arg) {
  if(type == 'post') {
    var protocol = window.location.protocol; // 프로토콜을 가져온다.
    var hostname = window.location.hostname;
    var params = window.location.search;
    if(params == null) { // 최초 로딩 화면에서 스와이프 시 기본 파라미터값을 설정해준다.
      params = Array('page=1')[0];
      // console.log(params);
    }
    var params_array = params.split('&'); // params를 '&'로 자른다.

    // console.log('params_array : '+ params_array);
    params_array.shift(); // 첫번째 요소를 제거하고 
    // console.log('params_array : '+ params_array);
    params_part = params_array.join('&'); // 나머지 요소를 '&'를 구분자로 하여 join한다.
    var url_status = protocol+"//"+hostname+'/postmanage/detaillist/'+arg+'?page='+cur_page+'&'+params_part; // 주소창에 나타낼 url을 만들어 준다.
    // console.log('url_status : '+ url_status);
    ///////////////////////////////////////////////////////////////////////////////
    history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
    ///////////////////////////////////////////////////////////////////////////////
   
  } else {
    var protocol = window.location.protocol; // 프로토콜을 가져온다.
    var hostname = window.location.hostname;
    var params = window.location.search;
    if(params == null) { // 최초 로딩 화면에서 스와이프 시 기본 파라미터값을 설정해준다.
      params = Array('page=1')[0];
      // console.log(params);
    }
    var params_array = params.split('&'); // params를 '&'로 자른다.
    if(params_array.length > 1){ // 첫번째 요소를 제거하고 
      // console.log('params_array : '+ params_array);
      params_array.shift(); // 첫번째 요소를 제거하고 
      // console.log('params_array : '+ params_array);
      params_part = params_array.join('&'); // 나머지 요소를 '&'를 구분자로 하여 join한다.
      var url_status = protocol+"//"+hostname+'/postmanage/index?page='+arg+'&'+params_part; // 주소창에 나타낼 url을 만들어 준다.
      // console.log('url_status : '+ url_status);
      ///////////////////////////////////////////////////////////////////////////////
      history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
      ///////////////////////////////////////////////////////////////////////////////
    } else { // 'page' 이외의 파라미터값이 없는 경우
      var url_status = protocol+"//"+hostname+'/postmanage/index?page='+arg;
      // console.log('url_status : '+ url_status);
      ///////////////////////////////////////////////////////////////////////////////
      history.pushState('', '', url_status);  // 서버 호출없이 주소창에 url만 변경한다.
      ///////////////////////////////////////////////////////////////////////////////
    }
  }
  document.getElementById('header').scrollIntoView(true); // 화면 상단으로 스크롤 한다.
}

// 사이드바 메뉴를 슬라이드 한다.
function navbar_slide(){
  // console.log("버튼을 클릭 했습니다");
  if (document.querySelector(".nav_side_menu").classList.contains("on")) { 
    //메뉴 slideOut 
    document.querySelector(".nav_side_menu").classList.remove("on");
    //slideOut시 menuBtn의 img src를 menu icon으로 변경 
    document.getElementById("hamburger").innerHTML = 
      '<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">'+
      '<path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/></svg>';
    
    document.querySelector("#hamburger").classList.remove("btnRotate");
    
    // 슬라이드 이외의 영역 처리를 위해 추가한 요소를 제거한다.
    document.querySelector("#dimmed").remove();

  } else { 
    //메뉴 slideIn 
    document.querySelector(".nav_side_menu").classList.add("on");
    //slideIn시 menuBtn의 img src를 cross icon으로 변경
    document.getElementById("hamburger").innerHTML = 
      '<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">' +
      '<path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/></svg>';

    document.querySelector("#hamburger").classList.add("btnRotate");

    // 슬라이드 이외의 영역 처리를 위한 요소를 추가한다.
    let div = document.createElement("div"); 
    div.id = "dimmed"; 
    document.body.append(div);

    //페이지 스크롤 락 모바일 이벤트 차단 
    document 
      .querySelector("#dimmed") 
      .addEventListener( "scroll touchmove touchend mousewheel", function (e) { 
        e.preventDefault(); 
        e.stopPropagation(); 
        return false; 
        } 
      );
  }
}
// 저장된 댓글 요소를 추한다.
function input_comment(comment,post_slug) {
  // console.log(comment);
  var comment_list_item_id = 'comment_list_item_'+post_slug;
  var word_count = text_to_word(comment.post_comment).length; // 단어의 개수를 센다.
  var br_count = count_str(comment.post_comment,'\n'); // 엔터문자의 개수를 센다.
  var mark = 'comment';
  // console.log(comment_list_item_id);
  // $("#"+comment_list_id+"").empty();
  $('#toast_message').remove(); // 기존의 토스트 메세지 요소를 제거한다.
  if(word_count > 4 || br_count > 0 ) {
    // console.log('단어수가 5개 이상이거나 엔터문자가 1개 이상인 경우');
    $("#"+comment_list_item_id+"").append(
      '<!-- 코멘트 내용 -->'+
      '<div class="border-top" id="comment_'+comment.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+              
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+
                '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                  '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                  '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                '</svg>'+
                '<span class="ms-1" style="font-size:.75rem; font-weight:bolder" title="'+comment.comment_nickname+'">'+
                comment.comment_nickname+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="mr-2 text-dark" style="font-size:.5rem;">'+
                comment.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_'+comment.idx+'">'+
                nl2br(word_limit(comment.post_comment,4)) +
                
                '<div>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="unfold_comment(this)">자세히 보기</span>'+
                '</div>'+
              
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_'+comment.idx+'">'+
                nl2br(comment.post_comment)+
                
                '<div>'+						
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="fold_comment(this)">간략히 보기</span>'+
                '</div>'+
                
              '</div>'+
              '<!-- 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_'+comment.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_'+comment.idx+'">'+
                  '<label class="form-check-label me-3" for="is_secret_'+comment.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="fold_comment(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_'+comment.idx+'" name="comment_modify_'+comment.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment.post_comment+'</textarea>'+
                  '<label for="comment_modify_'+comment.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment.idx+'" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_'+comment.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="passwd_check_'+comment.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_'+comment.idx+'" idx="'+comment.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="'+comment.idx+'" title="댓글 남기기">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                        '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="modify_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="'+comment.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="delete_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" post_slug ="'+post_slug+'" mark="comment" title="삭제">'+
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
              '<!-- 댓글에 대한 댓글 등록 요소  -->'+
              '<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_'+comment.idx+'">'+
                '<div class="row mb-2">'+
                  '<div class="col-3 pe-0">'+
                    '<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_'+comment.idx+'" name="comment_reply_nickname_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 pe-0">'+
                    '<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_'+comment.idx+'" name="comment_reply_passwd_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">'+
                    '<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_'+comment.idx+'" name="comment_reply_is_secret_'+comment.idx+'">'+
                    '<label class="form-check-label" for="comment_reply_is_secret_'+comment.idx+'">'+
                      '<span style="font-size:.75rem;">비밀글</span>'+
                    '</label>'+
                  '</div>'+
                '</div>'+
                '<div class="row">'+
                  '<div class="col-9 form-floating d-flex justify-content-center pe-0">'+
                    '<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_'+comment.idx+'" name="post_comment_reply_'+comment.idx+'" style="height: 100px; width:98%;" ></textarea>'+
                    '<label class="ps-4" for="post_comment_reply_'+comment.idx+'">댓글에 댓글 남기기</label>'+
                  '</div>'+
                  '<div class="col-3 d-grid ps-0">'+
                    '<button type="button" class="btn btn-outline-primary" slug="'+post_slug+'" org_idx="'+comment.idx+'" onclick="save_comment_reply(this)" style="width:95%;">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+												
          '</div>'+
        '</div>'+
        '<!-- end of comment -->'+
        '<!-- 댓글에 대한 댓글 내용 -->'+
        '<div id="comment_reply_list_'+comment.idx+'">'+
        '</div>'+
        '<!-- end of comment_reply_list -->'+
        '<input type="hidden" id="comment_reply_count_'+comment.idx+'" value="0" >'+
      '</div>'+
      '<!-- end of comment -->'

  );
  } else {
    // console.log('단어수가 5개 미만이거나 엔터문자가 없는 경우');
    $("#"+comment_list_item_id+"").append(
        '<!-- 코멘트 내용 -->'+
        '<div class="border-top" id="comment_'+comment.idx+'">'+
          '<!-- 토스트 메세지 요소 -->'+
          '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
            '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
              '<div class="d-flex justify-content-center">'+
                '<div class="toast-body">'+
                  '댓글이 추가 되었습니다.'+
                '</div>'+
                '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
              '</div>'+
            '</div>'+
          '</div>'+    
          '<div class="row no-gutters mt-2 mx-0 px-2">'+
            '<div class="col-md-12">'+
              '<div class="d-flex align-items-center mx-0 p-0">'+
                '<p class="text-center">'+
                  '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                    '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                    '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                  '</svg>'+
                  '<span class="ms-1" style="font-size:.75rem; font-weight:bolder" title="'+comment.comment_nickname+'">'+
                  comment.comment_nickname+
                  '</span>'+
                  '&nbsp;&nbsp;'+
                  '<small class="mr-2 text-dark" style="font-size:.5rem;">'+
                  comment.reg_date+
                  '</small>'+
                '</p>'+
              '</div>'+
              '<div class="row">'+

                '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_'+comment.idx+'">'+
                  nl2br(word_limit(comment.post_comment,4)) +
                  
                '</div>'+
                '<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_'+comment.idx+'">'+
                  nl2br(comment.post_comment)+
                  
                '</div>'+
                '<!-- 댓글 수정을 위한 textarea hidden 요소 -->'+
                '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_'+comment.idx+'">'+
                  '<div class="col-12 form-check d-flex align-items-center">'+
                    '<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_'+comment.idx+'">'+
                    '<label class="form-check-label me-3" for="is_secret_'+comment.idx+'">'+
                      '<span style="font-size:.75rem;">비밀글</span>'+
                    '</label>'+
                    '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" onclick="fold_comment(this)">취소</span>'+
                    
                  '</div>'+
                  '<div class="form-floating">'+
                    '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_'+comment.idx+'" name="comment_modify_'+comment.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment.post_comment+'</textarea>'+
                    '<label for="comment_modify_'+comment.idx+'">댓글 수정하기</label>'+
                  '</div>'+
                  '<br>'+									
                '</div>'+
                
                '<!-- 댓글 수정,삭제 관련 요소 -->'+
                '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                  '<div class="row d-flex justify-content-center px-0">'+
                    '<div class="d-flex justify-content-center" style="height:80%;" >'+
                      '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment.idx+'" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_'+comment.idx+'">'+
                        '<span class="info_text" title="저장">'+
                          '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                            '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                          '</svg>'+
                        '</span>'+
                      '</button>'+
                    '</div>'+
                    '<div class="comment_pw_check mb-1" id="passwd_check_'+comment.idx+'">'+
                      '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_'+comment.idx+'" idx="'+comment.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                    '</div>'+
                  '</div>'+
                  '<div class="d-flex justify-content-center align-items-end">'+
                    '<div class="comment_icon_div me-2">'+
                      '<span class="me-2 comment-icon" id="reply_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="'+comment.idx+'" title="댓글 남기기">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</div>'+
                    '<div class="comment_icon_div me-2">'+
                      '<span class="me-2 comment-icon" id="modify_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="'+comment.idx+'" title="수정">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                          '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                          '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</div>'+
                    '<div class="comment_icon_div del_btn" id="delete_button_'+comment.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment.idx+'" post_slug ="'+post_slug+'" mark="comment" title="삭제">'+
                      '<p class="trash_cap_wrap text-center">'+
                        '<span class="trash_cap py-0 my-0"></span>'+
                      '</p>'+
                      '<span class="trash comment_icon">'+
                      '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '<!-- 댓글에 대한 댓글 등록 요소  -->'+
              '<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_'+comment.idx+'">'+
                '<div class="row mb-2">'+
                  '<div class="col-3 pe-0">'+
                    '<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_'+comment.idx+'" name="comment_reply_nickname_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 pe-0">'+
                    '<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_'+comment.idx+'" name="comment_reply_passwd_'+comment.idx+'" value="">'+
                  '</div>'+
                  '<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">'+
                    '<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_'+comment.idx+'" name="comment_reply_is_secret_'+comment.idx+'">'+
                    '<label class="form-check-label" for="comment_reply_is_secret_'+comment.idx+'">'+
                      '<span style="font-size:.75rem;">비밀글</span>'+
                    '</label>'+
                  '</div>'+
                '</div>'+
                '<div class="row">'+
                  '<div class="col-9 form-floating d-flex justify-content-center pe-0">'+
                    '<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_'+comment.idx+'" name="post_comment_reply_'+comment.idx+'" style="height: 100px; width:98%;" ></textarea>'+
                    '<label class="ps-4" for="post_comment_reply_'+comment.idx+'">댓글에 댓글 남기기</label>'+
                  '</div>'+
                  '<div class="col-3 d-grid ps-0">'+
                    '<button type="button" class="btn btn-outline-primary" slug="'+post_slug+'" org_idx="'+comment.idx+'" onclick="save_comment_reply(this)" style="width:95%;">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+												
          '</div>'+
        '</div>'+
        '<!-- end of comment -->'+
        '<!-- 댓글에 대한 댓글 내용 -->'+
        '<div id="comment_reply_list_'+comment.idx+'">'+
        '</div>'+
        '<!-- end of comment_reply_list -->'+
        '<input type="hidden" id="comment_reply_count_'+comment.idx+'" value="0" >'+
      '</div>'+
      '<!-- end of comment -->'
    );
  }
  trash_init_each(mark,comment.idx);
}

// 저장된 댓글에 대한 댓글 요소를 추가한다.
function input_comment_reply(comment_reply,org_idx) {
  var comment_reply_list_id = 'comment_reply_list_'+org_idx;
  var word_count = text_to_word(comment_reply.post_comment_reply).length; // 단어의 개수를 센다.
  var br_count = count_str(comment_reply.post_comment_reply,'\n'); // 엔터문자의 개수를 센다.
  var mark = 'comment_reply';
  // console.log(comment_reply_list_id);
  // $("#"+comment_reply_list_id+"").empty();
  $('#toast_message').remove(); // 기존의 토스트 메세지 요소를 제거한다.
  if(word_count > 4 || br_count > 0 ) {
    // console.log('단어수가 5개 이상이거나 엔터문자가 1개 이상인 경우');
    $("#"+comment_reply_list_id+"").append(
      '<div class="border-top comment_reply_content" id="comment_reply_'+comment_reply.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글에 대한 댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+                                            
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+                                                    
              '<span class="text-primary me-1">'+
                '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">'+
                  '<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>'+
                '</svg>'+
              '</span>'+  
              '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                  '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                  '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                '</svg>'+
                '<span class="ms-1" style="font-size:.75rem; font-weight:bolder" title="'+comment_reply.comment_reply_nickname+'">'+
                comment_reply.comment_reply_nickname+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="mr-2 text-dark" style="font-size:.5rem;">'+
                comment_reply.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 ps-5" style="font-size:.75rem;" id="short_comment_reply_'+comment_reply.idx+'">'+
                nl2br(word_limit(comment_reply.post_comment_reply,4)) +
                '<div>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="unfold_comment_reply(this)">자세히 보기</span>'+
                '</div>'+
              
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_'+comment_reply.idx+'">'+
                nl2br(comment_reply.post_comment_reply)+
                
                '<div>'+						
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="fold_comment_reply(this)">간략히 보기</span>'+
                '</div>'+
                
              '</div>'+
              '<!-- 댓글에 대한 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_'+comment_reply.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_'+comment_reply.idx+'">'+
                  '<label class="form-check-label me-3" for="reply_is_secret_'+comment_reply.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="fold_comment_reply(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_'+comment_reply.idx+'" name="comment_reply_modify_'+comment_reply.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment_reply.post_comment_reply+'</textarea>'+
                  '<label for="comment_reply_modify_'+comment_reply.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">	'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment_reply.idx+'" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_'+comment_reply.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="reply_passwd_check_'+comment_reply.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_'+comment_reply.idx+'" idx="'+comment_reply.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_modify_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="'+comment_reply.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="reply_delete_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" mark="comment_reply" title="삭제">'+									
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+	
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<!-- end of comment_reply -->'
    );
  } else {
    // console.log('단어수가 5개 미만이거나 엔터문자가 없는 경우');
    $("#"+comment_reply_list_id+"").append(
      '<div class="border-top comment_reply_content" id="comment_reply_'+comment_reply.idx+'">'+
        '<!-- 토스트 메세지 요소 -->'+
        '<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" id="toast_message">'+
          '<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">'+
            '<div class="d-flex justify-content-center">'+
              '<div class="toast-body">'+
                '댓글에 대한 댓글이 추가 되었습니다.'+
              '</div>'+
              '<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'+
            '</div>'+
          '</div>'+
        '</div>'+                                            
        '<div class="row no-gutters mt-2 mx-0 px-2">'+
          '<div class="col-md-12">'+
            '<div class="d-flex align-items-center mx-0 p-0">'+
              '<p class="text-center">'+
                '<span class="text-primary me-1">'+
                  '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">'+
                    '<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>'+
                  '</svg>'+
                '</span>'+
                '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">'+
                  '<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>'+
                  '<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>'+
                '</svg>'+
                '<span class="ms-1" style="font-size:.75rem; font-weight:bolder" title="'+comment_reply.comment_reply_nickname+'">'+
                comment_reply.comment_reply_nickname+
                '</span>'+
                '&nbsp;&nbsp;'+
                '<small class="mr-2 text-dark" style="font-size:.5rem;">'+
                comment_reply.reg_date+
                '</small>'+
              '</p>'+
            '</div>'+
            '<div class="row">'+

              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_'+comment_reply.idx+'">'+
                nl2br(word_limit(comment_reply.post_comment_reply,4)) +
              
              '</div>'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_'+comment_reply.idx+'">'+
                nl2br(comment_reply.post_comment_reply)+
                
              '</div>'+
              '<!-- 댓글에 대한 댓글 수정을 위한 textarea hidden 요소 -->'+
              '<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_'+comment_reply.idx+'">'+
                '<div class="col-12 form-check d-flex align-items-center">'+
                  '<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_'+comment_reply.idx+'">'+
                  '<label class="form-check-label me-3" for="reply_is_secret_'+comment_reply.idx+'">'+
                    '<span style="font-size:.75rem;">비밀글</span>'+
                  '</label>'+
                  '<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" onclick="fold_comment_reply(this)">취소</span>'+
                  
                '</div>'+
                '<div class="form-floating">'+
                  '<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_'+comment_reply.idx+'" name="comment_reply_modify_'+comment_reply.idx+'" style="height: 100px; font-size:.75rem;" value="">'+comment_reply.post_comment_reply+'</textarea>'+
                  '<label for="comment_reply_modify_'+comment_reply.idx+'">댓글 수정하기</label>'+
                '</div>'+
                '<br>'+									
              '</div>'+
              
              '<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->'+
              '<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">'+
                '<div class="row d-flex justify-content-center px-0">	'+
                  '<div class="d-flex justify-content-center" style="height:80%;" >'+
                    '<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="'+comment_reply.idx+'" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_'+comment_reply.idx+'">'+
                      '<span class="info_text" title="저장">'+
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">'+
                          '<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>'+
                        '</svg>'+
                      '</span>'+
                    '</button>'+
                  '</div>'+
                  '<div class="comment_pw_check mb-1" id="reply_passwd_check_'+comment_reply.idx+'">'+
                    '<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_'+comment_reply.idx+'" idx="'+comment_reply.idx+'" placeholder="비밀번호" maxlength="4" title="" value="">'+
                  '</div>'+
                '</div>'+
                '<div class="d-flex justify-content-center align-items-end">'+
                  '<div class="comment_icon_div me-2">'+
                    '<span class="me-2 comment-icon" id="reply_modify_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="'+comment_reply.idx+'" title="수정">'+
                      '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">'+
                        '<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>'+
                        '<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>'+
                      '</svg>'+
                    '</span>'+
                  '</div>'+
                  '<div class="comment_icon_div del_btn" id="reply_delete_button_'+comment_reply.idx+'" style="font-size:.5rem; cursor:pointer;" idx="'+comment_reply.idx+'" mark="comment_reply" title="삭제">'+									
                    '<p class="trash_cap_wrap text-center">'+
                      '<span class="trash_cap py-0 my-0"></span>'+	
                    '</p>'+
                    '<span class="trash comment_icon">'+
                    '</span>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<!-- end of comment_reply -->'
    );
  }
  trash_init_each(mark,comment_reply.idx);
}

// 단어수를 제한하여 보여준다.
function word_limit(content,count) {
  var text_array = content.split(' '); // 문자열을 공백(단어) 단위로 자른다.
  var word_count = text_to_word(content).length;
  var new_text = ''; // 새로 생성할 문자열을 선언한다.
  if(word_count > count) { // 단어수가 count를 초과할 경우
    for(i=0; i < count; i++) {
      if(i < (count-1)) {
        new_text += text_array[i] + ' '; // 단어 배열의 각 요소를 새로 생성한 문자열에 담는다.
      } else {
        new_text += text_array[i];
      }
    }
    new_text += '...';
    // console.log(new_text+' , '+word_count);

    return new_text;
  } else {
    // console.log(content+' , '+word_count);

    return content;
  }
}

// 현재 브라우저 높이를 구한다.
function get_browser_height() {
  var userAgent = navigator.userAgent.toLowerCase();
 
  var browser = {
    msie    : /msie/.test( userAgent ) && !/opera/.test( userAgent ),
    safari  : /webkit/.test( userAgent ),
    firefox : /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent ),
    opera   : /opera/.test( userAgent )
  };   
 
  var nFinalHeight = 0;
 
  if( browser.msie ){ // 인터넷 익스플로러
    var scrollHeight = document.documentElement.scrollHeight;
    var browserHeight = document.documentElement.clientHeight;
  
    nFinalHeight  = scrollHeight < browserHeight ? browserHeight : scrollHeight;
 
  } else if ( browser.safari ){ //Chrome 또는 Safari, 같은 엔진을 사용합니다
    nFinalHeight   = document.body.scrollHeight;
  
  } else if ( browser.firefox ){ // Firefox 또는 NS
    var bodyHeight = document.body.clientHeight;
 
    nFinalHeight  = window.innerHeight < bodyHeight ? bodyHeight : window.innerHeight;
 
  } else if ( browser.opera ){ // Opera
    var bodyHeight = document.body.clientHeight;
 
    nFinalHeight = window.innerHeight < bodyHeight ? bodyHeight : window.innerHeight;
 
  } else { 
    alert("지원하지 않는 브라우저입니다.");
  }
   
  return nFinalHeight;
  // return window.innerHeight;

}

// 임의좌표 클릭하기
function simulate_click(x,y) {
  var event = document.createEvent('MouseEvent');
  event.initMouseEvent("click",true,true,window,0,0,0,0,0,false,false,false,false,0,null);
  var cb = document.elementFromPoint(x,y);
  cb.dispatchEvent(event);
  // console.log('('+x+' , '+y+') 좌표를 클릭했습니다.');
}

// 특정 키보드 누른 효과 발생
function clickevent()
{
// var e = $.Event("keydown");
// e.which = 9;
// e.keyCode = 9;
// $(document).trigger(e);

// console.log('tab을 눌렀습니다');
}

// 토스트 메세지 초기화 및 화면에 표시하기
function toast_init() {
  var toastElList = [].slice.call(document.querySelectorAll('.toast'))
  var toastList = toastElList.map(function (toastEl) {
    return new bootstrap.Toast(toastEl, {
      animation : true,
      autohide : true,
      delay : 5000 // 5초 후 사라짐
    })
  })
  // console.log(toastList);
  toastList[0].show();
}

// 랭킹 포인트를 업데이트 한다.
function update_ranking_point(slug) {
  $.ajax({
    // 서버와  ajax()함수 사이의 http 통신 정보
    url: '/postmanage/updaterankingpoint',   //정보 처리를 할 서버 파일 위치
    type: 'post',                             //정보 전달 방식
    data: {                                   //서버에 전달될 정보 (변수이름 : 값)

      slug : slug

    },
    dataType: 'json',                         //처리 후 돌려받을 정보의 형식
    async: true,                              //서버와 프론트단의 동기화를 하지 않을건지 여부 (true:동기화 안함 / false:동기화 함)
    cache: false,                             //데이터 값을 캐싱 할건지 여부
    success: function(data) {
      // $('#loading').fadeOut(200);

      if (data.code != 0) {
        alert(data.message + '\n(오류코드 : ' + data.code + ')');
        return;
      }
      
      var result_array = data.extra;
      $('#comment_save_btn_'+slug+'').blur(); // 댓글 저장 버튼에서 포커스를 아웃 시킨다.
      // const button = document.querySelector('#comment_save_btn_'+slug+'');
      // const mouseleaveEvent = new Event('mouseleave', {bubbles: true, cancelable: true }); // 이벤트 객체를 생성하고
      // button.addEventListener('mouseleave',function(){
      //   console.log('mouse가 요소에서 벗어났습니다.');
      // });
      // button.dispatchEvent(mouseleaveEvent); // 생성된 이벤트를 실행 시킨다. (버튼을 클릭하지 않고 버튼에 등록된 이벤트를 실행)

      // console.log(result_array.slug+'의 랭킹 포인트가 '+result_array.ranking_point+'점으로 업데이트 되었습니다.');
      
    },
    error: function(err) {
      // $('#loading').fadeOut(200);

      var error_message = (err ? '\n(' + err.status + ': ' + err.statusText + ')' : '');
      alert('내부 오류가 발생하였습니다.' + error_message);
    }
  });
}

// viewer 객체 생성
function viewer_init() {
  var viewer_item_arr = document.querySelectorAll('.img-fluid');
  // console.log(viewer_item_arr);
  if(viewer_item_arr.length > 0){ // 콘텐츠 내에 이미지 요소가 있는 경우
    // jquery
    // $.each(viewer_item_arr,function(index,item){
    //   $(item).closest('div').addClass('viewer-item');
    // });
    // pure javascript
    viewer_item_arr.forEach(function(item){
      var parent = item.parentNode;
      parent.classList.add('viewer-item');
    });
    var viewer_items = document.querySelector('.viewer-items');
    viewer = new Viewer(viewer_items, {
      title : 0,           // 이미지 제목(파일명) 표시하기 않도록 설정.
      backdrop : 'static', // 모달 배경 영역을 클릭해도 viewer가 종료 되지 않도록 설정.
      // movable : false,
      toolbar: {
        zoomIn: 0,
        zoomOut: 0,
        oneToOne: 0,
        reset: {
          show: 1,
          size: 'large',
        },
        prev: {
          show: 1,
          size: 'large',
        },
        play: {
          show: 1,
          size: 'large',
        },
        next: {
          show: 1,
          size: 'large',
        },
        rotateLeft: 0,
        rotateRight: 0,
        flipHorizontal: 0,
        flipVertical: 0      
      },
      // backdrop : false
      view(event) {
        // console.log(event.detail.index);
      },
      viewed(event) {
        // console.log('이미지를 로딩했습니다.');
      },
    });
    // console.log('viewer가 생성되었습니다.');
  }
}

// popovers 초기화 
function popovers_init() {
  var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
  var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    var el_id = popoverTriggerEl.id;
    // popoverTriggerEl에 이벤트를 추가한다.
    var el_instance = document.getElementById(el_id); // 개별 popoverTrigger요소의 인스턴스를 생성한다.
    el_instance.addEventListener('show.bs.popover', function () { // popover를 나타내는 메서드가 호출될때 실행
      // console.log(el_id+'의 popover가 열립니다.');
    });

    el_instance.addEventListener('inserted.bs.popover', function () { // popover를 나타내기 위한 요소가 DOM에 추가됐을때 실행
      // console.log(el_id+'의 popover 요소가 추가되었습니다.');
    });

    el_instance.addEventListener('shown.bs.popover', function () { // popover가 나타난 이후 실행
      // console.log(el_id+'의 popover가 열렸습니다.');
    });

    el_instance.addEventListener('hide.bs.popover', function () { // popover가 사라지기 시작할 때 실행
      // console.log(el_id+'의 popover가 닫힙니다.');
    });

    el_instance.addEventListener('hidden.bs.popover', function () { // popover가 사라진 후 이후 실행
      // console.log(el_id+'의 popover가 닫혔습니다.');
    });
    
    var popover = new bootstrap.Popover(popoverTriggerEl, {
      // template : '<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-header"></h3><div class="popover-body info_text"></div></div>',
      html: true, // popover 요소안에 html 태그 사용
      customClass : 'info_text',  // 로딩된 *.css 파일의 클래스 이름을 추가하면 popover 요소 전체에 해당 스타일이 적용된다.
      popperConfig: function (defaultBsPopperConfig) {
        var newPopperConfig = {
          // popper 설정값을 넣는다.
        }
        // use defaultBsPopperConfig if needed...
        // console.log(defaultBsPopperConfig);
        return newPopperConfig;
      }
    });
    return popover;
  });

  // console.log(popoverList);
}