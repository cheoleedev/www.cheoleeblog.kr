
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-light px-0" >
		<?= $this->session->flashdata('message') ?>
		<div class="col-sm-12 col-md-12 col-lg-12 px-2">
			<div class="pt-2">
				<svg class="bi me-1" width="16" height="16"><use xlink:href="#post_manage"/></svg>
				<span class="fs-5 fw-bold me-3">포스트 목록</span>
				
			</div>
			<hr>
			<!-- 검색 기능 요소 -->
			<form action="/postmanage/index" enctype="multipart/form-data" method="post" accept-charset="utf-8">
				<div class="d-flex justify-content-end mb-3">
					<div class="col-6 col-sm-6 col-md-4 col-lg-4 me-1">
						<select class="form-select form-select-sm" aria-label=".form-select-sm example" id="filter" name="filter">
						<option value="title" <?= $filter == 'title' ? 'selected' : '' ?>>제목</option>
						<option value="foreword" <?= $filter == 'foreword' ? 'selected' : '' ?>>머리말</option>
						<option value="writer_id" <?= $filter == 'writer_id' ? 'selected' : '' ?>>작성자</option>
						<option value="content" <?= $filter == 'content' ? 'selected' : '' ?>>내용</option>
						</select>
					</div>
					<div class="col-6 col-sm-6 col-md-4 col-lg-4 d-flex">
						<input class="form-control form-control-sm me-1" type="search" placeholder="검색어 입력" aria-label="Search" id="search" name="search" value="<?= $search ?>">
						<button class="btn btn-sm btn-outline-primary" type="submit"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#search"/></svg></button>
					</div>
				</div>
			</form>
			<!-- 검색 기능 끝 -->
			<!-- --start of swiper-container -->
			<div class="post_list_wrap" id="post_list">
				<!-- --start of swiper-container -->
				<div class="swiper-container">
					<!-- --start of swiper-wrapper -->
					<div class="swiper-wrapper mb-5">	
						<!-- content -->
						<?php
							if(count($posts) === 0) { // 검색결과가 없을 경우 상단 정보만 표시한다.
						?>
						<!-- --start of swiper-slide -->
						<div class="swiper-slide slide_<?= $cur_page ?>">
							<nav class="border-bottom mb-1" aria-label="breadcrumb" style="">
								<div class="d-flex justify-content-end align-items-center">
									<!-- breadcrumb(사이트 이동 경로) -->
									<div id="clock" class="col-3">
										<span style="font-size:.75rem;">(총</span>
										<span class="fw-bold mx-1" style="font-size:.75rem; color:blue;">0</span>
										<span class="me-3" style="font-size:.75rem;">개)</span>
									</div>
									<div id="page_num" class="col-3">	
										<span class="fw-bold mx-1 me-1" style="font-size:.75rem; color:blue;"><?= $i+1  ?></span>
										<span style="font-size:.75rem;">페이지</span>
										<span class="me-1" style="font-size:.75rem;">of</span>
										<span class="fw-bold mx-1" style="font-size:.75rem;"><?= $all_page ?></span>
									</div>
									<div class="d-flex align-items-end justify-content-end col-6">
										<!-- <span class="border rounded bg-secondary bg-gradient text-white" style="margin-bottom: 1rem; margin-right: 0.5rem; padding: 0 10px; font-size:0.85rem">위치</span> -->
										<ol class="breadcrumb mb-0">
											<li class="breadcrumb-item d-inline-block align-middle"><span class="text-primary " url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></span></li>
											<!-- <li class="breadcrumb-item d-inline-block align-middle"><a class="text-primary " href="/postmanage/postlist" url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></a></li> -->
											<?php
												if($breadcrumb_main != null) { // 메인 주제 변수값이 있을 경우
											?>
											<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_main" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="?main=<?= $breadcrumb_main ?>" parent="<?= $breadcrumb_main ?>" is_main="Y"><?= $breadcrumb_main ?></span></li>
											<?php
												}
												if($breadcrumb_sub != null) { // 하위 주제 변수값이 있을 경우
											?>
											<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_sub" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="?main=<?= $breadcrumb_main?>&sub=<?= $breadcrumb_sub?>" parent="<?= $breadcrumb_main ?>" is_main="N" name="<?= $breadcrumb_sub ?>"><?= $breadcrumb_sub ?></span></li>
											<?php
												}
												if($breadcrumb_foreword != null) { // 머리말 변수값이 있을 경우
											?>
											<li class="breadcrumb-item active" aria-current="page"><span id="breadcrumb_foreword" style="font-size:.75rem;"><?= $breadcrumb_foreword ?></span></li>
											<?php
												}
											?>
										</ol>
									</div>						
								</div>
							</nav>
						</div>
						<!-- -- end of slide -->
						<?php
							} else {
							for($i=0; $i < $all_page; $i++) {
								$start_index = $i*$posts_per_page;
						?>
						<!-- --start of swiper-slide -->
						<div class="swiper-slide slide_<?= $cur_page ?>">
							<nav class="border-bottom mb-1" aria-label="breadcrumb" style="">
								<div class="d-flex justify-content-end align-items-center">
									<!-- breadcrumb(사이트 이동 경로) -->
									<div id="clock" class="col-5">
										<span style="font-size:.75rem;">(총</span>
										<span class="fw-bold mx-1" style="font-size:.75rem; color:blue;"><?= $all_post ?></span>
										<span class="me-3" style="font-size:.75rem;">개)</span>
										<span class="fw-bold mx-1 me-1" style="font-size:.75rem; color:blue;"><?= $i+1 ?></span>
										<span class="me-1" style="font-size:.75rem;">of</span>
										<span class="fw-bold" style="font-size:.75rem;"><?= $all_page ?></span>
										<span style="font-size:.75rem;">page</span>
									</div>
									<div class="d-flex align-items-end justify-content-end col-7">
										<!-- <span class="border rounded bg-secondary bg-gradient text-white" style="margin-bottom: 1rem; margin-right: 0.5rem; padding: 0 10px; font-size:0.85rem">위치</span> -->
										<ol class="breadcrumb mb-0">
											<li class="breadcrumb-item d-inline-block align-middle"><span class="text-primary " url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></span></li>
											<!-- <li class="breadcrumb-item d-inline-block align-middle"><a class="text-primary " href="/postmanage/postlist" url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></a></li> -->
											<?php
												if($breadcrumb_main != null) { // 메인 주제 변수값이 있을 경우
											?>
											<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_main" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="?main=<?= $breadcrumb_main ?>" parent="<?= $breadcrumb_main ?>" is_main="Y"><?= $breadcrumb_main ?></span></li>
											<?php
												}
												if($breadcrumb_sub != null) { // 하위 주제 변수값이 있을 경우
											?>
											<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_sub" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="?main=<?= $breadcrumb_main?>&sub=<?= $breadcrumb_sub?>" parent="<?= $breadcrumb_main ?>" is_main="N" name="<?= $breadcrumb_sub ?>"><?= $breadcrumb_sub ?></span></li>
											<?php
												}
												if($breadcrumb_foreword != null) { // 머리말 변수값이 있을 경우
											?>
											<li class="breadcrumb-item active" aria-current="page"><span id="breadcrumb_foreword" style="font-size:.75rem;"><?= $breadcrumb_foreword ?></span></li>
											<?php
												}
											?>
										</ol>
									</div>						
								</div>
							</nav>
							<!-- --end of breadcrumb -->
							<?php 
								// 현재 페이지의 포스트 목록을 생성한다.
								// $cur_num = 0;
								for($j=0; $j < $posts_per_page; $j++){ 
									$cur_num = $j + $start_index;
									if($cur_num == count($posts)){
										break;
									}
								// echo "현재 포스트 번호: {$cur_num}"." 전체 포스트 수".count($posts);
							?>
							<div class="card mb-2 shadow-sm">
								<div class="row no-gutters mt-2 mx-0 px-2">
									<div class="col-md-12">
										<span url="/postmanage/detail/<?=$posts[$cur_num]['slug']?>" onclick="post_detail(this)" class="text-dark text-decoration-none p-0 m-0" style="cursor:pointer;" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search?>" parent="<?= $posts[$cur_num]['main_subject_name'] ?>" is_main="N" name="<?= $posts[$cur_num]['sub_subject_name'] ?>">
											<div class="row d-flex align-items-center justify-content-between mx-0 p-0">
												<div class="row d-flex col-10">
													<div class="col-1 p-0 mx-0 d-flex justify-content-start">
														<img src="<?= '/img/post/'.$posts[$cur_num]['thumbnail'] ?>" class="img-thumbnail inline" width="30" alt="포스트 썸네일">
													</div>
													<div class="col-10 p-0 d-flex align-items-center mx-0 ">
														<span class="text-ellipsis inline align-middle" title="<?= '['.$posts[$cur_num]['foreword'].'] '.$posts[$cur_num]['title'] ?>">
															<?= 
																// word_limiter('['.$post->foreword.'] '.$post->title, 4)
																'['.$posts[$cur_num]['foreword'].'] '.$posts[$cur_num]['title'];
															?>
														</span>
													</div>
												</div>
												<div class="col-1 px-0">
													<span class="badge rounded-pill bg-primary">
														<?php
															// 포스트의 댓글 정보를 가져와 개수를 출력한다.
															$comments = $this->post->get_all_comment($posts[$cur_num]['slug']);
															echo count($comments);
														?>
													</span>
												</div>
											</div>
											<hr class="my-1">
											<div class="row d-flex align-items-center mx-0 p-0">
												<div class="col-sm-12 col-md-6 col-lg-6 inline px-0 ">
													<small class="mr-2">
														<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">작성자</i>
														<?= ' : '.$posts[$cur_num]['writer_id'] ?>
													</small>
													<small class="mr-2 float-end post-info-start">
														<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">조회수</i>
														<?= ' : '.$posts[$cur_num]['view_count'] ?>
													</small>					
												</div>
												<div class="col-sm-12 col-md-6 col-lg-6 inline px-0">
													<small class="mr-2 post-info-end">
														<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">주제</i>
														<?= ' : '.$posts[$cur_num]['main_subject_name'].' > '.$posts[$cur_num]['sub_subject_name']  ?>
													</small>
													<small class="mr-2 float-end">
														<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">등록일시</i>
														<?= ' : '.$posts[$cur_num]['reg_date']  ?>
													</small>
												</div>
											</div>				
											<hr class="mt-1">
											<?= word_limiter($posts[$cur_num]['content'], 4) ?>
										</span>					
									</div>
								</div>
							</div>
							<?php
									}
							?>
						</div>
							<!-- --end of swiper-slide -->
							<?php
								} // -- end of for (swiper-slide)
							} // -- end of else (count($posts) == 0)
							?>
					</div>
					<!-- --end of swiper-wrapper -->
					<div class="swiper-pagination page-item" id="slide_pagination"></div>
				</div>
				<!-- --end of swiper-container -->
			</div>
			<!-- --end of post_lsit -->

		</div>
	</div>
	<!-- --end of main content -->
	<script>
		var cur_post = ''; // 현재 페이지 index를 전역 변수에 담아준다.
		var all_page = <?= $all_page ?>	;// 마지막 페이지 index를 전역 변수에 담아준다.
		var all_post = ''; // 전체 포스트 수를 초기화 한다.
		swiper_init();
	</script>
	
	
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>