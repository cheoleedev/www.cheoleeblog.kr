<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		<symbol id="home" viewBox="0 0 16 16">
			<path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
		</symbol>
		<symbol id="speedometer" viewBox="0 0 16 16">
			<path d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2zM3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.389.389 0 0 0-.029-.518z"></path>
			<path fill-rule="evenodd" d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.945 11.945 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0z"></path>
		</symbol>
		<symbol id="speedometer2" viewBox="0 0 16 16">
    		<path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4zM3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.389.389 0 0 0-.029-.518z"></path>    
			<path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A7.988 7.988 0 0 1 0 10zm8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3z"></path>
		<symbol id="write" viewBox="0 0 16 16">
			<path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
		</symbol>
		<symbol id="post_manage" viewBox="0 0 16 16">
			<path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
  			<path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
		</symbol>
		<symbol id="setting" viewBox="0 0 16 16">
			<path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
  			<path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
		</symbol>
		<symbol id="search" viewBox="0 0 16 16">
			<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
		</symbol>
		<symbol id="key" viewBox="0 0 16 16">
			<path d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z"/>
			<path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
		</symbol>
		<symbol id="statics" viewBox="0 0 16 16">
			<path d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2zm1 12h2V2h-2v12zm-3 0V7H7v7h2zm-5 0v-3H2v3h2z"/>
		</symbol>
		<symbol id="lock" viewBox="0 0 16 16">
			<path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2zM5 8h6a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1z"/>
		</symbol>
	</svg>

	<header id="header" class="bd-navbar">
		<!-- 상단 메뉴 영역(반응형 햄버거 메뉴) -->
		<nav class="navbar navbar-dark fixed-top bg-dark">
		<!-- <nav class="navbar navbar-expand-md navbar-dark  bg-dark"> -->
			<div class="container-fluid nav_box">
				<a class="navbar-brand" href="/postmanage/index">
					<img class="me-2" src="/img/logo.png" width="25" alt="logo" title="logo"><?= $config->svc_title ?>
				</a>
				
				<!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="true" aria-label="Toggle navigation" id="hamburger"> -->
				<span class="" style="color:#fff; navbar-toggler" onclick="navbar_slide();" id="hamburger">
					<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
						<path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
					</svg>
				</span>
				<!-- </button> -->
			</div>
			<!-- <div class="collapse navbar-collapse" id="navbarCollapse"> -->
			<div class="nav_side_menu" id="navbar_slide">
				<ul class="navbar-nav me-auto mb-2 mb-md-0">
				<?php
					for($i=0; $i < count($main_subject_data); $i++) {
						$main = $main_subject_data[$i];
						$main_posts = array();
						$main_post_count = 0;
						for($f=0; $f < count($posts); $f++){
							if($main['main_subject_name'] == $posts[$f]['main_subject_name']){
								$main_post_count++;
								$main_posts[] = $posts[$f]; // 메인 주제에 해당하는 포스트를 새로운 배열에 담는다.
							}
						}
				?>
					<li class="nav-item dropdown">
						<!--상단 메뉴 클릭 시 사이드바 메뉴와 동기화 되도록하기 위해 속성을 추가한다.  
							. parent : 메인 주제 이름
							. is_main : 메인 주제 여부
							. name : 하위 주제 이름
							. status : 메뉴 펼쳐져 있는지 여부
						-->
						<a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" url="/postmanage/postlist" menu_type="top_menu" role="button" aria-expanded="false" onclick="post_list(this)" params="?main=<?= $main['main_subject_name']?>" parent="<?= $main['main_subject_name']?>" is_top="Y" is_main="Y" status="C"><?= $main['main_subject_name']?><span class="ms-2">(<?= $main_post_count ?>)</span></a>
						
						<ul class="dropdown-menu">
							<?php
								for($j=0; $j < count($sub_subject_data); $j++) {
								$sub = $sub_subject_data[$j];
								$sub_post_count = 0;
								if($main['main_subject_code'] == $sub['main_subject_code']){
									for($g=0; $g < count($main_posts); $g++){
										if($sub['sub_subject_name'] == $main_posts[$g]['sub_subject_name']){
											$sub_post_count++;
										}
									}
							?>
							<li style="cursor:pointer;"><a class="dropdown-item sub_menu" url="/postmanage/postlist" menu_type="top_menu" onclick="post_list(this)" params="?main=<?= $main['main_subject_name']?>&sub=<?=$sub['sub_subject_name']?>" parent="<?= $main['main_subject_name']?>" is_top="Y" is_main="N" name="<?=$sub['sub_subject_name'] ?>" type="top_sub"><?=$sub['sub_subject_name'] ?><span class="ms-2">(<?= $sub_post_count ?>)</span></a></li>
							
							<?php
								} // --end of if
							} // --end of for
							?>
						</ul>
					</li>
				<?php
					} // --end of for
				?>
					<li class="nav-item dropdown mb-3">
						<a role="button" class="navbar-brand" id="about_cheoleeblog_top" data-bs-toggle="popover" title="about cheoleeblog" 
						data-bs-content="<?= $config->about_cheoleeblog ?>" >
						<span class="text-secondary" style="font-size:1rem;">(?) cheoleeblog</span></a>
					</li>
				</ul>
				<form class="d-flex" action="https://www.google.co.kr/search" method="get" target="_blank">
					<div class="d-inline-flex align-items-center">
						<img class="me-2" src="/img/google.png" width="30" height="30" alt="구글 로고" title="구글 로고">
					</div>
					<input class="form-control me-2" type="search" placeholder="구글 검색" name="q" aria-label="Search">
					<button class="btn btn-outline-success" type="submit"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#search"/></svg></button>
				</form>
			</div>
		</nav>
	</header>
	<!-- main -->
	<main class="flex-shrink-0">
		<div class="container-lg">
			<div class="row">
				<!-- content -->
				<nav class="" aria-label="breadcrumb" style="margin-top:55px;">

				</nav>
				<!-- --end of breadcrumb -->
				<!-- banner -->
				<div id="title_images" class="carousel slide mx-0 p-0" data-bs-ride="false" style="margin-top: 11px; display:none;">
		
					<div class="carousel-indicators">
					<?php for($i=0; $i < count($images); $i++ ){
						$num = $i + 1;
						if($i == 0){ ?>
							<button type="button" data-bs-target="#title_images" data-bs-slide-to="<?= $i ?>" class="active" aria-current="true" aria-label="Slide <?= $num ?>"></button>
						<?php } else { ?>
						<button type="button" data-bs-target="#title_images" data-bs-slide-to="<?= $i ?>" aria-label="Slide <?= $num ?>"></button>
						<?php }
						} // --end of for
					?>
					</div>
					<div class="carousel-inner">
					<?php for($i=0; $i < count($images); $i++ ){
						$num = $i + 1;

						if($images[$i]['is_activate'] == 'Y'){ ?>
							<div class="carousel-item active">
								<a class="m-0 p-0" href="<?= $images[$i]['title_href'] ?>"><img class="img-fluid title_img" src="<?= '/img/title/'.$images[$i]['title_img'];?>" class="d-block w-100" style="cursor:pointer; border:none;" alt="타이틀 이미지" file_name="<?= $images[$i]['title_img'];?>"></a>
							</div>
						<?php } else { ?>
							<div class="carousel-item ">
								<a class="m-0 p-0" href="<?= $images[$i]['title_href'] ?>"><img class="img-fluid title_img" src="<?= '/img/title/'.$images[$i]['title_img'];?>" class="d-block w-100" style="cursor:pointer; border:none;" alt="타이틀 이미지" file_name="<?= $images[$i]['title_img'];?>"></a>
							</div>
						<?php }
						} // --end of for
					?>	
					</div>
					<button class="carousel-control-prev" type="button" data-bs-target="#title_images" data-bs-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="visually-hidden">Previous</span>
					</button>
					<button class="carousel-control-next" type="button" data-bs-target="#title_images" data-bs-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="visually-hidden">Next</span>
					</button>
				</div>
				<!-- crousel 관련 스크립트 -->
				<script>
					var myCarousel = document.querySelector('#title_images');
					var carousel = new bootstrap.Carousel(myCarousel, {
						pause : true,  // 이미지 자동 슬라이드 하지 않음
						interval : false,
						touch : false, // 모바일 기기에서 swipe 되지 않도록 함
						// wrap : false
					});

					// carousel 요소를 swipe 방향에 맞게 움직이도록 함
					$('.carousel').on('touchstart', function(event){
						const xClick = event.originalEvent.touches[0].pageX;
						$(this).one('touchmove', function(event){
							const xMove = event.originalEvent.touches[0].pageX;
							const sensitivityInPx = 5;

							if( Math.floor(xClick - xMove) > sensitivityInPx ){
								$(this).carousel('next');
							}
							else if( Math.floor(xClick - xMove) < -sensitivityInPx ){
								$(this).carousel('prev');
							}
						});
						$(this).on('touchend', function(){
							$(this).off('touchmove');
						});
					});

				</script>	
				<!-- sidebar menu -->
				<div id="sidebarMenu" class="col-sm-12 col-lg-2 d-lg-block sidebar-sticky collapse mt-2" >
					<div class="p-3" style="width: 280px;">
						<!-- <a href="/" class="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom">
							<svg class="bi me-2" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
							<span class="fs-5 fw-semibold">Collapsible</span>
						</a> -->
						<ul class="list-unstyled ps-0">
						<?php
							for($i=0; $i < count($main_subject_data); $i++) {
								$main = $main_subject_data[$i];
								$main_posts = array();
								$main_post_count = 0;
								for($f=0; $f < count($posts); $f++){
									if($main['main_subject_name'] == $posts[$f]['main_subject_name']){
										$main_post_count++;
										$main_posts[] = $posts[$f]; // 메인 주제에 해당하는 포스트를 새로운 배열에 담는다.
									}
								}
						?>
							<li class="mb-1">
								<!--  메인주제 메뉴 버튼을 클릭하면 버튼 클릭 상태를 업데이트 하기위해 속성을 추가한다. parent, is_main -->
								<button class="btn btn-toggle align-items-center rounded collapsed" data-bs-toggle="collapse" data-bs-target="#<?= $main['main_subject_code'] ?>" aria-expanded="false" id="btn_<?= $main['main_subject_name'] ?>" click="" >
									<span url="/postmanage/postlist" onclick="post_list(this)" params="?main=<?= $main['main_subject_name']?>" parent="<?= $main['main_subject_name'] ?>" is_top="N" is_main="Y"><?= $main['main_subject_name'] ?><span class="ms-2">(<?= $main_post_count ?>)</span></span>
								</button>
								
								<div class="collapse" id="<?= $main['main_subject_code'] ?>">
									<ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
									<?php
									for($j=0; $j < count($sub_subject_data); $j++) {
										$sub = $sub_subject_data[$j];
										$sub_post_count = 0;
										if($main['main_subject_code'] == $sub['main_subject_code']){
											for($g=0; $g < count($main_posts); $g++){
												if($sub['sub_subject_name'] == $main_posts[$g]['sub_subject_name']){
													$sub_post_count++;
												}
											}
									?>
										<!-- 사이드바 하위 주제 클릭시에는 메인주제 클릭 상태 변경을 하지 않는다. -->
										<li class="sub_menu"><a url="/postmanage/postlist" class="link-dark rounded" id="<?= $sub['sub_subject_name'] ?>" style="cursor:pointer;" onclick="post_list(this)" params="?main=<?= $main['main_subject_name'] ?>&sub=<?= $sub['sub_subject_name'] ?>" name="<?=$sub['sub_subject_name'] ?>" parent="" is_top="N" is_main="N"><?= $sub['sub_subject_name'] ?><span class="ms-2">(<?= $sub_post_count ?>)</span></a></li>
									<?php
										} // --end of if
									} // --end of for
									?>	
									</ul>
								</div>
							</li>
						<?php
							} // --end of for
						?>
							<li class="mb-1">
								<a role="button" class="navbar-brand" style="text-decoration:none;" id="about_cheoleeblog_side" data-bs-toggle="popover" title="about cheoleeblog" 
								data-bs-content="<?= $config->about_cheoleeblog ?>" >
								<span class="text-secondary" style="font-size:1rem;">(?) cheoleeblog</span></a>
							</li>
						</ul>
					</div>	
				</div>
				<!-- ---end of sidebar menu -->
				<!-- main content -->
				<div class="col-sm-12 col-lg-8 bg-light px-0" id="main_content">
					<?= $this->session->flashdata('message') ?>
					<div class="col-sm-12 col-md-12 col-lg-12 px-2">
						<div class="pt-2 mt-2" id="head_title_div">
							<svg class="bi me-1" width="16" height="16"><use xlink:href="#post_manage"/></svg>
							<span class="fs-5 fw-bold me-3" id="head_title">포스트 보기</span>
						</div>
						<hr>
						<!-- 검색 기능 요소 -->
						<form action="/postmanage/index" enctype="multipart/form-data" method="post" accept-charset="utf-8" style="display:none;" id="search_form">
							<div class="d-flex justify-content-end mb-3" style="display:none;" id="search_div">
								<div class="col-6 col-sm-6 col-md-4 col-lg-4 me-1" id="search_filter">
									<select class="form-select form-select-sm" aria-label=".form-select-sm example" id="filter" name="filter">
									<option value="title" <?= $filter == 'title' ? 'selected' : '' ?>>제목</option>
									<option value="foreword" <?= $filter == 'foreword' ? 'selected' : '' ?>>머리말</option>
									<option value="writer_id" <?= $filter == 'writer_id' ? 'selected' : '' ?>>작성자</option>
									<option value="content" <?= $filter == 'content' ? 'selected' : '' ?>>내용</option>
									</select>
								</div>
								<div class="col-6 col-sm-6 col-md-4 col-lg-4 d-flex" id="search_word">
									<input class="form-control form-control-sm me-1" type="search" placeholder="검색어 입력" aria-label="Search" id="search" name="search" value="<?= $search ?>">
									<button class="btn btn-sm btn-outline-primary" type="submit"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#search"/></svg></button>
								</div>
							</div>
						</form>
						<!-- 검색 기능 끝 -->
						<!-- --start of swiper-container -->
						<div class="post_list_wrap" id="post_list">
						<script>
							// 팝오버 요소 초기화
							popovers_init();
						</script>