<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

        <!-- footer -->
        <footer class="footer mt-auto py-3 bg-light" id="footer">
                <div class="container d-flex justify-content-center align-items-center">
                    <span class="text-muted"><?= $config->copyright ?></span>
                </div>
        </footer>
        <!-- --end of footer -->
    </body>
</html>