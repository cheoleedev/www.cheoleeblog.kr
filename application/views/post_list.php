
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<!-- --start of swiper-container -->
			<div class="swiper" id="container_<?= $cur_page ?>">
				<!-- --start of swiper-wrapper -->
				<div class="swiper-wrapper mb-5" id="wrapper_<?= $cur_page ?>">	
					<!-- content -->
					<?php
						if(count($posts) === 0) { // 검색결과가 없을 경우 상단 정보만 표시한다.
					?>
					<!-- --start of swiper-slide -->
					<div class="swiper-slide" id="slide_<?= $cur_page ?>">
						<nav class="border-bottom mb-1" aria-label="breadcrumb" style="">
							<div class="d-flex justify-content-end align-items-center">
								<!-- breadcrumb(사이트 이동 경로) -->
								<div id="clock" class="col-4">
									<span style="font-size:.75rem;">(총</span>
									<span class="fw-bold mx-1" style="font-size:.75rem; color:blue;">0</span>
									<span class="me-3" style="font-size:.75rem;">개)</span>
								</div>
								<div class="d-flex align-items-end justify-content-end col-8">
									<!-- <span class="border rounded bg-secondary bg-gradient text-white" style="margin-bottom: 1rem; margin-right: 0.5rem; padding: 0 10px; font-size:0.85rem">위치</span> -->
									<ol class="breadcrumb mb-0">
										<li class="breadcrumb-item d-inline-block align-middle"><span class="text-primary " url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></span></li>
										<!-- <li class="breadcrumb-item d-inline-block align-middle"><a class="text-primary " href="/postmanage/postlist" url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></a></li> -->
										<?php
											if($breadcrumb_main != null) { // 메인 주제 변수값이 있을 경우
										?>
										<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_main" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="?main=<?= $breadcrumb_main ?>" parent="<?= $breadcrumb_main ?>" is_main="Y"><?= $breadcrumb_main ?></span></li>
										<?php
											}
											if($breadcrumb_sub != null) { // 하위 주제 변수값이 있을 경우
										?>
										<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_sub" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="?main=<?= $breadcrumb_main?>&sub=<?= $breadcrumb_sub?>" parent="<?= $breadcrumb_main ?>" is_main="N" name="<?= $breadcrumb_sub ?>"><?= $breadcrumb_sub ?></span></li>
										<?php
											}
											if($breadcrumb_foreword != null) { // 머리말 변수값이 있을 경우
										?>
										<li class="breadcrumb-item active" aria-current="page"><span id="breadcrumb_foreword" style="font-size:.75rem;"><?= $breadcrumb_foreword ?></span></li>
										<?php
											}
										?>
									</ol>
								</div>						
							</div>
						</nav>
					</div>
					<!-- -- end of slide -->
					<?php
						} else {
						for($i=0; $i < $all_page; $i++) {
							$start_index = $i*$posts_per_page;
					?>
					<!-- --start of swiper-slide -->
					<div class="swiper-slide" id="slide_<?= $i+1 ?>">
						<nav class="border-bottom mb-1" aria-label="breadcrumb" style="">
							<div class="d-flex justify-content-end align-items-center">
								<!-- breadcrumb(사이트 이동 경로) -->
								<div id="post_info" class="col-5">
									<span style="font-size:.75rem;">(총</span>
									<span class="fw-bold mx-1" style="font-size:.75rem; color:blue;"><?= $all_post ?></span>
									<span class="me-1" style="font-size:.75rem;">개)</span>
									<span class="info_text fw-bold me-1">Page</span><span class="info_text fw-bold" style="color:blue;"><?= $i+1 ?></span>
									<span class="info_text me-2">of</span><span class="info_text fw-bold"><?= $all_page ?></span>
								</div>
								<div class="d-flex align-items-end justify-content-end col-7">
									<!-- <span class="border rounded bg-secondary bg-gradient text-white" style="margin-bottom: 1rem; margin-right: 0.5rem; padding: 0 10px; font-size:0.85rem">위치</span> -->
									<ol class="breadcrumb mb-0">
										<li class="breadcrumb-item d-inline-block align-middle"><span class="text-primary " url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></span></li>
										<?php
											if($breadcrumb_main != null) { // 메인 주제 변수값이 있을 경우
										?>
										<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_main" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search.'&main='.$breadcrumb_main ?>" is_main="Y"><?= $breadcrumb_main ?></span></li>
										<?php
											}
											if($breadcrumb_sub != null) { // 하위 주제 변수값이 있을 경우
										?>
										<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_sub" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search.'&main='.$breadcrumb_main.'&sub='.$breadcrumb_sub ?>" parent="<?= $breadcrumb_main ?>" is_main="N" name="<?= $breadcrumb_sub ?>"><?= $breadcrumb_sub ?></span></li>
										<?php
											}
											if($breadcrumb_foreword != null) { // 머리말 변수값이 있을 경우
										?>
										<li class="breadcrumb-item active" aria-current="page"><span id="breadcrumb_foreword" style="font-size:.75rem;"><?= $breadcrumb_foreword ?></span></li>
										<?php
											}
										?>
									</ol>
								</div>						
							</div>
						</nav>
						<!-- --end of breadcrumb -->
						<?php 
							// 현재 페이지의 포스트 목록을 생성한다.
							// $cur_num = 0;
							for($j=0; $j < $posts_per_page; $j++){ 
								$cur_num = $j + $start_index;
								if($cur_num == count($posts)){
									break;
								}
							// echo "현재 포스트 번호: {$cur_num}"." 전체 포스트 수".count($posts);
						?>
						<div class="card mb-2 shadow-sm">
							<div class="row no-gutters mt-2 mx-0 px-2">
								<div class="col-md-12">
									<span url="/postmanage/detail/<?= $posts[$cur_num]['slug']?>" onclick="post_detail(this)" class="text-dark text-decoration-none m-0" style="cursor:pointer;" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search?>" parent="<?= $posts[$cur_num]['main_subject_name'] ?>" is_main="N" name="<?= $posts[$cur_num]['sub_subject_name'] ?>">
										<div class="row d-flex align-items-center justify-content-between mx-0 p-0">
											<div class="row d-flex col-10">
												<div class="col-1 p-0 mx-0 d-flex justify-content-start">
													<img src="<?= '/img/post_profile/'.$posts[$cur_num]['thumbnail'] ?>" class="img-thumbnail inline" width="30" alt="포스트 썸네일">
												</div>
												<div class="col-10 p-0 d-flex align-items-center mx-0 ">
													<span class="text-ellipsis inline align-middle" title="<?= '['.$posts[$cur_num]['foreword'].'] '.$posts[$cur_num]['title'] ?>">
														<?= 
															// word_limiter('['.$post->foreword.'] '.$post->title, 4)
															'['.$posts[$cur_num]['foreword'].'] '.$posts[$cur_num]['title'];
														?>
													</span>
												</div>
											</div>
											<div class="col-1 px-0">
												<span class="badge rounded-pill bg-primary">
													<?php
														// 포스트의 댓글 정보를 가져와 개수를 출력한다.
														$comments = $this->post->get_all_comment($posts[$cur_num]['slug']);
														echo count($comments);
													?>
												</span>
											</div>
										</div>
										<hr class="my-1">
										<div class="row d-flex align-items-center mx-0 p-0">
											<div class="col-sm-12 col-md-6 col-lg-6 inline px-0 ">
												<small class="mr-2">
													<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">작성자</i>
													<span class="info_text"><?= ' : '.$posts[$cur_num]['writer_id'] ?></span>
												</small>
												<small class="mr-2 float-end post-info-start" >
													<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">조회수</i>
													<span class="info_text"><?= ' : '.$posts[$cur_num]['view_count'] ?></span>
												</small>					
											</div>
											<div class="col-sm-12 col-md-6 col-lg-6 inline px-0">
												<small class="mr-2 post-info-end" >
													<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">주제</i>
													<span class="info_text"><?= ' : '.$posts[$cur_num]['main_subject_name'].' > '.$posts[$cur_num]['sub_subject_name']  ?></span>
												</small>
												<small class="mr-2 float-end">
													<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">등록일</i>
													<span class="info_text "><?= ' : '.date('Y-m-d',strtotime($posts[$cur_num]['reg_date']))  ?></span>
												</small>
											</div>
										</div>				
										<hr class="mt-1">
										<p><?= word_limiter(strip_tags($posts[$cur_num]['content']), 4) ?></p>
									</span>					
								</div>
							</div>
						</div>
						<?php
								}
						?>
					</div>
					<!-- --end of swiper-slide -->
					<input type="hidden" id="is_cur_<?= $i+1 ?>" value="<?= $i+1 ?>">
						<?php
							} // -- end of for (swiper-slide)
						} // -- end of else (count($$post) == 0)
						?>
				</div>
				<!-- --end of swiper-wrapper -->
				<div class="swiper-pagination" id="slide_pagination"></div>
			</div>
			<!-- --end of swiper-container -->

<script>
	var type = 'post_list'; // 슬라이드 유형을 전역 변수에 담아준다.
	var cur_slide = <?= $cur_page ?>; // 현재 페이지 index를 전역 변수에 담아준다.
	var all_slide = <?= $all_page ?>	// 마지막 페이지 index를 전역 변수에 담아준다.
	var all_post = <?= count($posts) ?>;
	var page_item_count = <?= $page_item_count ?>; // 페이지 아이템 개수를 가져온다.
	swiper_init();
	bullet_add_attr(type)
</script>