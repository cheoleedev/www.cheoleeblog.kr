
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-body" style="height:calc(100vh - 167px); overflow:auto;" >
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
			<span class="fs-5 pt-3">데쉬보드</span>
		</div>
		<hr>
		<div class="d-flex">
			<div class="flex-shrink-0">
				<img src="/img/bootstrap-fill.svg" alt="bootstrap-logo">
			</div>
			<div class="flex-grow-1 ms-3">
				통계 및 그래프 등
			</div>
		</div>
		
	</div>
	<!-- --end of main content -->
					
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
