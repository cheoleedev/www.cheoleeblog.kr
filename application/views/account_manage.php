
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-body" style="height:calc(100vh - 167px); overflow:auto;">
		<?= $this->session->flashdata('message') ?>
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-2 " width="16" height="16"><use xlink:href="#key"/></svg>
			<span class="">계정 관리</span>
		</div>
		<hr>
		<div class="px-2" id="account_manage">							
			<div class="col-sm-12 col-lg-12">
				<?= form_open_multipart('accountmanage/accountadd') ?>
				<div class="d-flex row">
					<div class="col-sm-12 col-lg-6">
						<div class="input-group mb-2">
							<input type="text" class="form-control" placeholder="아이디" aria-label="admin_id" aria-describedby="admin_id" id="admin_id" name="admin_id">
							<?php echo form_error('admin_id', '<div class="text-danger ml-2 ">', '</div>') ?>
						</div>
						<div class="input-group mb-2">
							<input type="password" class="form-control rounded-end" placeholder="비밀번호" aria-label="passwd" aria-describedby="passwd" id="passwd" name="passwd">
							<?php echo form_error('passwd', '<div class="text-danger ml-2 ">', '</div>') ?>
						</div>
					</div>
					<div class="col-sm-12 col-lg-6">
						<div class="input-group mb-2">
							<input type="text" class="form-control" placeholder="닉네임" aria-label="nickname" aria-describedby="nickname" id="nickname" name="nickname">
							<?php echo form_error('nickname', '<div class="text-danger ml-2 ">', '</div>') ?>
							<!-- 닉네임 중복 여부 체크 결과 -->
							<input type="hidden" id="nickname_check" name="nickname_check" value="N">
							<button type="button" class="btn btn-sm btn-outline-dark" onclick="is_nickname()">	
								<span class="account-info fw-bold">중복 확인</span>
							</button>
						</div>						
						<div class="input-group mb-2">
							<input type="text" class="form-control" placeholder="이메일" aria-label="email" aria-describedby="email" id="email" name="email">
							<?php echo form_error('email', '<div class="text-danger ml-2 ">', '</div>') ?>
						</div>
					</div>
				</div>
				<div class="input-group mb-2">
					<input type="submit" class="form-control btn btn-outline-secondary rounded-start" value="계정 추가">
				</div>
				<?= form_close() ?>
				<!-- 계정 정보 -->
				<div class="row border-bottom border-3 py-1" id="admin_list">
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">#</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">id</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">profile</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">nickname</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">level</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">last_login</div>
					<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">status</div>
				</div>
				<?php for($i=0; $i < count($account_data); $i++) {
					$admin_id = $account_data[$i]['admin_id'];
					$nickname = $account_data[$i]['nickname'];
					$profile_image = $account_data[$i]['profile_image'];
					if(empty($profile_image)) {
						$profile_image = 'default.png';
					}
					$level = $account_data[$i]['level'];
					$admin_status = $account_data[$i]['admin_status'];
					$last_login = $account_data[$i]['last_login_date'];
					if(($i+1) % 2 != 0 ) {  // 홀수번째 짝수번째 데이터를 색깔로 구분해준다.
				?>
				<!-- 홀수번째 데이터 -->
				<div class="row py-1 border-bottom" style="background-color:#F2F2F2;" admin_id="<?= $admin_id ?>">
					<div class="col align-middle text-center" >
						<button type="button" class="btn btn-sm btn-outline-primary" onclick="account_update(this)" admin_id="<?= $admin_id ?>">	
							<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
								<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
								<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
							</svg></span>
						</button>
					</div>
					<div class="col align-middle text-center" ><span class="account-info fw-bold"><?= $admin_id ?></span></div>
					<div class="col align-middle text-center">
						<img src="<?= base_url('img/profile_image/').$profile_image ?>" class="img-thumbnail" style="cursor:pointer;" onclick="profile_change(this)" id="<?= $admin_id ?>" width="30" alt="프로필 이미지">
						
					</div>
					<div class="col align-middle text-center d-flex justify-content-center"><input class="form-control py-0 account-info text-center" value="<?= $nickname ?>"></div>
					<div class="col align-middle text-center d-flex justify-content-center"><input class="form-control py-0 account-info text-center" value="<?= $level ?>"></div>
					<div class="col d-flex justify-content-center align-items-center"><span class="account-info text-ellipsis" title="<?= $last_login ?>"><?= $last_login ?></span></div>
					<div class="col align-middle text-center">
						<div class="form-check form-switch d-flex justify-content-center">
							<?php if($admin_status == 'O') { ?>
								<span title="상태 변경"><input class="form-check-input" type="checkbox" id="<?= $admin_id ?>" checked  onclick="id_status_change(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
							<?php } else { ?>
								<span title="상태 변경"><input class="form-check-input" type="checkbox" id="<?= $admin_id ?>" onclick="id_status_change(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
							<?php } // --end of if ?>
						</div>						
					</div>
				</div>
				<!-- 프로필 이미지 변경 정보 input 요소 -->
				<?= form_open_multipart('accountmanage/profileupload') ?>
				<div class="row" id="<?= $admin_id ?>_profile_change" style="display:none; background-color:#E7EBED;">
					<div class="col-12 my-1 input-group d-flex align-items-center">
						<input class="btn btn-outline-secondary py-0 d-flex align-items-center account-info py-1" type="submit" value="프로필 변경">
						<input type="file" class="form-control form-control-sm account-info align-middle text-center" id="new_profile" name="new_profile" aria-describedby="new_profile" aria-label="new_profile">
						<input type="hidden" name="user_id" value="<?= $admin_id ?>">
						<?php echo form_error('new_profile', '<div class="text-danger ml-2 ">', '</div>') ?>
					</div>
				</div>
				<?= form_close() ?>
				<!-- end of 프로필 -->
						
				<?php
					} else {
				?>
				<!-- 짝수번째 데이터 -->
				<div class="row py-1 border-bottom" admin_id="<?= $admin_id ?>">
					<div class="col align-middle text-center" >
						<button type="button" class="btn btn-sm btn-outline-primary" onclick="account_update(this)" admin_id="<?= $admin_id ?>">	
							<span title="업데이트"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16" title="업데이트">
								<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
								<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
							</svg></span>
						</button>
					</div>
					<div class="col align-middle text-center" ><span class="account-info fw-bold"><?= $admin_id ?></span></div>
					<div class="col align-middle text-center">
						<img src="<?= base_url('img/profile_image/').$profile_image ?>" class="img-thumbnail" style="cursor:pointer;" onclick="profile_change(this)" id="<?= $admin_id ?>" width="30" alt="프로필 이미지">
						
					</div>
					<div class="col align-middle text-center d-flex justify-content-center"><input class="form-control py-0 account-info text-center" value="<?= $nickname ?>"></div>
					<div class="col align-middle text-center d-flex justify-content-center"><input class="form-control py-0 account-info text-center" value="<?= $level ?>"></div>
					<div class="col d-flex justify-content-center align-items-center"><span class="account-info text-ellipsis" title="<?= $last_login ?>"><?= $last_login ?></span></div>
					<div class="col align-middle text-center">
						<div class="form-check form-switch d-flex justify-content-center">
							<?php if($admin_status == 'O') { ?>
							<span title="상태 변경"><input class="form-check-input" type="checkbox" id="<?= $admin_id ?>" checked  onclick="id_status_change(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
							<?php } else { ?>
								<span title="상태 변경"><input class="form-check-input" type="checkbox" id="<?= $admin_id ?>" onclick="id_status_change(this)" style="width:3em; height:1.5em; cursor:pointer;"></span>
							<?php } // --end of if ?>
						</div>						
					</div>
				</div>
				<!-- 프로필 이미지 변경 정보 input 요소 -->
				<?= form_open_multipart('accountmanage/profileupload') ?>
				<div class="row" id="<?= $admin_id ?>_profile_change" style="display:none; background-color:#E7EBED;">
					<div class="col-12 my-1 input-group d-flex align-items-center">
						<input class="btn btn-outline-secondary py-0 d-flex align-items-center account-info py-1" type="submit" value="프로필 변경">
						<input type="file" class="form-control form-control-sm account-info align-middle text-center" id="new_profile" name="new_profile" aria-describedby="new_profile" aria-label="new_profile">
						<input type="hidden" name="user_id" value="<?= $admin_id ?>">
						<?php echo form_error('new_profile', '<div class="text-danger ml-2 ">', '</div>') ?>
					</div>
				</div>
				<?= form_close() ?>
				<!-- end of 프로필 -->
				
				<?php
					}  // --end of else
				} // --end of for
				?>
			</div>
			
		</div>

	</div>
	<!-- --end of main content -->
					
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
