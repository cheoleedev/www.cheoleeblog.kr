<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>	
			<!-- content -->
			<div class="swiper">
				<div class="swiper-wrapper mb-5">
			<?php
				// for ($i=0; $i<count($posts); $i++){
				// 	// echo $i;
				// 	$postDetail = $this->post->getDetailPost($posts[$i]['slug']);
				// 	$breadcrumb_main = $postDetail['main_subject_name']; // 포스트 위치 표시를 위한 변수
				// 	$breadcrumb_sub = $postDetail['sub_subject_name']; // 포스트 위치 표시를 위한 변수
				// 	$breadcrumb_foreword = $postDetail['foreword']; // 포스트 위치 표시를 위한 변수					
			?>
					<div class="swiper-slide" id="slide_<?= $post_order ?>">
						<input type="hidden" id="pre_post_slug" value="<?= $pre_post_slug ?>">
						<input type="hidden" id="next_post_slug" value="<?= $next_post_slug ?>">
						<input type="hidden" id="cur_page" value="<?= $cur_page ?>">
						<nav class="border-bottom mb-1" aria-label="breadcrumb" style="">
							<div class="d-flex justify-content-end align-items-center">
								<!-- breadcrumb(사이트 이동 경로) -->
								<div id="clock" class="col-4">
									<span class="mx-0" style="font-size:.75rem;">(<span class="fw-bold" style="color:blue;"><?= $post_order ?></span></span>								
									<span style="font-size:.75rem;">of</span>
									<span class="fw-bold" style="font-size:.75rem;"><?= count($posts) ?>)</span>
								</div>
								<div class="d-flex align-items-end justify-content-end col-8">
									<!-- <span class="border rounded bg-secondary bg-gradient text-white" style="margin-bottom: 1rem; margin-right: 0.5rem; padding: 0 10px; font-size:0.85rem">위치</span> -->
									<ol class="breadcrumb mb-0">
										<li class="breadcrumb-item d-inline-block align-middle"><span class="text-primary " url="/postmanage/postlist" id="home" title="메인" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params=""><svg class="bi me-1 fs-5" width="20" height="20"><use xlink:href="#home"/></svg></span></li>
										<?php
											if($breadcrumb_main != null) { // 메인 주제 변수값이 있을 경우
										?>
										<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_main" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search.'&main='.$breadcrumb_main ?>" parent="<?= $breadcrumb_main ?>" is_main="Y"><?= $breadcrumb_main ?></span></li>
										<?php
											}
											if($breadcrumb_sub != null) { // 하위 주제 변수값이 있을 경우
										?>
										<li class="breadcrumb-item"><span class="text-primary" url="/postmanage/postlist" id="breadcrumb_sub" style="cursor:pointer; font-size:.75rem;" onclick="post_list(this)" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search.'&main='.$breadcrumb_main.'&sub='.$breadcrumb_sub ?>" parent="<?= $breadcrumb_main ?>" is_main="N" name="<?= $breadcrumb_sub ?>"><?= $breadcrumb_sub ?></span></li>
										<?php
											}
											if($breadcrumb_foreword != null) { // 머리말 변수값이 있을 경우
										?>
										<li class="breadcrumb-item active" aria-current="page"><span id="breadcrumb_foreword" style="font-size:.75rem;"><?= $breadcrumb_foreword ?></span></li>
										<?php
											}
										?>
									</ol>
								</div>						
							</div>
						</nav>
						<div class="row d-flex align-items-center justify-content-between">
							<!-- <div class="row d-flex col-sm-12 col-md-6 col-lg-6 "> -->
							<div class="row d-flex col-12">
								<div class="col-12 p-0 d-flex align-items-center">
									<img src="<?= '/img/post_profile/'.$thumbnail ?>" class="img-thumbnail inline ms-3 me-1" width="30" alt="포스트 썸네일">
									<span class="text-ellipsis inline align-middle" title="<?= '['.$foreword.'] '.$title ?>">
										<?= 
											// word_limiter('['.$post->foreword.'] '.$post->title, 4)
											'['.$foreword.'] '.$title;
										?>
									</span>
								</div>
							</div>
							<hr class="my-1">
							<!-- <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-between"> -->
							<div class="col-12 d-flex justify-content-between">
								<small class="mr-2">
									<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">포스트 번호</i>
									&nbsp;:&nbsp;<span class="info_text" index="<?= $post_order+1 ?>" id="post_slug_<?= $slug ?>"><?= $slug ?></span>
								</small>
								<?php	
									// 클릭한 포스트 번호와 같은 경우 해당 배열 인덱스를 히든 요소에 담아준다.
									if($slug == $slug){
								?>
								<input type="hidden" id="is_cur_<?= $slug ?>" value="<?= $post_order+1 ?>">
								<?php
									}
								?>
								<small class="mr-2">
									<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">작성자</i>
									<span class="info_text"><?= ' : '.$writer_id ?></span>
								</small>
							</div>
						</div>
						<div class="d-flex align-items-center justify-content-between">
							<small class="mr-2">
								<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">주제</i>
								&nbsp;:&nbsp;<span class="info_text" id="main_subject_name_<?= $slug ?>"><?= $main_subject_name ?></span>&nbsp;>&nbsp;<span class="info_text" id="sub_subject_name_<?= $slug ?>"><?= $sub_subject_name ?></span>
							</small>
							<small class="mr-2">
								<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">조회수</i>
								<span id="view_count_<?= $slug ?>"class="info_text"><?= ' : '.$view_count ?></span>
							</small>
							<small class="mr-2">
								<i class="fas fa-user mr-1 text-info" style="font-size:.75rem;">등록일</i>
								<span class="info_text"><?= ' : '.date('Y-m-d', strtotime($reg_date)) ?></span>
							</small>
						</div>	
						<hr class="shadow mt-1">
						<div class="viewer-items m-0 p-0">
						<?= $content ?>
						</div>
						<div class="d-flex justify-content-center mt-2">
							<button type="button" id="post_list_btn_<?= $slug ?>" class="btn btn-primary btn-sm" url="/postmanage/postlist" onclick="post_list(this)" params="<?='?page='.$cur_page.'&filter='.$filter.'&search='.$search.'&main='.$breadcrumb_main.'&sub='.$breadcrumb_sub ?>" class="text-dark text-decoration-none p-0 m-0" parent="<?= $breadcrumb_main ?>" is_main="N" name="<?= $breadcrumb_sub ?>">
								<span title="포스트 목록">포스트 목록</span>
							</button>
						</div>
						<hr>
						<!-- 코멘트 입력 및 저장 요소 -->
						<div class="mt-3 pb-1">
							<div class="row mb-2">
								<div class="col-3 pe-0">
									<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_nickname_<?= $slug ?>" name="comment_nickname_<?= $slug ?>" value="">
								</div>
								<div class="col-3 pe-0">
									<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_passwd_<?= $slug ?>" name="comment_passwd_<?= $slug ?>" value="">
								</div>
								<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">
									<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_<?= $slug ?>" name="is_secret_<?= $slug ?>">
									<label class="form-check-label" for="is_secret_<?= $slug ?>">
										<span style="font-size:.75rem;">비밀글</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-9 form-floating d-flex justify-content-center pe-0">
									<textarea class="form-control" placeholder="댓글 남기기" id="post_comment_<?= $slug ?>" name="post_comment_<?= $slug ?>" style="height: 100px; width:98%;"></textarea>
									<label class="ps-4" for="post_comment_<?= $slug ?>">댓글 남기기</label>
								</div>
								<div class="col-3 d-grid ps-0">
									<button type="button" class="btn btn-outline-primary" slug="<?= $slug ?>" onclick="save_comment(this)" style="width:95%;" id="comment_save_btn_<?= $slug ?>">
										<span class="info_text" title="저장">
											<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
												<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
											</svg>
										</span>
									</button>
								</div>
							</div>
						</div>
					
						<!-- 코멘트 리스트 -->
						<div id="comment_list_<?= $slug ?>" status="close">
							<?php
								// $comments = $this->post->get_all_comment($posts[$i]['slug']);
								// $comments_reply = $this->post->get_all_comment_reply_by_slug($posts[$i]['slug']);
							?>
							<!-- 코멘트 개수 -->
							<div id="comment_count">
								<span class="fw-bolder me-2"> 댓글 <span style="color:blue;" id="comment_count_<?= $slug ?>"><?= count($comments) ?></span> 개 </span>
								<span class="me-2" status="open" id="comment_list_open_<?= $slug ?>" style="font-size:.75rem; cursor:pointer; " slug="<?= $slug ?>" onclick="comment_list_toggle(this)">댓글 보기</span>
								<span class="" status="close" id="comment_list_close_<?= $slug ?>" style="font-size:.75rem; cursor:pointer; display:none;" slug="<?= $slug ?>" onclick="comment_list_toggle(this)">댓글 숨기기</span>
							</div>
							<div id="comment_list_item_<?= $slug ?>" style="display:none;">
								<?php for($j=0; $j < count($comments); $j++){
									if( $j < $comment_limit ){
								?>
								<!-- 코멘트 내용 -->
								<div class="border-top" id="comment_<?= $comments[$j]['idx'] ?>">
									<div class="row no-gutters mt-2 mx-0 px-2">
										<div class="col-md-12">
											<div class="d-flex align-items-center mx-0 p-0">
												<p class="text-center">
												<?php if($comments[$j]['comment_nickname'] == "운영자"){?>
													<img src="/img/logo.png" width="16" height="16">
													<span class="ms-1 text-primary" style="font-size:.75rem; font-weight:bolder" title="<?= $comments[$j]['comment_nickname'] ?>">
														<?= $comments[$j]['comment_nickname'] ?>
													</span>
												<?php } else { ?>
													<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
														<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
														<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
													</svg>
													<span style="font-size:.75rem; font-weight:bolder" title="<?= $comments[$j]['comment_nickname'] ?>">
														<?= $comments[$j]['comment_nickname'] ?>
													</span>
												<?php } // -- end of if(운영자) ?>
													&nbsp;&nbsp;
													<small class="mr-2 text-dark" style="font-size:.5rem;">
														<?= $comments[$j]['reg_date']  ?>
													</small>
												</p>
											</div>
											<div class="row">
												<?php
													$text = nl2br($comments[$j]['post_comment']); // 댓글 데이터의 줄바꿈 문자를 <br /> 테그 문자로 변경한다.
													$string_count = count(explode(' ',$comments[$j]['post_comment']));
													// echo "{$string_count}";
													// echo htmlspecialchars($text);
													$new_text = explode('<',$text); // 태그 시작문자 '<'로 댓글 문자열을 쪼갠다.
													$br_count = 0; //'br' 태그 개수
													foreach($new_text as $value ){
														$each_text = explode(' ',$value);
														if($each_text[0] == 'br') { //br 태그의 개수를 카운팅 한다.
															$br_count += 1;
														}
													}
													// echo var_dump($new_text[0],$br_count);
													if($comments[$j]['is_secret'] == 'Y') {
												?>
												<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_<?= $comments[$j]['idx'] ?>">
													<div>
														<span class="text-secondary"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#lock"/></svg></span>
														<span class="text-secondary">비밀글입니다.</span>
													</div>
												<?php
													} else {
												?>
												<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_<?= $comments[$j]['idx'] ?>">
													<?= word_limiter(nl2br($comments[$j]['post_comment']),5) ?><br>
													<?php
														if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
													?>
													<div>
														<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="unfold_comment(this)">자세히 보기</span>
													</div>
													<?php
														}
													?>
												</div>
												<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_<?= $comments[$j]['idx'] ?>">
													<?= nl2br($comments[$j]['post_comment']) ?><br>
													
													<?php
														if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
													?>	
													<div>						
														<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="fold_comment(this)">간략히 보기</span>
													</div>
													<?php
														}
													}
													?>
												</div>
												<!-- 댓글 수정을 위한 textarea hidden 요소 -->
												<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_<?= $comments[$j]['idx'] ?>">
													<div class="col-12 form-check d-flex align-items-center">
													<?php if($comments[$j]['is_secret'] == 'Y'){?>
														<input class="form-check-input me-2" type="checkbox" checked value="" id="is_secret_<?= $comments[$j]['idx'] ?>">
													<?php } else { ?>
														<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_<?= $comments[$j]['idx'] ?>">
													<?php }?>
														<label class="form-check-label me-3" for="is_secret_<?= $comments[$j]['idx'] ?>">
															<span style="font-size:.75rem;">비밀글</span>
														</label>
														<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="fold_comment(this)">취소</span>
													</div>
													<div class="form-floating">
														<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_<?= $comments[$j]['idx'] ?>" name="comment_modify_<?= $comments[$j]['idx'] ?>" style="height: 100px; font-size:.75rem;" value=""><?= $comments[$j]['post_comment'] ?></textarea>
														<label for="comment_modify_<?= $comments[$j]['idx'] ?>">댓글 수정하기</label>
													</div>
													<br>									
												</div>
												
												<!-- 댓글 수정,삭제 관련 요소 -->
												<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">
													<div class="row d-flex justify-content-center px-0">
														<div class="d-flex justify-content-center" style="height:80%;" >
															<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="<?= $comments[$j]['idx'] ?>" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_<?= $comments[$j]['idx'] ?>">
																<span class="info_text" title="저장">
																	<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																		<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																	</svg>
																</span>
															</button>
														</div>
														<div class="comment_pw_check mb-1" id="passwd_check_<?= $comments[$j]['idx'] ?>">
															<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_<?= $comments[$j]['idx'] ?>" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" placeholder="비밀번호" maxlength="4" title="" value="">
														</div>
													</div>
													<div class="d-flex justify-content-center align-items-end">
														<div class="comment_icon_div me-2">
															<span class="me-2 comment-icon" id="reply_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="<?= $comments[$j]['idx'] ?>" title="댓글 남기기">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																	<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																</svg>
															</span>
														</div>
													<?php if($comments[$j]['comment_nickname'] != '운영자'){ ?>
														<div class="comment_icon_div me-2">
															<span class="me-2 comment-icon" id="modify_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="<?= $comments[$j]['idx'] ?>" title="수정">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
																	<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
																	<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
																</svg>
															</span>
														</div>
														<div class="comment_icon_div del_btn" id="delete_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" mark="comment" title="삭제">										
															<p class="trash_cap_wrap text-center">
																<span class="trash_cap py-0 my-0"></span>	
															</p>
															<span class="trash comment_icon">
															</span>
														</div>
													<?php } // -- end of if(운영자) ?>
													</div>
												</div>
												<!-- 댓글에 대한 댓글 등록 요소  -->
												<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_<?= $comments[$j]['idx'] ?>">
													<div class="row mb-2">
														<div class="col-3 pe-0">
															<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_<?= $comments[$j]['idx'] ?>" name="comment_reply_nickname_<?= $comments[$j]['idx'] ?>" value="">
														</div>
														<div class="col-3 pe-0">
															<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_<?= $comments[$j]['idx'] ?>" name="comment_reply_passwd_<?= $comments[$j]['idx'] ?>" value="">
														</div>
														<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">
															<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>" name="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>">
															<label class="form-check-label" for="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>">
																<span style="font-size:.75rem;">비밀글</span>
															</label>
														</div>
													</div>
													<div class="row">
														<div class="col-9 form-floating d-flex justify-content-center pe-0">
															<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_<?= $comments[$j]['idx'] ?>" name="post_comment_reply_<?= $comments[$j]['idx'] ?>" style="height: 100px; width:98%;" ></textarea>
															<label class="ps-4" for="post_comment_reply_<?= $comments[$j]['idx'] ?>">댓글에 댓글 남기기</label>
														</div>
														<div class="col-3 d-grid ps-0">
															<button type="button" class="btn btn-outline-primary" slug="<?= $slug ?>" org_idx="<?= $comments[$j]['idx'] ?>" onclick="save_comment_reply(this)" style="width:95%;">
																<span class="info_text" title="저장">
																	<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																		<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																	</svg>
																</span>
															</button>
														</div>
													</div>
												</div>
											</div>												
										</div>
									</div>
									<!-- end of comment -->
									<!-- 댓글에 대한 댓글 내용 -->
									<div id="comment_reply_list_<?= $comments[$j]['idx'] ?>">
										<?php
											$comment_reply_count = 0; // 대댓글 개수를 세기 위한 변수
											for($k = 0; $k < count($comments_reply); $k++) { // 댓글에 대한 댓글 갯수만큼 반복한다.
												$comment_idx = $comments[$j]['idx']; // 원본 댓글의 idx
												$comment_reply_org_idx = $comments_reply[$k]['org_idx']; // 원본 댓글에 대한 댓글 정보의 idx
												if( $comment_idx == $comment_reply_org_idx) { // 원본 댓글의 idx에 해당하는지 확인한다.
													$comment_reply_count++ ; // 댓글에 대한 댓글 개수를 센다.
										?>
										<div class="border-top comment_reply_content" id="comment_reply_<?= $comments_reply[$k]['idx'] ?>">
											<div class="row no-gutters mt-2 mx-0 px-2">
												<div class="col-md-12">
													<div class="d-flex align-items-center mx-0 p-0">
														<p class="text-center">
															<span class="text-primary me-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">
																	<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>
																</svg>
															</span>
														<?php if($comments_reply[$k]['comment_reply_nickname'] == "운영자"){?>
															<img src="/img/logo.png" width="16" height="16">
															<span class="ms-1 text-primary" style="font-size:.75rem; font-weight:bolder" title="<?= $comments_reply[$k]['comment_reply_nickname'] ?>">
																<?= $comments_reply[$k]['comment_reply_nickname'] ?>
															</span>
														<?php } else { ?>
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
																<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
																<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
															</svg>
															<span style="font-size:.75rem; font-weight:bolder" title="<?= $comments_reply[$k]['comment_reply_nickname'] ?>">
																<?= $comments_reply[$k]['comment_reply_nickname'] ?>
															</span>
														<?php } // -- end of if(운영자) ?>
															&nbsp;&nbsp;
															<small class="mr-2 text-dark" style="font-size:.5rem;">
																<?= $comments_reply[$k]['reg_date']  ?>
															</small>
														</p>
													</div>
													<div class="row">
														<?php
															$text = nl2br($comments_reply[$k]['post_comment_reply']); // 댓글 데이터의 줄바꿈 문자를 <br /> 테그 문자로 변경한다.
															$string_count = count(explode(' ',$comments_reply[$k]['post_comment_reply']));
															// echo "{$string_count}";
															// echo htmlspecialchars($text);
															$new_text = explode('<',$text); // 태그 시작문자 '<'로 댓글 문자열을 쪼갠다.
															$br_count = 0; //'br' 태그 개수
															foreach($new_text as $value ){
																$each_text = explode(' ',$value);
																if($each_text[0] == 'br') { //br 태그의 개수를 카운팅 한다.
																	$br_count += 1;
																}
															}
															// echo var_dump($new_text[0],$br_count);
															if($comments_reply[$k]['is_secret'] == 'Y') {
														?>
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<div>
																<span class="text-secondary"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#lock"/></svg></span>
																<span class="text-secondary">비밀글입니다.</span>
															</div>
														<?php
															} else {
														?>
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<?= word_limiter(nl2br($comments_reply[$k]['post_comment_reply']),5) ?><br>
															<?php
																if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
															?>
															<div>
																<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="unfold_comment_reply(this)">자세히 보기</span>
															</div>
															<?php
																}
															?>
														</div>
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<?= nl2br($comments_reply[$k]['post_comment_reply']) ?><br>
															
															<?php
																if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
															?>	
															<div>						
																<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="fold_comment_reply(this)">간략히 보기</span>
																</div>
															<?php
																}
															}
															?>
														</div>
														<!-- 댓글에 대한 댓글 수정을 위한 textarea hidden 요소 -->
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<div class="col-12 form-check d-flex align-items-center">
															<?php if($comments_reply[$k]['is_secret'] == 'Y'){?>
																<input class="form-check-input me-2" type="checkbox" checked value="" id="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
															<?php } else { ?>
																<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
															<?php }?>
																<label class="form-check-label me-3" for="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
																	<span style="font-size:.75rem;">비밀글</span>
																</label>
																<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="fold_comment_reply(this)">취소</span>
																
															</div>
															<div class="form-floating">
																<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>" name="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>" style="height: 100px; font-size:.75rem;" value=""><?= $comments_reply[$k]['post_comment_reply'] ?></textarea>
																<label for="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>">댓글 수정하기</label>
															</div>
															
															<br>									
														</div>
														
														<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->
														<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">
															<div class="row d-flex justify-content-center px-0">	
																<div class="d-flex justify-content-center" style="height:80%;" >
																	<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_<?= $comments_reply[$k]['idx'] ?>">
																		<span class="info_text" title="저장">
																			<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																				<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																			</svg>
																		</span>
																	</button>
																</div>
																<div class="comment_pw_check mb-1" id="reply_passwd_check_<?= $comments_reply[$k]['idx'] ?>">
																	<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_<?= $comments_reply[$k]['idx'] ?>" idx="<?= $comments_reply[$k]['idx'] ?>" placeholder="비밀번호" maxlength="4" title="" value="">
																</div>
															</div>
															<div class="d-flex justify-content-center align-items-end">
															<?php if($comments_reply[$k]['comment_reply_nickname'] != '운영자') { ?>
																<div class="comment_icon_div me-2">
																	<span class="me-2 comment-icon" id="reply_modify_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="<?= $comments_reply[$k]['idx'] ?>" title="수정">
																		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
																			<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
																			<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
																		</svg>
																	</span>
																</div>
																<div class="comment_icon_div del_btn" id="reply_delete_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" mark="comment_reply" title="삭제">										
																	<p class="trash_cap_wrap text-center">
																		<span class="trash_cap py-0 my-0"></span>	
																	</p>
																	<span class="trash comment_icon">
																	</span>
																</div>
															<?php } ?>
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<!-- end of comment_reply -->
										<?php
												} // --end of if (comment_reply_org_idx)
											} // --end of for (comment_reply)
										?>
									</div>
									<!-- end of comment_reply_list -->
									<input type="hidden" id="comment_reply_count_<?= $comments[$j]['idx'] ?>" value="<?= $comment_reply_count ?>" >
								</div>
								<!-- end of comment -->
			<?php 
								} else { // 코멘트 더보기를 위한 숨김 요소(기본 표시 개수 이외의 요소)
			?>
								<!-- 코멘트 내용 -->
								<div class="border-top hidden_comment" id="comment_<?= $comments[$j]['idx'] ?>">
									<div class="row no-gutters mt-2 mx-0 px-2">
										<div class="col-md-12">
											<div class="d-flex align-items-center mx-0 p-0">
												<p class="text-center">
												<?php if($comments[$j]['comment_nickname'] == "운영자"){?>
													<img src="/img/logo.png" width="16" height="16">
													<span class="ms-1 text-primary" style="font-size:.75rem; font-weight:bolder" title="<?= $comments[$j]['comment_nickname'] ?>">
														<?= $comments[$j]['comment_nickname'] ?>
													</span>
												<?php } else { ?>
													<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
														<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
														<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
													</svg>
													<span style="font-size:.75rem; font-weight:bolder" title="<?= $comments[$j]['comment_nickname'] ?>">
														<?= $comments[$j]['comment_nickname'] ?>
													</span>
												<?php } // -- end of if(운영자) ?>
													&nbsp;&nbsp;
													<small class="mr-2 text-dark" style="font-size:.5rem;">
														<?= $comments[$j]['reg_date']  ?>
													</small>
												</p>
											</div>
											<div class="row">
												<?php
													$text = nl2br($comments[$j]['post_comment']); // 댓글 데이터의 줄바꿈 문자를 <br /> 테그 문자로 변경한다.
													$string_count = count(explode(' ',$comments[$j]['post_comment']));
													// echo "{$string_count}";
													// echo htmlspecialchars($text);
													$new_text = explode('<',$text); // 태그 시작문자 '<'로 댓글 문자열을 쪼갠다.
													$br_count = 0; //'br' 태그 개수
													foreach($new_text as $value ){
														$each_text = explode(' ',$value);
														if($each_text[0] == 'br') { //br 태그의 개수를 카운팅 한다.
															$br_count += 1;
														}
													}
													// echo var_dump($new_text[0],$br_count);
													if($comments[$j]['is_secret'] == 'Y') {
												?>
												<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_<?= $comments[$j]['idx'] ?>">
													<div>
														<span class="text-secondary"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#lock"/></svg></span>
														<span class="text-secondary">비밀글입니다.</span>
													</div>
												<?php
													} else {
												?>
												<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem;" id="short_comment_<?= $comments[$j]['idx'] ?>">
													<?= word_limiter(nl2br($comments[$j]['post_comment']),5) ?><br>
													<?php
														if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
													?>
													<div>
														<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="unfold_comment(this)">자세히 보기</span>
													</div>
													<?php
														}
													?>
												</div>
												<div class="col-9 col-lg-10 col-md-10 col-sm-9" style="font-size:.75rem; display:none;" id="long_comment_<?= $comments[$j]['idx'] ?>">
													<?= nl2br($comments[$j]['post_comment']) ?><br>
													
													<?php
														if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
													?>	
													<div>						
														<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="fold_comment(this)">간략히 보기</span>
													</div>
													<?php
														}
													}
													?>
												</div>
												<!-- 댓글 수정을 위한 textarea hidden 요소 -->
												<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_<?= $comments[$j]['idx'] ?>">
													<div class="col-12 form-check d-flex align-items-center">
														<input class="form-check-input me-2" type="checkbox" value="" id="is_secret_<?= $comments[$j]['idx'] ?>">
														<label class="form-check-label me-3" for="is_secret_<?= $comments[$j]['idx'] ?>">
															<span style="font-size:.75rem;">비밀글</span>
														</label>
														<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" onclick="fold_comment(this)">취소</span>
													</div>
													<div class="form-floating">
														<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_modify_<?= $comments[$j]['idx'] ?>" name="comment_modify_<?= $comments[$j]['idx'] ?>" style="height: 100px; font-size:.75rem;" value=""><?= $comments[$j]['post_comment'] ?></textarea>
														<label for="comment_modify_<?= $comments[$j]['idx'] ?>">댓글 수정하기</label>
													</div>
													<br>									
												</div>
												
												<!-- 댓글 수정,삭제 관련 요소 -->
												<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">
													<div class="row d-flex justify-content-center px-0">
														<div class="d-flex justify-content-center" style="height:80%;" >
															<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="<?= $comments[$j]['idx'] ?>" onclick="update_comment(this)" style="width:100%;" id="comment_modify_save_btn_<?= $comments[$j]['idx'] ?>">
																<span class="info_text" title="저장">
																	<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																		<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																	</svg>
																</span>
															</button>
														</div>
														<div class="comment_pw_check mb-1" id="passwd_check_<?= $comments[$j]['idx'] ?>">
															<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value(this,undo_input,unfold_hidden_comment,delete_comment)" id="modify_passwd_<?= $comments[$j]['idx'] ?>" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" placeholder="비밀번호" maxlength="4" title="" value="">
														</div>
													</div>
													<div class="d-flex justify-content-center align-items-end">
														<div class="comment_icon_div me-2">
															<span class="me-2 comment-icon" id="reply_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="comment_reply(this)" idx="<?= $comments[$j]['idx'] ?>" title="댓글 남기기">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																	<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																</svg>
															</span>
														</div>
													<?php if($comments[$j]['comment_nickname'] != '운영자'){ ?>
														<div class="comment_icon_div me-2">
															<span class="me-2 comment-icon" id="modify_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment(this)" idx="<?= $comments[$j]['idx'] ?>" title="수정">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
																	<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
																	<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
																</svg>
															</span>
														</div>
														<div class="comment_icon_div del_btn" id="delete_button_<?= $comments[$j]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments[$j]['idx'] ?>" post_slug ="<?= $slug ?>" mark="comment" title="삭제">										
															<p class="trash_cap_wrap text-center">
																<span class="trash_cap py-0 my-0"></span>	
															</p>
															<span class="trash comment_icon">
															</span>
														</div>
													<?php } // -- end of if(운영자) ?>
													</div>
												</div>
												<!-- 댓글에 대한 댓글 등록 요소  -->
												<div class="comment_reply mt-2 p-1 border rounded" id="comment_reply_reg_<?= $comments[$j]['idx'] ?>">
													<div class="row mb-2">
														<div class="col-3 pe-0">
															<input type="text" class="form-control ms-2" placeholder="별명" aria-label="별명" id="comment_reply_nickname_<?= $comments[$j]['idx'] ?>" name="comment_reply_nickname_<?= $comments[$j]['idx'] ?>" value="">
														</div>
														<div class="col-3 pe-0">
															<input type="password" class="form-control" placeholder="비밀번호" aria-label="비밀번호" onkeyup="check_passwd_value(this,undo_input)" maxlength="4" id="comment_reply_passwd_<?= $comments[$j]['idx'] ?>" name="comment_reply_passwd_<?= $comments[$j]['idx'] ?>" value="">
														</div>
														<div class="col-3 form-check d-flex justify-content-center align-items-center px-0">
															<input class="form-check-input me-2" type="checkbox" value="" id="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>" name="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>">
															<label class="form-check-label" for="comment_reply_is_secret_<?= $comments[$j]['idx'] ?>">
																<span style="font-size:.75rem;">비밀글</span>
															</label>
														</div>
													</div>
													<div class="row">
														<div class="col-9 form-floating d-flex justify-content-center pe-0">
															<textarea class="form-control" placeholder="댓글에 댓글 남기기" id="post_comment_reply_<?= $comments[$j]['idx'] ?>" name="post_comment_reply_<?= $comments[$j]['idx'] ?>" style="height: 100px; width:98%;" ></textarea>
															<label class="ps-4" for="post_comment_reply_<?= $comments[$j]['idx'] ?>">댓글에 댓글 남기기</label>
														</div>
														<div class="col-3 d-grid ps-0">
															<button type="button" class="btn btn-outline-primary" slug="<?= $slug ?>" org_idx="<?= $comments[$j]['idx'] ?>" onclick="save_comment_reply(this)" style="width:95%;">
																<span class="info_text" title="저장">
																	<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																		<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																	</svg>
																</span>
															</button>
														</div>
													</div>
												</div>
											</div>												
										</div>
									</div>
									<!-- end of comment -->
									<!-- 댓글에 대한 댓글 내용 -->
									<div id="comment_reply_list_<?= $comments[$j]['idx'] ?>">
										<?php
											$comment_reply_count = 0; // 대댓글 개수를 세기 위한 변수
											for($k = 0; $k < count($comments_reply); $k++) { // 댓글에 대한 댓글 갯수만큼 반복한다.
												$comment_idx = $comments[$j]['idx']; // 원본 댓글의 idx
												$comment_reply_org_idx = $comments_reply[$k]['org_idx']; // 원본 댓글에 대한 댓글 정보의 idx
												if( $comment_idx == $comment_reply_org_idx) { // 원본 댓글의 idx에 해당하는지 확인한다.
													$comment_reply_count++ ; // 댓글에 대한 댓글 개수를 센다.
										?>
										<div class="border-top comment_reply_content" id="comment_reply_<?= $comments_reply[$k]['idx'] ?>">
											<div class="row no-gutters mt-2 mx-0 px-2">
												<div class="col-md-12">
													<div class="d-flex align-items-center mx-0 p-0">
														<p class="text-center">
															<span class="text-primary me-1">
																<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-return-right" viewBox="0 0 16 16">
																	<path fill-rule="evenodd" d="M1.5 1.5A.5.5 0 0 0 1 2v4.8a2.5 2.5 0 0 0 2.5 2.5h9.793l-3.347 3.346a.5.5 0 0 0 .708.708l4.2-4.2a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 8.3H3.5A1.5 1.5 0 0 1 2 6.8V2a.5.5 0 0 0-.5-.5z"/>
																</svg>
															</span>
														<?php if($comments_reply[$k]['comment_reply_nickname'] == "운영자"){?>
															<img src="/img/logo.png" width="16" height="16">
															<span class="ms-1 text-primary" style="font-size:.75rem; font-weight:bolder" title="<?= $comments_reply[$k]['comment_reply_nickname'] ?>">
																<?= $comments_reply[$k]['comment_reply_nickname'] ?>
															</span>
														<?php } else { ?>
															<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
																<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
																<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
															</svg>
															<span style="font-size:.75rem; font-weight:bolder" title="<?= $comments_reply[$k]['comment_reply_nickname'] ?>">
																<?= $comments_reply[$k]['comment_reply_nickname'] ?>
															</span>
														<?php } // -- end of if(운영자) ?>
															&nbsp;&nbsp;
															<small class="mr-2 text-dark" style="font-size:.5rem;">
																<?= $comments_reply[$k]['reg_date']  ?>
															</small>
														</p>
													</div>
													<div class="row">
														<?php
															$text = nl2br($comments_reply[$k]['post_comment_reply']); // 댓글 데이터의 줄바꿈 문자를 <br /> 테그 문자로 변경한다.
															$string_count = count(explode(' ',$comments_reply[$k]['post_comment_reply']));
															// echo "{$string_count}";
															// echo htmlspecialchars($text);
															$new_text = explode('<',$text); // 태그 시작문자 '<'로 댓글 문자열을 쪼갠다.
															$br_count = 0; //'br' 태그 개수
															foreach($new_text as $value ){
																$each_text = explode(' ',$value);
																if($each_text[0] == 'br') { //br 태그의 개수를 카운팅 한다.
																	$br_count += 1;
																}
															}
															// echo var_dump($new_text[0],$br_count);
															if($comments_reply[$k]['is_secret'] == 'Y') {
														?>
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<div>
																<span class="text-secondary"><svg class="bi me-1" width="16" height="16" fill="currentColor"><use xlink:href="#lock"/></svg></span>
																<span class="text-secondary">비밀글입니다.</span>
															</div>
														<?php
															} else {
														?>
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem;" id="short_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<?= word_limiter(nl2br($comments_reply[$k]['post_comment_reply']),5) ?><br>
															<?php
																if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
															?>
															<div>
																<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="unfold_comment_reply(this)">자세히 보기</span>
															</div>
															<?php
																}
															?>
														</div>
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 ps-5" style="font-size:.75rem; display:none;" id="long_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<?= nl2br($comments_reply[$k]['post_comment_reply']) ?><br>
															
															<?php
																if($string_count > 4 or $br_count > 0 ){ // br 태그가 1개 이상일 경우 요소를 표시한다.
															?>	
															<div>						
																<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="fold_comment_reply(this)">간략히 보기</span>
																</div>
															<?php
																}
															}
															?>
														</div>
														<!-- 댓글에 대한 댓글 수정을 위한 textarea hidden 요소 -->
														<div class="col-9 col-lg-10 col-md-10 col-sm-9 pe-0 me-0" style="font-size:.75rem; display:none;" id="hidden_comment_reply_<?= $comments_reply[$k]['idx'] ?>">
															<div class="col-12 form-check d-flex align-items-center">
															<?php if($comments_reply[$k]['is_secret'] == 'Y'){?>
																<input class="form-check-input me-2" type="checkbox" checked value="" id="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
															<?php } else { ?>
																<input class="form-check-input me-2" type="checkbox" value="" id="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
															<?php }?>
																<label class="form-check-label me-3" for="reply_is_secret_<?= $comments_reply[$k]['idx'] ?>">
																	<span style="font-size:.75rem;">비밀글</span>
																</label>
																<span class="fold" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="fold_comment_reply(this)">취소</span>
																
															</div>
															<div class="form-floating">
																<textarea class="form-control comment_modify" wrap="hard" cols="12" placeholder="댓글 수정하기" id="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>" name="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>" style="height: 100px; font-size:.75rem;" value=""><?= $comments_reply[$k]['post_comment_reply'] ?></textarea>
																<label for="comment_reply_modify_<?= $comments_reply[$k]['idx'] ?>">댓글 수정하기</label>
															</div>
															
															<br>									
														</div>
														
														<!-- 댓글에 대한 댓글 수정,삭제 관련 요소 -->
														<div class="col-3 col-lg-2 col-md-2 col-sm-3 row m-0 p-0 d-flex justify-content-center align-items-end">
															<div class="row d-flex justify-content-center px-0">	
																<div class="d-flex justify-content-center" style="height:80%;" >
																	<button type="button" class="btn btn-outline-primary comment_modify_save_btn mx-0" idx="<?= $comments_reply[$k]['idx'] ?>" onclick="update_comment_reply(this)" style="width:100%;" id="comment_reply_modify_save_btn_<?= $comments_reply[$k]['idx'] ?>">
																		<span class="info_text" title="저장">
																			<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
																				<path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
																			</svg>
																		</span>
																	</button>
																</div>
																<div class="comment_pw_check mb-1" id="reply_passwd_check_<?= $comments_reply[$k]['idx'] ?>">
																	<input type="password" class="w-100 text-center" style="font-size:.5rem;" onkeyup="check_value_reply(this,undo_input,unfold_hidden_comment_reply,delete_comment_reply)" id="modify_reply_passwd_<?= $comments_reply[$k]['idx'] ?>" idx="<?= $comments_reply[$k]['idx'] ?>" placeholder="비밀번호" maxlength="4" title="" value="">
																</div>
															</div>
															<div class="d-flex justify-content-center align-items-end">
															<?php if($comments_reply[$k]['comment_reply_nickname'] != '운영자') { ?>
																<div class="comment_icon_div me-2">
																	<span class="me-2 comment-icon" id="reply_modify_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" onclick="modify_comment_reply(this)" idx="<?= $comments_reply[$k]['idx'] ?>" title="수정">
																		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
																			<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
																			<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
																		</svg>
																	</span>
																</div>
																<div class="comment_icon_div del_btn" id="reply_delete_button_<?= $comments_reply[$k]['idx'] ?>" style="font-size:.5rem; cursor:pointer;" idx="<?= $comments_reply[$k]['idx'] ?>" mark="comment_reply" title="삭제">										
																	<p class="trash_cap_wrap text-center">
																		<span class="trash_cap py-0 my-0"></span>	
																	</p>
																	<span class="trash comment_icon">
																	</span>
																</div>
															<?php } ?>
															</div>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<!-- end of comment_reply -->
										<?php
												} // --end of if (comment_reply_org_idx)
											} // --end of for (comment_reply)
										?>
									</div>
									<!-- end of comment_reply_list -->
									<input type="hidden" id="comment_reply_count_<?= $comments[$j]['idx'] ?>" value="<?= $comment_reply_count ?>" >
								</div>
								<!-- end of comment -->
								<input type="hidden" id="after_comment_save" value="">


			<?php						
								} // --end of if(comment_limit)
					} // --end of for (comment) 
			?>
							</div>
							<!-- end of comment_list_item  -->
						</div>
						<input type="hidden" id="new_comment_start_order_<?= $slug ?>" value="<?= $comment_limit ?>" >
						<!-- end of comment_list -->
					<?php if(count($comments) > $comment_limit){	?>
						<div class="d-flex justify-content-center mt-2">
							<button type="button" id="comment_more_btn_<?= $slug ?>" class="btn btn-outline-secondary w-100 btn-sm" url="/postmanage/postlist" onclick="comment_more(this)" style="display:none;" class="text-dark text-decoration-none p-0 m-0" slug="<?= $slug ?>">
								<span title="더보기">더보기</span>
							</button>
						</div>
					<?php
						}
						// $comments_json = json_encode($comments); // 댓글 데이터를 json 형태로 인코딩 한다.
						// $comments_reply_json = json_encode($comments_reply); // 대댓글 데이터를 json 형태로 인코딩 한다.
					?>
						<hr>
						<!-- 관련 포스트 목록 -->
						<div class="my-3" id="post_list">
							<p><span class="fw-bolder">관련 포스트 목록</span></p>
							<ul class="sibling_posts">
							<?php
								for($i=(count($posts)-1); $i >= 0; $i--) {
									$page = ceil(($i+1)/$posts_per_page); // 현재 포스트의 페이지 번호
									if($posts[$i]['slug'] == $slug) {
							?>
								<li class="fw-bolder"><a style="text-decoration:none;" href="<?= '/postmanage/detaillist/' . $posts[$i]['slug'] .'?page='.$page.'&filter='.$filter.'&search='.$search ?>" target="_blank"><?= '['.$posts[$i]['foreword'].'] '.$posts[$i]['title'] ?></a></li>
							<?php	} else {
							?>
								<li class=""><a style="text-decoration:none;" href="<?= '/postmanage/detaillist/' . $posts[$i]['slug'] .'?page='.$page.'&filter='.$filter.'&search='.$search ?>" target="_blank"><?= '['.$posts[$i]['foreword'].'] '.$posts[$i]['title'] ?></a></li>
							<?php   } 
								}	
							?>
							</ul>
						</div>
						<!-- end of post_list -->
					</div>
					<!-- end of swiper-slide -->
					<?php
				// } // --end of for (post)
			?>
				</div>
				<!-- --end of swiper-wrapper -->
				<!-- <div class="swiper-pagination" id="slide_pagination"></div> -->
			</div>
			<!-- --end of swiper-container -->

			<script>
				var cur_page = $("#cur_page").val();
				var post_order = <?= $post_order ?>;
				var type = 'post'; // 슬라이드 유형을 전역 변수에 담아준다.
				var pre_post_slug = $("#pre_post_slug").val(); // 이전 포스트 slug를 가져온다.
				var next_post_slug = $("#next_post_slug").val(); // 다음 포스트 slug를 가져온다.
				var cur_slide = $("#is_cur_<?= $slug ?>").val(); // 클릭한 포스트의 순서를 가져온다.
				var all_slide = <?= count($posts)?>; // 전체 포스트 수를 가져온다.
				var page_item_count = <?= $page_item_count ?>; // 페이지 아이템 개수를 가져온다.
				var params = $("#post_list_btn_<?= $slug ?>").val(); // 클릭한 포스트의 속성값을 가져온다.
				var comment_limit = <?= $comment_limit ?>; // 댓글 표시 개수를 전역 변수에 담는다.
				// viewer 객체를 생성한다.
				swiper_init();
				trash_init();
				viewer_init();			
			</script>

