
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/header.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/nav_head_sidebar.php');
?>
	
	<!-- main content -->
	<div class="col-sm-12 col-lg-8 bg-body" >
		<?= $this->session->flashdata('message') ?>
		<div class="pt-2 fs-5 fw-bold">
			<svg class="bi me-2 " width="16" height="16"><use xlink:href="#setting"/></svg>
			<span class="">환경 설정</span>
		</div>
		<hr>
		<div id="title_change">							
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold" >타이틀 이미지 변경</p>
			</div>
			<?= form_open_multipart('setting/titleupload') ?>
			<div class="input-group mb-3">
				<input type="file" class="form-control" id="new_image" name="new_image">
				<input type="submit" class="btn btn-outline-secondary" value="업로드">
				<?php echo form_error('new_image', '<div class="text-danger ml-2 ">', '</div>') ?>
			</div>
			<?= form_close() ?>
			<?= form_open_multipart('setting/titlechange') ?>
			<div class="input-group mb-3">
				<input type="text" class="form-control" placeholder="선택한 이미지" aria-label="selected title" aria-describedby="title_change" id="selected_title" name="selected_title">
				<?php echo form_error('selected_title', '<div class="text-danger ml-2 ">', '</div>') ?>
				<input type="submit" class="btn btn-outline-secondary" value="변경">
			</div>
			<?= form_close() ?>
		</div>
		
		<div id="title_images" class="carousel slide" data-bs-ride="false">
		
			<div class="carousel-indicators">
			<?php for($i=0; $i < count($images); $i++ ){
				$num = $i + 1;
				if($i == 0){ ?>
					<button type="button" data-bs-target="#title_images" data-bs-slide-to="<?= $i ?>" class="active" aria-current="true" aria-label="Slide <?= $num ?>"></button>
				<?php } else { ?>
				<button type="button" data-bs-target="#title_images" data-bs-slide-to="<?= $i ?>" aria-label="Slide <?= $num ?>"></button>
				<?php }
				} // --end of for
			?>
			</div>
			<div class="carousel-inner">
			<?php for($i=0; $i < count($images); $i++ ){
				$num = $i + 1;

				if($images[$i]['is_activate'] == 'Y'){ ?>
					<div class="carousel-item active">
						<img src="<?= base_url('img/title/').$images[$i]['title_img'];?>" class="d-block w-100" style="cursor:pointer;" alt="타이틀 이미지" onclick="title_select(this);" file_name="<?= $images[$i]['title_img'];?>">
					</div>
				<?php } else { ?>
					<div class="carousel-item">
						<img src="<?= base_url('img/title/').$images[$i]['title_img'];?>" class="d-block w-100" style="cursor:pointer;" alt="타이틀 이미지" onclick="title_select(this);" file_name="<?= $images[$i]['title_img'];?>">
					</div>
				<?php }
				} // --end of for
			?>	
			</div>
			<button class="carousel-control-prev" type="button" data-bs-target="#title_images" data-bs-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Previous</span>
			</button>
			<button class="carousel-control-next" type="button" data-bs-target="#title_images" data-bs-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Next</span>
			</button>
		</div>
		<hr style="background-color:grey;">
		<!-- crousel 관련 스크립트 -->
		<script>
			var myCarousel = document.querySelector('#title_images');
			var carousel = new bootstrap.Carousel(myCarousel, {
				pause : true,  // 이미지 자동 슬라이드 하지 않음
				interval : false,
				wrap : false
				
			});
		</script>
		<!-- end of crousel -->
		<div id="subject_manage">
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold" >주제 관리</p>
			</div>
			
			<div class="d-flex row">
				<!-- 메인 주제 요소 -->
				<div class="col-sm-12 col-lg-6">
					<?= form_open_multipart('setting/mainsubjectadd') ?>
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="메인 주제 입력" aria-label="main subject" aria-describedby="main_subject" id="main_subject" name="main_subject">
						<?php echo form_error('main_subject', '<div class="text-danger ml-2 ">', '</div>') ?>
						<input type="submit" class="btn btn-outline-secondary" value="추가">
					</div>
					<div class="border rounded mb-3">
						<ul id="main_subject_list" class="list-group list-group-flush" style="font-size:13px; height:120px; overflow:auto;">
							<?php for($i=0; $i < count($main_subject_data); $i++) {
								$main_subject_name = $main_subject_data[$i]['main_subject_name'];
								$main_subject_code = $main_subject_data[$i]['main_subject_code'];
								$is_activate = $main_subject_data[$i]['is_activate'];
							?>
							<li class="list-group-item border-bottom subject" style="padding:.2rem 1rem;" name="<?= $main_subject_name ?>">
								<div class="form-check form-switch d-flex justify-content-between align-items-center p-0" >
									<div class="flex-grow-1" >
										<label class="form-check-label w-100" for="" onclick="selected_subject(this)" code="<?= $main_subject_code ?>" style="cursor:pointer;"><?= $main_subject_name ?></label>
									</div>
									<?php if($is_activate == 'Y') { ?>
									<input class="form-check-input" type="checkbox" id="<?= $main_subject_code ?>" checked  onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;">
									<?php } else { ?>
										<input class="form-check-input" type="checkbox" id="<?= $main_subject_code ?>" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;">
									<?php } // --end of if ?>								
								</div>
							</li>
							<?php 
							} // --end of for
							?>
							
						</ul>
					</div>
					<?= form_close() ?>
				</div>

				<!-- 하위 주제 요소 -->
				<div class="col-sm-12 col-lg-6">
					<?= form_open_multipart('setting/subsubjectadd') ?>
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="선택한 메인 주제" id="main_subject_name" name="main_subject_name" value="<?= $selected_subject_name ?>" readonly>
						<input type="text" class="form-control" placeholder="하위 주제 입력" aria-label="sub subject" aria-describedby="sub_subject" id="sub_subject" name="sub_subject">
						<input type="hidden" id="main_subject_code" name="main_subject_code" value="<?= $selected_subject_code ?>" readonly>
						<?php echo form_error('sub_subject', '<div class="text-danger ml-2 ">', '</div>') ?>
						<input type="submit" class="btn btn-outline-secondary" value="추가">
					</div>
					<div class="border rounded" style="font-size:13px; height:120px; overflow:auto;">
						<ul id="sub_subject_list" class="list-group list-group-flush">
							<?php if(empty($sub_subject_data)) { ?>
								<span class="text" style="color:gray;">메인 주제를 선택해주세요.</span>
							<?php } else { 
							for($i=0; $i < count($sub_subject_data); $i++) {
								$sub_subject_name = $sub_subject_data[$i]['sub_subject_name'];
								$sub_subject_code = $sub_subject_data[$i]['sub_subject_code'];
								$is_activate = $sub_subject_data[$i]['is_activate'];
							?>
							<li class="list-group-item border-bottom subject" style="padding:.2rem 1rem;">
								<div class="form-check form-switch d-flex justify-content-between align-items-center p-0" >
									<div class="flex-grow-1" >
										<label class="form-check-label w-100" code="<?= $sub_subject_code ?>"><?= $sub_subject_name ?></label>
									</div>
									<?php if($is_activate == 'Y') { ?>
									<input class="form-check-input" type="checkbox" id="<?= $sub_subject_code ?>" checked  onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;">
									<?php } else { ?>
										<input class="form-check-input" type="checkbox" id="<?= $sub_subject_code ?>" onclick="subject_check(this)" style="width:3em; height:1.5em; cursor:pointer;">
									<?php } // --end of if ?>								
								</div>
							</li>
							<?php 
								} // --end of for
							} // --end of if
							?>
						</ul>
					</div>
					<?= form_close() ?>
				</div>		

			</div>
		</div>


		<hr style="background-color:grey;">
		
		<!-- 포스트 목록 관리 -->
		<div id="post_listing_setting">
			<div class="me-3 d-flex align-items-center">
				<p class="flex-glow-1 fs-6 fw-bold" >포스트 목록 관리</p>
			</div>
			<div class="row border-bottom border-3 py-1 mx-0">
				<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">적용</div>
				<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">페이지 당 포스트 개수</div>
				<div class="col align-middle text-center fw-bolder" style="font-size:.75rem;">페이지 아이템 표시 개수</div>
			</div>
			<div class="row py-1 border-bottom mx-0" style="background-color:#F2F2F2;">
				<div class="col align-middle text-center" >
					<button type="button" class="btn btn-sm btn-outline-primary" onclick="post_listing_setting()" >	
						<span title="적용"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
							<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
							<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
						</svg></span>
					</button>
				</div>
				<div class="col align-middle text-center d-flex justify-content-center"><input type="number" class="form-control py-0 account-info text-center" min='1' id="posts_per_page" name="posts_per_page" value="<?= $config->posts_per_page ?>"></div>
				<div class="col align-middle text-center d-flex justify-content-center"><input type="number" class="form-control py-0 account-info text-center" min='1' id="page_item_count" name="page_item_count" value="<?= $config->page_item_count ?>"></div>
			</div>
		</div>

		<hr style="background-color:grey;">

	</div>
	<!-- --end of main content -->
					
<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/aside.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/views/template/footer.php');
?>
