<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Setting extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->model('Setting_model', 'setting');
      $this->load->helper(array('form', 'url','alert','text'));
      $this->load->library(array('form_validation', 'session', 'upload'));
      
    }
  /**
     * 클라이언트 웹페이지의 jQuery $.ajax() 함수를 위한 반환용 JSON 코드를 생성한다.
     *
     * @param int    $code    결과 코드(기본값: 0)
     * @param string $message 결과 메시지(기본값: null)
     * @param mixed  $extra   추가로 전달할 데이터(기본값: null)
     * @param string $debug   디버그 메시지(기본값: null)
     */
  public  function make_ajax_json($code = 0, $message = null, $extra = null, $debug = null) {
      return json_encode(
          array('code' => $code, 'message' => $message, 'extra' => $extra, 'debug' => $debug)
      );
  }

  //index 함수 설정은 "URL/index.php/main" 또는 "URL/index.php/main/index"로 접속가능하게 함
	public function index() {
    if($this->session->userdata('admin_id') != '') { // 세션 정보가 정상일 경우
      $data = array(
        'title'             => 'cheoleeblog setting',
        'images'            => $this->setting->get_all_title(),
        'main_subject_data' => $this->setting->get_all_main_subject(),
        'selected_subject_name' => '',
        'selected_subject_code' => '',
        'config'                => $this->setting->get_config()
      );
      // var_dump($data);
      $this->load->view('setting', $data);
    } else { // 세션 정보가 없는 경우 로그인 화면으로 이동한다.
			$this->session->set_flashdata('message', '        
        <script>
          $(function(){
            alert("세션이 종료되었습니다. 로그인 후 이용해 주세요.");
          });
        </script>
			');

			redirect('/main/index'); //로그인 화면으로 이동
		}
  }
  
  // 선택한 타이틀 이미지 파일을 서버에 업로드한다.
  Public function titleUpload() {
    $description = $this->input->post('description');

    // validate image
		if (empty($_FILES['new_image']['name'])) {
			$this->form_validation->set_rules('new_image', 'New_image', 'required');
		}

    // var_dump($description.'.'.$_FILES['new_image']['name']);
    $config['upload_path']		= 'D:\workspace\Servers\Apache24\htdocs\cheoleeblog_admin\img\title'; //path folder
    $config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
    $config['encrypt_name']		= TRUE;

    $this->upload->initialize($config);
    // if user upload new image
    if (!empty($_FILES['new_image']['name'])) {

      if ($this->upload->do_upload('new_image')) {
        $img = $this->upload->data();

        //Compress Image
        $this->_resizeImage($img['file_name']);

        $title_img = $img['file_name'];
        $last_used_date = date('Y-m-d H:i:s',time());
        $reg_date = date('Y-m-d H:i:s',time());

        // 업로드한 이미지의 정보를 DB에 넣어준다.
        $data = array(
            'title_img'       => $title_img,
            'description'     => $description,
            'used_count'      => 0,
            'is_activate'     => 'N',
            'last_used_date'  => $last_used_date,
            'reg_date'        => $reg_date
        );
        
        // insert data with model
        $this->setting->insert_image('blog_title_image', $data);

        $this->session->set_flashdata('message', '
          <script>
            $(function(){
              alert("타이틀 이미지가 업로드 되었습니다.");
            });
          </script>
        ');
					redirect('setting/index');
      
      } else {
        // if uploading image error
        // show error
        $this->session->set_flashdata('message', '
        <script>
          $(function(){
            alert("' . $this->upload->display_errors() . '");
          });
        </script>
      ');
      redirect('setting/index');

      }
    } else {
      // if user not uploading image
      $this->session->set_flashdata('message', '
        <script>
          $(function(){
            alert("타이틀 이미지 파일을 선택해 주세요.");
          });
        </script>
    ');
    redirect('setting/index');
    }
  }

  function _resizeImage($file_name)
	{
		// Image resizing config
		$config = array(
			'image_library' => 'GD2',
			'source_image'  => '/img/post/' . $file_name,
			'maintain_ratio' => FALSE,
			'width'         => 600,
			'height'        => 400,
			'new_image'     => '/img/post/resize/' . $file_name
		);

		// load config (built in liblary CI3)
		$this->load->library('image_lib', $config);

		$this->image_lib->initialize($config);
		if (!$this->image_lib->resize()) {
			return false;
		}
		$this->image_lib->clear();
	}

  // 선택한 타이틀 이미지의 데이터를 업데이트 한다.
  public function titleChange() {
    $title_file = $this->input->post('selected_title');
    
    // validate input           
		$this->form_validation->set_rules('selected_title', 'Selected_title', 'required');
    
    if ($this->form_validation->run() == FALSE) {
			$this->index();
    } else {
      $this->setting->update_title('blog_title_image',$title_file);

      $this->session->set_flashdata('message', '
        <script>
          $(function(){
            alert("타이틀 이미지가 변경되었습니다.");
          });
        </script>
					');
      redirect('setting/index');
    }
  
  }

  // 메인 주제를 추가 한다.
  public function mainSubjectAdd() {
    $main_subject = $this->input->post('main_subject');

    // validate input           
		$this->form_validation->set_rules('main_subject', 'Main_subject', 'required|is_unique[main_subject.main_subject_name]');

    if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
      $main_subject_data = $this->setting->get_all_main_subject();
      $reg_date = date('Y-m-d H:i:s', time());
      if(count($main_subject_data) == 0) {
        $data = array(
            'main_subject_code'   => 'main'.'-'.'0',
            'main_subject_name'   => $main_subject,
            'main_subject_order'  => 0,
            'is_activate'         => 'N',
            'reg_date'            => $reg_date

        );
      } else {
        $data = array(
          'main_subject_code'   => 'main'.'-'.count($main_subject_data),
          'main_subject_name'   => $main_subject,
          'main_subject_order'  => count($main_subject_data),
          'is_activate'         => 'N',
          'reg_date'            => $reg_date

        );
      }

      // 메인 주제를 db에 추가한다.
      $this->setting->insert_main_subject('main_subject',$data);

      $this->session->set_flashdata('message', '
			  <script>
          $(function(){
            alert("메인 주제가 추가되었습니다.");
          });
        </script>
     ');
			redirect('setting/index');
    }

  }

  // 메인 주제 및 하위 주제 활성화 여부를 실시간 반영한다.
  public function subjectCheck() {
    $main_subject_code = $this->input->post('main_subject_code');
    $subject_code = $this->input->post('subject_code');
    $is_activate = $this->input->post('is_activate');
    $subject_type = explode('-',$subject_code)[0];

    if($subject_type == 'main') {
      $this->setting->main_subject_activate_check('main_subject',$subject_code,$is_activate);
      // redirect('setting/index');
    } else {
      $where = array(
        'main_subject_code' => $main_subject_code,
        'sub_subject_code' => $subject_code
      );
      $this->setting->sub_subject_activate_check('sub_subject',$is_activate,$where);
      // $this->reload($main_subject_code);
    }

    // 메인 주제 코드 표기하기 위해 다시 클라이언트에 보낸다.
    $result_array = array(
      'main_subject_code' => $main_subject_code,
    );

    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
    
  }

  // 하위 주제 추가 및 활성화 여부 변경 시 페이지에 변경 데이터를 반영한다.
  public function reload($main_subject_code) {
    $selected_main_subject = $this->setting->get_selected_main_subject($main_subject_code);
    $main_subject_name = $selected_main_subject[0]['main_subject_name'];

    $data = array(
      'title'             => 'cheoleeblog setting',
      'images'            => $this->setting->get_all_title(),
      'main_subject_data' => $this->setting->get_all_main_subject(),
      'sub_subject_data'  => $this->setting->get_sub_subject($main_subject_code),
      'selected_subject_name' => $main_subject_name,
      'selected_subject_code' => $main_subject_code
      );
    
    // var_dump($data);
    $this->load->view('setting', $data);
    
  }

  // 하위 주제를 추가한다.
  public function subSubjectAdd() {
    $main_subject_code = $this->input->post('main_subject_code');
    $sub_subject = $this->input->post('sub_subject');

    // validate input           
		$this->form_validation->set_rules('main_subject_code', 'Main_subject_code', 'required');

    // 메인 주제 선택여부를 체크한다.
    if ($this->form_validation->run() == FALSE) {
      alert_back('메인 주제를 선택해주세요');
      redirect('setting/index');
    }
    // validate input
    $this->form_validation->set_rules('sub_subject', 'Sub_subject', 'required|is_unique[sub_subject.sub_subject_name]');

    if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
      $sub_subject_data = $this->setting->get_sub_subject($main_subject_code);
      $reg_date = date('Y-m-d H:i:s', time());
      if(count($sub_subject_data) == 0) {
        $data = array(
            'main_subject_code'   => $main_subject_code,
            'sub_subject_code'   => 'sub'.'-'.'0',
            'sub_subject_name'   => $sub_subject,
            'sub_subject_order'  => 0,
            'is_activate'         => 'N',
            'reg_date'            => $reg_date

        );
      } else {
        $data = array(
          'main_subject_code'   => $main_subject_code,
          'sub_subject_code'   => 'sub'.'-'.count($sub_subject_data),
          'sub_subject_name'   => $sub_subject,
          'sub_subject_order'  => count($sub_subject_data),
          'is_activate'         => 'N',
          'reg_date'            => $reg_date

        );
      }

      // 메인 주제를 db에 추가한다.
      $this->setting->insert_sub_subject('sub_subject',$data);

      $this->session->set_flashdata('message', '
        <script>
          $(function(){
            alert("하위 주제가 추가되었습니다.");
          });
        </script>
			');
			$this->reload($main_subject_code);
    }

  }

  // 선택한 메인 주제에 해당하는 하위 주제를 모두 가져온다
  public function getSubSubject() {

    $subject_code = $this->input->post('selected_subject');

    // 선택한 메인 주제의 이름을 가져온다.
    $selected_main_subject = $this->setting->get_selected_main_subject($subject_code);
    $main_subject_name = $selected_main_subject[0]['main_subject_name'];
    
    $sub_subject = $this->setting->get_sub_subject($subject_code);

    //extra 배열에 메인 주젱의 이름을 연관 배열로 요소를 추가한다.
    $sub_subject[] = array(
      'main_subject_name' => $main_subject_name
    );
    
    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $sub_subject, 'debug' => null));
    // try{
    // echo make_ajax_json(0, null, array('sub_subject' => $sub_subject), null);
    // } catch(Execption $e) {
    //   echo make_ajax_json(10,$e->getMessage().$subject_code);
    // }
  }

  // 포스트 목록 설정 내용을 적용한다.
  public function postListingSetting() {

    $posts_per_page = $this->input->post('posts_per_page');
    $page_item_count = $this->input->post('page_item_count');

    $data = array(
      'posts_per_page'  => $posts_per_page,
      'page_item_count' => $page_item_count
    );
    
    $this->setting->update_config('config',$data);
    // 설정 내용을 모델을 통해 반영한다.
    $new_config = $this->setting->get_config();
    
    $result_array = array(
      'posts_per_page'  =>  $new_config->posts_per_page,
      'page_item_count'  =>  $new_config->page_item_count
    );
    // echo json_encode(array('code' => 100, 'message' => '들어왔어', 'extra' => null, 'debug' => null)); exit;
    echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
  }

}
