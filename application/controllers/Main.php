<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Main extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->helper('url'); // redirect(), base_url() 사용하기 위함
      $this->load->helper('alert');  //alert 관련 함수를 사용하기 위함

    }

  // index 함수 설정은 "URL/index.php/main" 또는 "URL/index.php/main/index"로 접속가능하게 함
	public function index() {

    $data['title'] = 'cheoleeblog login';
    $this->load->view('login_form',$data);

  }

  // 입력 데이터 유효성 검증 및 로그인 시도
  public function login_validation() {
    $home = $_SERVER['HTTP_HOST']; // 사이트 도메인을 변수에 담아준다.
    $this->load->library('form_validation');
    
    $this->form_validation->set_rules('admin_id','아이디','required'); // 필드명, 이용자가 읽기쉬운 이름, 검사규칙
    $this->form_validation->set_rules('passwd','비밀번호','required');

    if($this->form_validation->run()){ //입력 데이터 유효성 검사 통과
      $admin_id = $this->input->post('admin_id', TRUE);
      $passwd = $this->input->post('passwd', TRUE);
      
      //model function
      $this->load->model('Main_model');
      if($this->Main_model->login($admin_id,$passwd)){ // 로그인 성공
        $session_data = array(
          'admin_id'=> $admin_id
        );
        $this->session->set_userdata($session_data);
        
        $last_login_date = date('Y-m-d H:i:s',time());
        $table = 'admin';
        $data = array(
          'last_login_date' => $last_login_date
        );
        
        // 마지막 로그인 일자를 업데이트 해준다.
        $this->Main_model->update_last_login_date($table,$data,$session_data);

        // 로그인 횟수를 업데이트 해준다.
        $this->Main_model->update_login_count($table,$session_data);

        alert_move('로그인 되었습니다.','/main/enter');
        // redirect('/main/enter'); //enter 함수로 이동
      } else { // 로그인 실패
        // $this->session->set_flashdata('error','로그인에 실패했습니다');
        
        alert_move('로그인 정보를 정확히 입력해주세요.','/main/index');
        
        redirect('/main/index');
      }

    } else { //입력 데이터 유효성 검사 실패
      // $this->session->set_flashdata('validation_error','아이디와 비밀번호를 입력해주세요');
      alert_move('아이디와 비밀번호를 입력해주세요.','/main/index');
      $this->index(); //로그인 화면으로 이동
    }
  }

  public function enter() { //로그인 성공시
    if($this->session->userdata('admin_id') != '') {
      
      $this->load->view('dashboard');
      
    } else {
      redirect('/main/index'); //로그인 화면으로 이동
    }
  }

  public function logout() {
    $this->session->unset_userdata('admin_id');
    alert_move('로그아웃 되었습니다.','/main/index');
    // redirect('/main/index'); //로그인 화면으로 이동
  }


}
