<?php defined('BASEPATH') OR exit('No direct script access allowed');

//컨트롤러의 파일생성은 기본적으로 주소창의 주소 확장입니다.
//간단하게 말해 Main.php를 만들고 class를 설정하였다면 "URL/index.php/main"으로 접속 가능합니다.
//내부 function도 주소 확장입니다. "URL/index.php/main"로 접속하였다면 function index()가 기본적으로 실행됩니다.
//내부에 public function good() 함수를 추가하였다면 "URL/index.php/main/good"으로 실행됩니다.

class Postmanage extends CI_Controller {

	function __construct() {       
      parent::__construct();
      $this->load->model('Postmanage_model', 'post');
      $this->load->helper(array('form', 'url','alert','text'));
      $this->load->library(array('form_validation', 'session', 'upload'));
	  
    }

	// 선택된 포스트의 내용을 표시한다.
  	public function detail($slug)
	{	
		// config 데이터를 가져온다.
		$config = $this->post->get_config();
		$page_item_count = $config->page_item_count; // 페이지 아이템 표시 개수

		$where = array(
			'is_activate' 	=> 	'Y'
		);

		// $slug에 해당하는 포스트 정보를 가져온다.
		$postDetail = $this->post->getDetailPost($slug);

		// 페이지 조회 기록 및 조회수를 업데이트 한다.
		$this->updatePageViewLog($postDetail);

		// 사이트 이동 경로 변수를 가져온다.
		$breadcrumb_main = $this->post->getDetailPost($slug); // 메인 주제 이름 파라미터값을 가져온다.
		if($breadcrumb_main != null) {
			$where['main_subject_name'] = $postDetail['main_subject_name'];
		}
		$breadcrumb_sub = $this->post->getDetailPost($slug); // 하위 주제 이름 파라미터값을 가져온다.
		if($breadcrumb_sub != null) {
			$where['sub_subject_name'] = $postDetail['sub_subject_name'];
		}
		
		$breadcrumb_foreword = $postDetail['foreword']; // 머리말 파라미터값을 가져온다.
		
		$filter = urldecode($this->input->post_get('filter'));
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = urldecode($this->input->post_get('search'));
		if($search == null) {
			$search = '';
		}
		
		// 메인 주제 및 하위 주제 데이터를 가져온다.
		$main_subject_data = $this->post->get_all_main_subject();
		$sub_subject_data = $this->post->get_all_sub_subject();
		
		$posts = $this->post->get_all_data($filter,$search,$where);
		$post_order = 0; // 해당 주제에서의 포스트 순서를 초기화 해준다.
		for($i=0; $i < count($posts); $i++) {
			$post_order += 1;
			$post_temp = $posts[$i];
			if($postDetail['slug'] == $post_temp['slug']) {
				if($i != 0){
					$pre_post_slug =  $posts[$i-1]['slug']; // 이전 포스트 slug를 변수에 담는다.
				}else {
					$pre_post_slug =  $posts[$i]['slug'];
				}
				if(($i+1) != count($posts) ){
					$next_post_slug = $posts[$i+1]['slug']; // 다음 포스트 slug를 변수에 담는다.
				} else {
					$next_post_slug = $posts[$i]['slug'];
				}
				break;
			}
		}

		// 해당 주제내의 포스트 페이지 위치를 구한다.
		// config 정보를 가져온다.
		$config = $this->post->get_config();
		$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수
		// $posts_per_page = 5; // 한 페이지에 보여줄 포스트의 수
		
		// 현재 포스트의 페이지 위치를 구한다.
		$cur_page = ceil($post_order/$posts_per_page);

		// $slug에 해당하는 포스트 정보를 가져온다.
		$postDetail = $this->post->getDetailPost($slug);
		
		$comments = $this->post->get_all_comment($slug);
		$comments_reply = $this->post->get_all_comment_reply_by_slug($slug);
    	// var_dump($slug);
		// var_dump($postDetail);
		$data = array(
			'config'				=> $config,
			'posts'					=> $posts,
			'images'				=> $this->db->get_where('blog_title_image',array('is_use' => 'Y'))->result_array(),
			'post_order'			=> $post_order, // 해당 주제에서의 포스트 순서
			'pre_post_slug'			=> $pre_post_slug, // 이전 포스트 슬러그
			'next_post_slug'		=> $next_post_slug,	// 다음 포스트 슬러그
			'page_item_count'		=> $page_item_count,
			'posts_per_page'		=> $posts_per_page,
			'main_subject_data'		=> $main_subject_data,
			'sub_subject_data'		=> $sub_subject_data,
			'comments'				=> $comments,
			'comment_limit'			=> 10,
			'comments_reply'		=> $comments_reply,
			'writer_id'				=> $postDetail['writer_id'],
			'main_subject_name' 	=> $postDetail['main_subject_name'],
			'sub_subject_name'  	=> $postDetail['sub_subject_name'],
			'foreword'    			=> $postDetail['foreword'],
			'breadcrumb_main'		=> $postDetail['main_subject_name'], // 포스트 위치 표시를 위한 변수
			'breadcrumb_sub'		=> $postDetail['sub_subject_name'], // 포스트 위치 표시를 위한 변수
			'breadcrumb_foreword'	=> $postDetail['foreword'], // 포스트 위치 표시를 위한 변수
			'title'					=> $postDetail['title'],
			'content' 				=> $postDetail['content'],
			'slug' 					=> $postDetail['slug'],
			'view_count' 			=> $postDetail['view_count'],
			'reg_date'				=> $postDetail['reg_date'],
			'thumbnail'				=> $postDetail['thumbnail'],
			'filter'				=> $filter,
			'search'				=> $search,
			'cur_page'				=> $cur_page,
			'comment_nickname'		=> '',
			'comment_passwd'		=> '',
			'post_comment'			=> '',
			'is_secret'				=> 'Y'
		);
		// var_dump($data);

		$this->load->view('post_detail', $data);

	}

	// 포스트의 view_count를 업데이트 한다.
	public function updateViewCount() {
		$slug = $this->input->post_get('slug');
		$ip_addr = $this->input->ip_address();

		if($ip_addr == '219.254.133.17' || $ip_addr == '192.168.0.164' || $ip_addr == '127.0.0.1' ){
			// do nothing.
			
		}else{
			$post_detail = $this->post->getDetailPost($slug);
			$view_count = $post_detail['view_count'];
			$new_view_count = $view_count + 1;
			$this->db->update('post_content',array('view_count' => $new_view_count),array('slug' => $slug));
		}
		$result_array = $this->post->getDetailPost($slug);

		// 업데이트 결과를 클라이언트로 보낸다.
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));

	}
	// 포스트 보기창에서 새로고침하면 포스트 보기 목록을 새로고침한다.
	public function detailList($slug)
	{	
		// config 데이터를 가져온다.
		$config = $this->post->get_config();
		$page_item_count = $config->page_item_count; // 페이지 아이템 표시 개수

		$where = array(
			'is_activate' 	=> 	'Y'
		);
		// $slug에 해당하는 포스트 정보를 가져온다.
		$postDetail = $this->post->getDetailPost($slug);

		// 페이지 조회 기록 및 조회수를 업데이트 한다.
		$this->updatePageViewLog($postDetail);

		// 사이트 이동 경로 변수를 가져온다.
		$breadcrumb_main = $this->post->getDetailPost($slug); // 메인 주제 이름 파라미터값을 가져온다.
		if($breadcrumb_main != null) {
			$where['main_subject_name'] = $postDetail['main_subject_name'];
		}
		$breadcrumb_sub = $this->post->getDetailPost($slug); // 하위 주제 이름 파라미터값을 가져온다.
		if($breadcrumb_sub != null) {
			$where['sub_subject_name'] = $postDetail['sub_subject_name'];
		}
		$breadcrumb_foreword = $postDetail['foreword']; // 머리말 파라미터값을 가져온다.
		
		$filter = urldecode($this->input->post_get('filter'));
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = urldecode($this->input->post_get('search'));
		if($search == null) {
			$search = '';
		}
		
		// 메인 주제 및 하위 주제 데이터를 가져온다.
		$main_subject_data = $this->post->get_all_main_subject();
		$sub_subject_data = $this->post->get_all_sub_subject();

		$posts = $this->post->get_all_data($filter,$search,$where);
		$post_order = 0; // 해당 주제에서의 포스트 순서를 초기화 해준다.
		for($i=0; $i < count($posts); $i++) {
			$post_order += 1;
			$post_temp = $posts[$i];
			if($postDetail['slug'] == $post_temp['slug']) {
				if($i != 0){
					$pre_post_slug =  $posts[$i-1]['slug']; // 이전 포스트 slug를 변수에 담는다.
				}else {
					$pre_post_slug =  $posts[$i]['slug'];
				}
				if(($i+1) != count($posts) ){
					$next_post_slug = $posts[$i+1]['slug']; // 다음 포스트 slug를 변수에 담는다.
				} else {
					$next_post_slug = $posts[$i]['slug'];
				}
				break;
			}
		}

		// 해당 주제내의 포스트 페이지 위치를 구한다.
		// config 정보를 가져온다.
		$config = $this->post->get_config();
		$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수
		// $posts_per_page = 5; // 한 페이지에 보여줄 포스트의 수
		
		// 현재 포스트의 페이지 위치를 구한다.
		$cur_page = ceil($post_order/$posts_per_page);

		// 업데이트된 포스트 정보를 가져온다.
		$postDetail = $this->post->getDetailPost($slug);
		
		$comments = $this->post->get_all_comment($slug);
		$comments_reply = $this->post->get_all_comment_reply_by_slug($slug);
    	// var_dump($slug);
		// var_dump($postDetail);
		$data = array(
			'config'				=> $config,
			'posts'					=> $posts,
			'images'				=> $this->db->get_where('blog_title_image',array('is_use' => 'Y'))->result_array(),
			'post_order'			=> $post_order, // 해당 주제에서의 포스트 순서
			'pre_post_slug'			=> $pre_post_slug, // 이전 포스트 슬러그
			'next_post_slug'		=> $next_post_slug,	// 다음 포스트 슬러그
			'page_item_count'		=> $page_item_count,
			'posts_per_page'		=> $posts_per_page,
			'main_subject_data'		=> $main_subject_data,
			'sub_subject_data'		=> $sub_subject_data,
			'comments'				=> $comments,
			'comment_limit'			=> 10,
			'comments_reply'		=> $comments_reply,
			'writer_id'				=> $postDetail['writer_id'],
			'main_subject_name' 	=> $postDetail['main_subject_name'],
			'sub_subject_name'  	=> $postDetail['sub_subject_name'],
			'foreword'    			=> $postDetail['foreword'],
			'breadcrumb_main'		=> $postDetail['main_subject_name'], // 포스트 위치 표시를 위한 변수
			'breadcrumb_sub'		=> $postDetail['sub_subject_name'], // 포스트 위치 표시를 위한 변수
			'breadcrumb_foreword'	=> $postDetail['foreword'], // 포스트 위치 표시를 위한 변수
			'title'					=> $postDetail['title'],
			'content' 				=> $postDetail['content'],
			'slug' 					=> $postDetail['slug'],
			'view_count' 			=> $postDetail['view_count'],
			'reg_date'				=> $postDetail['reg_date'],
			'thumbnail'				=> $postDetail['thumbnail'],
			'filter'				=> $filter,
			'search'				=> $search,
			'cur_page'				=> $cur_page,
			'comment_nickname'		=> '',
			'comment_passwd'		=> '',
			'post_comment'			=> '',
			'is_secret'				=> 'Y'
		);

		$this->load->view('/template/header', $data);
		$this->load->view('/template/nav_head_sidebar_details', $data);
		$this->load->view('post_detail', $data);
		$this->load->view('/template/aside', $data);
		$this->load->view('/template/footer', $data);

	}
	// 포스트 전체 목록을 표시한다.
	public function index()
	{	
		// if($this->session->userdata('admin_id') != '') { // 세션 정보가 정상일 경우
			// 포스트 데이터 검색조건 배열을 초기화 한다.
			$where = array(
				'is_activate' 	=> 	'Y'
			);

			// 페이지 조회 기록을 업데이트 한다.
			$ip_addr = $this->input->ip_address();
			$reg_date = date('Y-m-d H:i:s',time());


			// 클라이언트 ip의 접속 로그 데이터를 가져온다.
			$view_check_data = $this->db->get_where('page_view_log',array('ip_addr' => $ip_addr))->result_array();

			if($view_check_data){ // 접속 로그 데이터가 있을 경우
				// 마지막 로그 데이터를 선택한다.
				$view_check_data_last = $view_check_data[count($view_check_data)-1];
			} else {
				$view_check_data_last = '';
			}
			
			if($view_check_data_last){ // 마지막 로그 데이터가 있을 경우
				// 시작 시간을 조회 기준 시간으로 설정한다.
				$start_time = $view_check_data_last['flag_view_date'];
				$flag_view_date = $view_check_data_last['flag_view_date'];
				$user_id = $view_check_data_last['user_id'];
				$last_ip_addr = $view_check_data_last['ip_addr'];
			} else { // 마지막 로그 데이터가 없는 경우
				// 시작 시간을 현재 시간으로 설정한다. 
				$start_time = $reg_date;
				$flag_view_date = $reg_date;
				$user_id = 'guest';
				$last_ip_addr = $ip_addr;
			}

			$end_time = $reg_date;

			try {
				$start = new DateTime($start_time);
				$end = new DateTime($end_time);
			} catch (Exception $e){
				echo $e->getMessage();
			}
			
			$interval = $start->diff($end); //기간의 시간 차이를 계산
			
			// $diff_day = $interval->day;  //기간 차이를 일수로 환산
			// $diff_month = ($interval->format('%y') * 12) + $interval->format('%m');  //기간 차이를 개월수로 환산
			$diff_time = ($interval->format('%a')*24) + $interval->format('%h');  //기간 차이를 시간으로 환산
			
			// var_dump($diff_time);

			if($diff_time > 8) {
				$flag_view_date = $reg_date;
			}

			// 페이지 조회 기록 데이터
			$log_data = array(
				'page_path' 		=> $_SERVER['PHP_SELF'],
				'user_id'			=> $user_id,
				'ip_addr'			=> $ip_addr,
				'flag_view_date'	=> $flag_view_date,
				'reg_date'			=> $reg_date
			);

			// 페이지 조회 로그 데이터를 DB에 추가한다.
			if($ip_addr != '219.254.133.17' && $ip_addr != '192.168.0.164' && $ip_addr != '127.0.0.1' ){
				$this->db->insert('page_view_log',$log_data);
			}

			// 사이트 이동 경로 변수를 가져온다.
			$breadcrumb_main = $this->input->get_post('main'); // 메인 주제 이름 파라미터값을 가져온다.
			if($breadcrumb_main != null) {
				$where['main_subject_name'] = $breadcrumb_main; 
			}
			$breadcrumb_sub = $this->input->get_post('sub'); // 하위 주제 이름 파라미터값을 가져온다.
			if($breadcrumb_sub != null) {
				$where['sub_subject_name'] = $breadcrumb_sub; 
			}
			$breadcrumb_foreword = $this->input->get_post('foreword'); // 머리말 파라미터값을 가져온다.
			
			$filter = $this->input->post_get('filter');
			if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
				$filter = 'title';
			}
			$search = $this->input->post_get('search');
			if($search == null) {
				$search = '';
			}

			// var_dump($filter.','.$search);

			if(!empty($filter) && !empty($search)) {
				$all_post = $this->post->count_search_data($filter,$search,$where); // 검색어 기준 모든 검색 결과 포스트의 수
			} else {
				$all_post = $this->post->count_all_search_data($where); // 포스트 위치 기준 모든 검색 결과 포스트의 수
			}
			
			// config 정보를 가져온다.
			$config = $this->post->get_config();
			$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수
			$page_item_count = $config->page_item_count; // 페이지 아이템 표시 개수

			// $page_item_count = 5; // 페이지 아이템 표시 개수
			// $posts_per_page = 5; // 한 페이지에 보여줄 포스트의 수
			$all_page = ceil($all_post/$posts_per_page); // 모든 페이지의 수
			// var_dump($slug);
			// var_dump($postDetail);
			$cur_page = $this->input->get('page');

			if($cur_page == null) {
				$cur_page = 1;
			}

			// 메인 주제 및 하위 주제 데이터를 가져온다.
			$main_subject_data = $this->post->get_all_main_subject();
			$sub_subject_data = $this->post->get_all_sub_subject();
			
			// 가져올 데이터의 시작 번호(index)
			$start_index = ($cur_page-1)*$posts_per_page;

			// var_dump($cur_page.','.$posts_per_page);
			
			$data = array(
				'all_post'				=> $all_post,
				'config'				=> $config,
				'posts'					=> $this->post->get_all_data($filter,$search,$where),
				'images'				=> $this->db->get_where('blog_title_image',array('is_use' => 'Y'))->result_array(),
				'main_subject_data'		=> $main_subject_data,
				'sub_subject_data'		=> $sub_subject_data,
				'all_page'				=> $all_page,
				'start_index'			=> $start_index,
				'posts_per_page'		=> $posts_per_page,
				'page_item_count'		=> $page_item_count,
				'breadcrumb_main'		=> $breadcrumb_main,
				'breadcrumb_sub'		=> $breadcrumb_sub,
				'breadcrumb_foreword'	=> $breadcrumb_foreword,
				'cur_page'				=> $cur_page,
				'filter'				=> $filter,
				'search'				=> $search
			);
			
			$this->session->set_flashdata('message', '
						<script>
							$(function(){
								$("#head_title").text("포스트 목록"); // 상단 타이틀을 변경해준다.
								$("#search_form").show(); // 검색요소를 보이게 한다.
								$("#search_div").show(); // 검색요소를 보이게 한다.
							});
						</script>
					');
			// var_dump($data);
			$this->load->view('/template/header', $data);
			$this->load->view('/template/nav_head_sidebar_posts', $data);
			$this->load->view('post_list', $data);
			$this->load->view('/template/aside', $data);
			$this->load->view('/template/footer', $data);

		// } else { // 세션 정보가 없는 경우 로그인 화면으로 이동한다.
		// 	$this->session->set_flashdata('message', '        
		// 		<script>
		// 			$(function(){
		// 				alert("세션이 종료되었습니다. 로그인 후 이용해 주세요.");
		// 			});			
		// 		</script>
		// 	');

		// 	redirect('/main/index'); //로그인 화면으로 이동
		// }
	}

	// 포스트 전체 목록을 표시한다.
	public function postList()
	{	
		// if($this->session->userdata('admin_id') != '') { // 세션 정보가 정상일 경우
			// 포스트 데이터 검색조건 배열을 초기화 한다.
			$where = array(
				'is_activate' 	=> 	'Y'
			);

			// 페이지 조회 기록을 업데이트 한다.
			$ip_addr = $this->input->ip_address();
			$reg_date = date('Y-m-d H:i:s',time());


			// 클라이언트 ip의 접속 로그 데이터를 가져온다.
			$view_check_data = $this->db->get_where('page_view_log',array('ip_addr' => $ip_addr))->result_array();

			if($view_check_data){ // 접속 로그 데이터가 있을 경우
				// 마지막 로그 데이터를 선택한다.
				$view_check_data_last = $view_check_data[count($view_check_data)-1];
			} else {
				$view_check_data_last = '';
			}
			
			if($view_check_data_last){ // 마지막 로그 데이터가 있을 경우
				// 시작 시간을 조회 기준 시간으로 설정한다.
				$start_time = $view_check_data_last['flag_view_date'];
				$flag_view_date = $view_check_data_last['flag_view_date'];
				$user_id = $view_check_data_last['user_id'];
				$last_ip_addr = $view_check_data_last['ip_addr'];
			} else { // 마지막 로그 데이터가 없는 경우
				// 시작 시간을 현재 시간으로 설정한다. 
				$start_time = $reg_date;
				$flag_view_date = $reg_date;
				$user_id = 'guest';
				$last_ip_addr = $ip_addr;
			}

			$end_time = $reg_date;

			try {
				$start = new DateTime($start_time);
				$end = new DateTime($end_time);
			} catch (Exception $e){
				echo $e->getMessage();
			}
			
			$interval = $start->diff($end); //기간의 시간 차이를 계산
			
			// $diff_day = $interval->day;  //기간 차이를 일수로 환산
			// $diff_month = ($interval->format('%y') * 12) + $interval->format('%m');  //기간 차이를 개월수로 환산
			$diff_time = ($interval->format('%a')*24) + $interval->format('%h');  //기간 차이를 시간으로 환산
			
			// var_dump($diff_time);

			if($diff_time > 8) {
				$flag_view_date = $reg_date;
			}

			// 페이지 조회 기록 데이터
			$log_data = array(
				'page_path' 		=> $_SERVER['PHP_SELF'],
				'user_id'			=> $user_id,
				'ip_addr'			=> $ip_addr,
				'flag_view_date'	=> $flag_view_date,
				'reg_date'			=> $reg_date
			);

			// 페이지 조회 로그 데이터를 DB에 추가한다.
			if($ip_addr != '219.254.133.17' && $ip_addr != '192.168.0.164' && $ip_addr != '127.0.0.1' ){
				$this->db->insert('page_view_log',$log_data);
			}
			
			// 사이트 이동 경로 변수를 가져온다.
			$breadcrumb_main = urldecode($this->input->get_post('main')); // 메인 주제 이름 파라미터값을 가져온다.
			if($breadcrumb_main != null) {
				$where['main_subject_name'] = $breadcrumb_main; 
			}
			$breadcrumb_sub = urldecode($this->input->get_post('sub')); // 하위 주제 이름 파라미터값을 가져온다.
			if($breadcrumb_sub != null) {
				$where['sub_subject_name'] = $breadcrumb_sub; 
			}
			$breadcrumb_foreword = urldecode($this->input->get_post('foreword')); // 머리말 파라미터값을 가져온다.
			
			$filter = urldecode($this->input->post_get('filter'));
			if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
				$filter = 'title';
			}
			$search = urldecode($this->input->post_get('search'));
			if($search == null) {
				$search = '';
			}

			// var_dump($filter.','.$search);
			
			if(!empty($filter) && !empty($search)) {
				$all_post = $this->post->count_search_data($filter,$search,$where); // 검색어 기준 모든 검색 결과 포스트의 수
			} else {
				$all_post = $this->post->count_all_search_data($where); // 포스트 위치 기준 모든 검색 결과 포스트의 수
			}
			
			// config 정보를 가져온다.
			$config = $this->post->get_config();
			$posts_per_page = $config->posts_per_page; // 페이지 당 포스트 개수
			$page_item_count = $config->page_item_count; // 페이지 아이템 표시 개수

			// $page_item_count = 5; // 페이지 아이템 표시 개수
			// $posts_per_page = 5; // 한 페이지에 보여줄 포스트의 수
			$all_page = ceil($all_post/$posts_per_page); // 모든 페이지의 수
			// var_dump($slug);
			// var_dump($postDetail);
			$cur_page = urldecode($this->input->get('page'));

			if($cur_page == null) {
				$cur_page = 1;
			}

			// 메인 주제 및 하위 주제 데이터를 가져온다.
			$main_subject_data = $this->post->get_all_main_subject();
			$sub_subject_data = $this->post->get_all_sub_subject();
			
			// 가져올 데이터의 시작 번호(index)
			$start_index = ($cur_page-1)*$posts_per_page;

			// var_dump($cur_page.','.$posts_per_page);
			
			$data = array(
				'all_post'				=> $all_post,
				'posts'					=> $this->post->get_all_data($filter,$search,$where),
				'images'				=> $this->db->get_where('blog_title_image',array('is_use' => 'Y'))->result_array(),
				'main_subject_data'		=> $main_subject_data,
				'sub_subject_data'		=> $sub_subject_data,
				'all_page'				=> $all_page,
				'start_index'			=> $start_index,
				'posts_per_page'		=> $posts_per_page,
				'page_item_count'		=> $page_item_count,
				'breadcrumb_main'		=> $breadcrumb_main,
				'breadcrumb_sub'		=> $breadcrumb_sub,
				'breadcrumb_foreword'	=> $breadcrumb_foreword,
				'cur_page'				=> $cur_page,
				'filter'				=> $filter,
				'search'				=> $search
			);
			// var_dump($data);
			$this->session->set_flashdata('message', '
						<script>
							$(function(){
								$("#head_title").text("포스트 목록"); // 상단 타이틀을 변경해준다.
								$("#search_form").show(); // 검색요소를 보이게 한다.
								$("#search_div").show(); // 검색요소를 보이게 한다.
							});
						</script>
					');

			$this->load->view('post_list', $data);
		// } else { // 세션 정보가 없는 경우 로그인 화면으로 이동한다.
		// 	$this->session->set_flashdata('message', '        
		// 		<script>
		// 			$(function(){
		// 				alert("세션이 종료되었습니다. 로그인 후 이용해 주세요.");
		// 			});			
		// 		</script>
		// 	');

		// 	redirect('/main/index'); //로그인 화면으로 이동
		// }
	}

	function _resizeImage($file_name)
	{
		// Image resizing config
		$config = array(
			'image_library' => 'GD2',
			'source_image'  => '/img/post/' . $file_name,
			'maintain_ratio' => FALSE,
			'width'         => 600,
			'height'        => 400,
			'new_image'     => '/img/post/resize/' . $file_name
		);

		// load config (built in liblary CI3)
		$this->load->library('image_lib', $config);

		$this->image_lib->initialize($config);
		if (!$this->image_lib->resize()) {
			return false;
		}
		$this->image_lib->clear();
			
	}

	// 선택한 포스트를 편집한다.
	public function edit($slug)
	{
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}
		// 메인 주제 및 하위 주제 데이터를 가져온다.
		$main_subject_data = $this->post->get_all_main_subject();
		$sub_subject_data = $this->post->get_all_sub_subject();
		// get data from model
		$postDetail = $this->post->getDetailPost($slug);

		$data = array(
			'main_subject_data' => $main_subject_data,
			'sub_subject_data'	=> $sub_subject_data,
			'main_subject_name' => $postDetail['main_subject_name'],
			'sub_subject_name'  => $postDetail['sub_subject_name'],
			'foreword'			=> $postDetail['foreword'],
			'title'				=> $postDetail['title'],
			'content' 			=> $postDetail['content'],
			'reg_date'			=> $postDetail['reg_date'],
			'idx'				=> $postDetail['idx'],
			'slug'				=> $postDetail['slug'],
			'thumbnail'			=> $postDetail['thumbnail'],
			'filter'			=> $filter,
			'search'			=> $search,
			'cur_page'			=> $cur_page
		);

		$this->load->view('post_edit', $data);
	}

	// 편집한 포스트 내용을 업데이트 한다.
	public function update()
	{
		// 파라미터 값을 가져온다.
		$filter = $this->input->post_get('filter');
		if($filter == null) { // 검색 필터가 없으면 'title'로 초기화 한다.
			$filter = 'title';
		}
		$search = $this->input->post_get('search');
		if($search == null) {
			$search = '';
		}
		$cur_page = $this->input->get('page');
		if($cur_page == null) {
			$cur_page = 1;
		}
		/////////////////////
		$main_subject_name = $this->input->post('main_subject_name');
		$sub_subject_name  = $this->input->post('sub_subject');
		$foreword    	   = $this->input->post('foreword');
		$title 			   = $this->input->post('title');
		$idx			   = $this->input->post('idx');
		$slug			   = $this->input->post('slug');
		$writer_id 		   = $this->input->post('writer_id');
		$contents 		   = $this->input->post('contents');
		$old_image		   = $this->input->post('old_image');
		$last_update_date  = date("Y-m-d H:i:s");

		$where = array('idx' => $idx);

		$no_upload_image = array(
			'main_subject_name'		=> $main_subject_name,
			'sub_subject_name'		=> $sub_subject_name,
			'foreword'				=> $foreword,
			'title'					=> $title,
			'content'				=> $contents,
			'last_update_date'		=> $last_update_date,
		);

		$this->form_validation->set_rules('main_subject', 'Main_subject', 'required');
		// $this->form_validation->set_rules('sub_subject', 'Sub_subject_name', 'required');
		$this->form_validation->set_rules('foreword', 'Foreword', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('contents', 'Contents', 'required');

		// validate image
		// 편집내용 업데이트 시에는 썸네일 유효성 검증을 하지 않는다.
		// if (empty($_FILES['thumbnail']['name'])) {
		// 	$this->form_validation->set_rules('thumbnail', 'Thumbnail', 'required');
		// }

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', '        
				<script>
				$(function(){
					alert("누락된 항목이 있습니다.");
				});            
				</script>
			');
			redirect('postmanage/edit/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
		} else {

			$config['upload_path']		= 'D:\workspace\Servers\Apache24\htdocs\cheoleeblog_admin\img\post'; //path folder
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg|bmp';
			$config['encrypt_name']		= TRUE;

			$this->upload->initialize($config);

			// if user upload new image
			if (!empty($_FILES['thumbnail']['name'])) {

				if ($this->upload->do_upload('thumbnail')) {
					$img = $this->upload->data();

					//Compress Image
					$this->_resizeImage($img['file_name']);

					$thumbnail = $img['file_name'];

					$data = array(
						'main_subject_name'	=> $main_subject_name,
						'sub_subject_name'	=> $sub_subject_name,
						'foreword'			=> $foreword,
						'title'				=> $title,
						'content'			=> $contents,
						'last_update_date'	=> $last_update_date,
						'thumbnail'   		=> $thumbnail,
					);

					// var_dump($data);

					$this->db->update('post_content', $data, $where);

					// remove old image thumbnail
					$filename = explode(".", $old_image)[0];
					array_map('unlink', glob(FCPATH . "/img/post/$filename.*"));
					// array_map('unlink', glob(FCPATH . "/img/post/resize/$filename.*"));

					$this->session->set_flashdata('message', '
						<script>
							$(function(){
								alert("포스트 내용이 수정되었습니다.");
							});
						</script>
					');
					redirect('postmanage/detail/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
				} else {
					// if upload image fail
					echo $this->upload->display_errors();
				}
			} else {
				var_dump($no_upload_image);
				// if user doesn't upload new image
				$this->db->update('post_content', $no_upload_image, $where);
				
				$this->session->set_flashdata('message', '
					<script>
						$(function(){
							alert("포스트 내용이 수정되었습니다.");
						});
					</script>
				');
				redirect('postmanage/detail/' . $slug.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
			}
		}
	}

	// 포스트 상태를 업데이트 한다.
	public function statusUpdate() {
		$slug = $this->input->post('slug');
		$status = $this->input->post('status');
	
		// 포스트 상태를 업데이트하고 업데이트된 포스트 정보를 가져온다.
		$this->post->status_update($slug,$status);
		
		$post_info = $this->post->getDetailPost($slug);
	 	
		// 계정 상태 확인을 위해 다시 클라이언트에 보낸다.
		$result_array = array(
		  'post_status' => $post_info['is_activate']
		);
	
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
	  }

	// 포스트를 삭제한다.
	public function delete($idx)
	{
		$this->post->deletePost($idx, 'post_content');

		$this->session->set_flashdata('message', '
			<script>
				$(function(){
					alert("포스트가 삭제되었습니다.");
				});
			</script>
		');
		redirect('postmanage/manage'.'?page='.$cur_page.'&filter='.$filter.'&search='.$search);
	}

	// 코멘트를 저장한다.
	public function saveComment() {
		$post_slug = $this->input->post('post_slug');
		$main_subject_name = $this->input->post('main_subject_name');
		$sub_subject_name = $this->input->post('sub_subject_name');
		$comment_nickname = $this->input->post('comment_nickname');
		$comment_passwd = $this->input->post('comment_passwd');
		$post_comment = $this->input->post('post_comment');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$is_secret = $this->input->post('is_secret');
		// echo $post_slug.','.$comment_nickname.','.$comment_passwd.',\\r'.$post_comment;
		// echo json_encode(array('code' => 200, 'message' => $post_slug.','.$comment_nickname.','.$comment_passwd.',\\r'.$post_comment, 'extra' => null, 'debug' => null));
		// 	exit;
		// validate input
		$this->form_validation->set_rules('post_slug', 'Post_slug', 'required');
		// $this->form_validation->set_rules('sub_subject', 'Sub_subject', 'required');
		// $this->form_validation->set_rules('main_subject_name', 'Main_subject_name', 'required');       
		$this->form_validation->set_rules('comment_nickname', 'Comment_nickname', 'required');
		$this->form_validation->set_rules('comment_passwd', 'Comment_passwd', 'required');
		$this->form_validation->set_rules('post_comment', 'Post_comment', 'required');

		
		if ($this->form_validation->run() == FALSE) {
			echo json_encode(array('code' => 100, 'message' => '누락된 항목이 있습니다.', 'extra' => null, 'debug' => null));
			exit;
			
		} else {
			
			$now = date("Y-m-d H:i:s",time());
			$data = array(
				'post_slug'				=> $post_slug,  //주소창에 붙는 포스트 url						
				'main_subject_name'		=> $main_subject_name,
				'sub_subject_name'		=> $sub_subject_name,
				'comment_nickname'		=> $comment_nickname,
				'post_comment'			=> $post_comment,
				'ip_addr' 				=> $ip_addr,
				'is_secret' 			=> $is_secret,
				'comment_passwd'		=> $comment_passwd,
				'reg_date'				=> $now,
				'is_del'				=> 'N',
				'last_update_date'		=> $now
			);
			// var_dump($data);

			// insert data with model
			$this->post->insert_comment('post_comment', $data);

			// 해당 포스트의 모든 코멘트 정보를 가져온다.
			$comments = $this->post->get_all_comment($post_slug);
			// 추가된 댓글 정보를 가져온다.
			$where = array('reg_date' => $now);
			$result = $this->post->get_comment($where);
			$result_array = $result[0];
			$result_array['count_all_count'] = count($comments); // 코멘트의 개수를 결과 배열에 넣어준다.
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;	
		}
	}

	// 코멘트의 댓글을 저장한다.
	public function saveCommentReply() {

		$org_idx = $this->input->post('org_idx');
		$post_slug = $this->input->post('post_slug');
		$comment_reply_nickname = $this->input->post('comment_reply_nickname');
		$comment_reply_passwd = $this->input->post('comment_reply_passwd');
		$post_comment_reply = $this->input->post('post_comment_reply');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$comment_reply_is_secret = $this->input->post('comment_reply_is_secret');
		// echo $post_slug.','.$comment_reply_nickname.','.$comment_reply_passwd.',\n'.$post_comment_reply;
		// echo json_encode(array('code' => 300, 'message' => $post_slug.','.$comment_reply_nickname.','.$comment_passwd.',\n'.$comment_reply_passwd, 'extra' => null, 'debug' => null));
		// exit;
		// validate input
		$this->form_validation->set_rules('org_idx', 'Org_idx', 'required');
		$this->form_validation->set_rules('post_slug', 'Post_slug', 'required');

		$this->form_validation->set_rules('comment_reply_nickname', 'Comment_reply_nickname', 'required');
		$this->form_validation->set_rules('comment_reply_passwd', 'Comment_reply_passwd', 'required');
		$this->form_validation->set_rules('post_comment_reply', 'Post_comment_reply', 'required');

		if ($this->form_validation->run() == FALSE) {
			echo json_encode(array('code' => 100, 'message' => '누락된 항목이 있습니다.', 'extra' => null, 'debug' => null));
			exit;
			
		} else {
			$now = date("Y-m-d H:i:s",time());
			$data = array(
				'org_idx'					=> $org_idx,  // 원본 댓글 idx
				'post_slug'					=> $post_slug, // 원본 댓글 해당 포스트 url 식별번호			
				'comment_reply_nickname'	=> $comment_reply_nickname,
				'post_comment_reply'		=> $post_comment_reply,
				'ip_addr' 					=> $ip_addr,
				'is_secret' 				=> $comment_reply_is_secret,
				'comment_reply_passwd'		=> $comment_reply_passwd,
				'reg_date'					=> $now,
				'is_del'					=> 'N',
				'last_update_date'			=> $now
			);
			// var_dump($data);

			// insert data with model
			$this->post->insert_comment_reply('post_comment_reply', $data);

			// 추가된 댓글 정보를 가져온다.
			$where = array('reg_date' => $now);
			$result = $this->post->get_comment_reply($where);
			$result_array = $result[0];
			// 대댓글 정보를 가져온다.
			$comment_reply_list = $this->post->get_all_comment_reply_by_idx($org_idx);
			//대댓글 개수를 댓글 정보 배열에 추가한다.
			$result_array['comment_reply_count'] = count($comment_reply_list);
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;	
		}
	}

	// 코멘트를 수정한다.
	function updateComment() {
		$idx = $this->input->post('idx');
		$post_comment = $this->input->post('new_comment');
		$comment_passwd = $this->input->post('new_comment_passwd');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$is_secret = $this->input->post('new_is_secret');
		$last_update_date = date('Y-m-d H:i:s',time());
		// echo json_encode(array('code' => 1, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
		// exit;
		// echo $comment_passwd;

		if (strlen((string)$comment_passwd) < 4) { // 비밀번호 자리수가 4개인지 확인한다.
			echo json_encode(array('code' => 1, 'message' => '비밀번호는 4자리 숫자로 입력해주세요.', 'extra' => null, 'debug' => null));
			exit;
			
		} else {
			
			$data = array(
				'post_comment'		=> $post_comment,
				'comment_passwd'	=> $comment_passwd,
				'ip_addr'			=> $ip_addr,
				'is_secret'			=> $is_secret,
				'last_update_date'	=> $last_update_date
			);
			// echo json_encode(array('code' => 2, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
			// exit;
			// 댓글 정보를 업데이트 한다.
			$this->db->update('post_comment',$data,array('idx' => $idx));

			// 업데이트된 댓글 정보를 가져온다.
			$where = array('idx' => $idx);
			$result = $this->post->get_comment($where);
			$result_array = $result[0];
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;
		}
	}

	// 코멘트의 댓글을 수정한다.
	function updateCommentReply() {
		$idx = $this->input->post('idx');
		$post_comment_reply = $this->input->post('new_comment_reply');
		$comment_reply_passwd = $this->input->post('new_comment_reply_passwd');
		$ip_addr = $this->input->ip_address(); // 사용자의 ip를 가져온다. 
		$is_secret = $this->input->post('new_reply_is_secret');
		$last_update_date = date('Y-m-d H:i:s',time());
		// echo json_encode(array('code' => 1, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
		// exit;
		// echo $comment_passwd;

		if (strlen((string)$comment_reply_passwd) < 4) { // 비밀번호 자리수가 4개인지 확인한다.
			echo json_encode(array('code' => 1, 'message' => '비밀번호는 4자리 숫자로 입력해주세요.', 'extra' => null, 'debug' => null));
			exit;
			
		} else {
			
			$data = array(
				'post_comment_reply'		=> $post_comment_reply,
				'comment_reply_passwd'		=> $comment_reply_passwd,
				'ip_addr'					=> $ip_addr,
				'is_secret'					=> $is_secret,
				'last_update_date'			=> $last_update_date
			);
			// echo json_encode(array('code' => 2, 'message' => '여긴가?', 'extra' => null, 'debug' => null)); 
			// exit;
			// 댓글 정보를 업데이트 한다.
			$this->db->update('post_comment_reply',$data,array('idx' => $idx));

			// 업데이트된 댓글 정보를 가져온다.
			$where = array('idx' => $idx);
			$result = $this->post->get_comment_reply($where);
			$result_array = $result[0];
			echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
			exit;
		}
	}

	// idx에 해당하는 코멘트 정보를 가져온다.
	function getComment() {
		$idx = $this->input->post('idx');
		
		// idx에 해당하는 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// idx에 해당하는 코멘트 정보를 가져온다.
	function getCommentReply() {
		$idx = $this->input->post('idx');
		
		// idx에 해당하는 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment_reply($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// idx에 해당하는 코멘트를 삭제 상태로 변경한다.
	function deleteComment() {
		$idx = $this->input->post('idx');

		$del_date = date('Y-m-d H:i:s',time());

		$data = array(
			'is_del'	=> 'Y',
			'del_date'	=> $del_date
		);

		// 댓글 정보를 업데이트 한다.
		$this->db->update('post_comment',$data,array('idx' => $idx));

		// 업데이트된 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// idx에 해당하는 코멘트를 삭제 상태로 변경한다.
	function deleteCommentReply() {
		$idx = $this->input->post('idx');

		$del_date = date('Y-m-d H:i:s',time());

		$data = array(
			'is_del'	=> 'Y',
			'del_date'	=> $del_date
		);

		// 댓글 정보를 업데이트 한다.
		$this->db->update('post_comment_reply',$data,array('idx' => $idx));

		// 업데이트된 댓글 정보를 가져온다.
		$where = array('idx' => $idx);
		$result = $this->post->get_comment_reply($where);
		$result_array = $result[0];
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null)); 
		exit;
	}

	// 댓글 리스트를 새로고침 한다.
	function commentList($slug) {
		$comments = $this->post->get_all_comment($slug);
		$comments_reply = $this->post->get_all_comment_reply_by_slug($slug);
		$data = array(
			'slug'				=> $slug, // 포스트 url 식별 번호
			'comments'			=> $comments, // 해당 포스트 댓글
			'comments_reply' 	=> $comments_reply // 해당 포스트 댓글에 대한 댓글
		);
		$this->load->view('comment_list',$data);
	}

	// 랭킹 포인트를 업데이트 한다.
	public function updateRankingPoint(){
		
		$slug = $this->input->post_get('slug');
		// echo json_encode(array('code' => 100, 'message' => $slug, 'extra' => null, 'debug' => null)); exit;
		$comments_count = $this->post->count_all_comment_by_slug($slug); // 해당 포스트의 모든 댓글 정보를 가져온다.
		$post = $this->post->getDetailPost($slug);
		$view_count = $post['view_count'];  // 조회수를 가져온다.
		$recommanded = $post['recommanded']; // 추천수를 가져온다.
		// 랭킹 포인트를 계산한다.
		$ranking_point = $view_count + ($comments_count*2) + ($recommanded*3);
	
		// 랭킹 포인트를 업데이트 한다.
		$this->db->update('post_content',array('ranking_point' => $ranking_point),array('slug' => $slug));
		
		$result_array = $this->post->getDetailPost($slug);
		
		echo json_encode(array('code' => 0, 'message' => null, 'extra' => $result_array, 'debug' => null));
	
	}

	// 페이지 조회 로그를 기록하고 조회수를 업데이트 한다.
	public function updatePageViewLog($postDetail){
		
		$ip_addr = $this->input->ip_address();
		$reg_date = date('Y-m-d H:i:s',time());
		
		// 현재 접속한 페이지의 path를 가져온다.
		$page_path = $_SERVER['PHP_SELF'];
		$page_path_arr = explode('/',$page_path);
		// var_dump($page_path_arr);
		// 현재 접속한 페이지의 path를 접속 로그 데이터의 path 형식과 맞춘다.
		if($page_path_arr[2] == 'detaillist') {
			$page_path = '/'.$page_path_arr[1].'/detail/'.$page_path_arr[3];
		}

		// 클라이언트 ip의 해당 페이지 접속 로그 데이터를 가져온다.
		$view_check_data = $this->db->get_where('page_view_log',array('ip_addr' => $ip_addr, 'page_path' => $page_path))->result_array();

		if($view_check_data){ // 접속 로그 데이터가 있을 경우
			// 마지막 로그 데이터를 선택한다.
			$view_check_data_last = $view_check_data[count($view_check_data)-1];
		} else {
			$view_check_data_last = '';
		}
		
		if($view_check_data_last){ // 마지막 로그 데이터가 있을 경우
			// 시작 시간을 조회 기준 시간으로 설정한다.
			$start_time = $view_check_data_last['flag_view_date'];
			$flag_view_date = $view_check_data_last['flag_view_date'];
			$user_id = $view_check_data_last['user_id'];
			$last_ip_addr = $view_check_data_last['ip_addr'];
		} else { // 마지막 로그 데이터가 없는 경우
			// 시작 시간을 현재 시간으로 설정한다. 
			$start_time = $reg_date;
			$flag_view_date = $reg_date;
			$user_id = 'guest';
			$last_ip_addr = $ip_addr;
		}

		$end_time = $reg_date;

		try {
			$start = new DateTime($start_time);
			$end = new DateTime($end_time);
		  } catch (Exception $e){
			echo $e->getMessage();
		  }
		  
		$interval = $start->diff($end); //기간의 시간 차이를 계산
		
		// $diff_day = $interval->day;  //기간 차이를 일수로 환산
		// $diff_month = ($interval->format('%y') * 12) + $interval->format('%m');  //기간 차이를 개월수로 환산
		$diff_time = ($interval->format('%a')*24) + $interval->format('%h');  //기간 차이를 시간으로 환산
		
		// var_dump($diff_time);

		if($diff_time > 8) {
			$flag_view_date = $reg_date;
		}

		// 페이지 조회 기록 데이터
		$log_data = array(
			'page_path' 		=> $page_path,
			'user_id'			=> $user_id,
			'ip_addr'			=> $ip_addr,
			'flag_view_date'	=> $flag_view_date,
			'reg_date'			=> $reg_date
		);

		// 특정 아이피 이외의 아이피에서 접속 시 페이지 조회 로그 데이터를 DB에 추가한다.
		if($ip_addr != '219.254.133.17' && $ip_addr != '192.168.0.164' && $ip_addr != '127.0.0.1'){
			$this->db->insert('page_view_log',$log_data);
		
			// 조회수 증가 조건
			// case 1. 동일 ip에서 해당 페이지에 처음 접속한 경우
			// case 2. 동일 ip에서 해당 페이지에 접속한지 8시간이 경과된 경우
			//---------------------------------------------------------
			// (case 1) 해당 ip의 해당 페이지 접속 로그 데이터가 없는 경우 조회수를 증가 시킨다.
			if(!$view_check_data){ 
				$new_view_count = $postDetail['view_count'] + 1;
				$this->db->update('post_content',array('view_count' => $new_view_count),array('slug' => $postDetail['slug']));	
			// (case 2) 해당 ip의 해당 페이지 접속 로그 데이터가 있고 로그 기록 기준 시간이 8시간 이전인 경우 조회수를 증가 시킨다.
			} else if($last_ip_addr = $ip_addr && $diff_time > 8) { 
				$new_view_count = $postDetail['view_count'] + 1;
				$this->db->update('post_content',array('view_count' => $new_view_count),array('slug' => $postDetail['slug']));	
			}
		}

	}
}
