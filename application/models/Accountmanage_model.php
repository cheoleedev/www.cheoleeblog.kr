<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Accountmanage_model extends CI_model {

	function __construct(){ 
		parent::__construct(); 
        //위에서 설정한 /application/config/database.php 파일에서 $db['cheolee'] 설정값을 불러오겠다는 뜻입니다.
        $this->db = $this->load->database('db', TRUE);
	} 
    
    // 모든 계정 정보를 가져온다. (연관 배열)
    public function get_all_account()
    {
        // "SELECT * FROM admin ORDER BY 'idx' ASC"; ( DESK =  오름차순 정렬 ) 
        $this->db->order_by('idx', 'ASC'); //내림차순 정렬
        return $this->db->get('admin')->result_array();
    }

    // 계정을 추가한다.
    public function add_account($table, $data)
    {
       $this->db->insert($table, $data);
    }

    // 프로필 이미지 정보를 추가한다.
    public function insert_profile_image($table, $data)
    {
       $this->db->insert($table, $data);
    }

    // 계정 정보를 업데이트 한다.
    public function update_account_info($table,$data,$admin_id){
        $this->db->update($table,$data,array('admin_id' => $admin_id));
        
    }

    // 계정 상태를 업데이트 한다.
    public function status_update($admin_id,$status)
    {
        $this->db->update('admin',array('admin_status' => $status),array('admin_id' => $admin_id));
    }

    // admin_id에 해당하는 계정 정보를 가져온다.
    public function get_detail_account($admin_id)
    {
        return $this->db->get_where('admin', array('admin_id' => $admin_id))->result_array();
    }

    // 닉네임의 중복개수를 확인한다. 
    public function count_nickname($table,$nickname) 
    {
        return $this->db->get_where('admin', array('nickname' => $nickname))->num_rows();
    }

    // FUNCTION HAPUS
    public function delete_account($admin_id)
    {
        $this->delete_profile_file($admin_id);
        return $this->db->delete('admin', $admin_id);
    }

    // id에 해당하는 관리자(이용자) 데이터를 가져온다.
    public function getById($admin_id)
    {
        return $this->db->get_where('admin', array('admin_id' => $admin_id))->row();
    }
    
    // 프로필 이미지 테이블의 이전 프로필 이미지 활성화 여부를 'N'으로 업데이트 한다.
    public function update_profile_image_status($user_id,$profile_image)
    {
        $this->db->update('user_profile_image',array('is_activate' => 'N'),array('user_id' => $user_id, 'profile_image' => $profile_image));
        return $this->db->get_where('user_profile_image',array('user_id' => $user_id, 'profile_image' => $profile_image))->row();
    }    
    
    // 프로필 이미지 파일을 삭제한다.
    public function  delete_profile_file($user_id)
    {
        $profiles = $this->getById($user_id);
        $profile_image = $profiles->profile_image;
        // 프로필 이미지 정보를 업데이트 한다.
        $this->update_profile_image_status($user_id, $profile_image);
        
        $filename = explode(".", $profile_image)[0];
        if(!empty($filename)){ // 프로필 이미지 정보가 있는 경우만 삭제를 진행한다.
            // delte old thumbnail
            $delImg = array_map('unlink', glob(FCPATH . "img/profile_image/$filename.*"));
            // $delImg = array_map('unlink', glob(FCPATH . "img/profile_image/resize/$filename.*"));
        }
        
        return $delImg;
    }
}