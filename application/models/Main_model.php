<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_model {

	function __construct(){ 
		parent::__construct(); 
        //위에서 설정한 /application/config/database.php 파일에서 $db['cheolee'] 설정값을 불러오겠다는 뜻입니다.
      	$this->db = $this->load->database('db', TRUE);
	} 
    
    function login($admin_id,$admin_pw) {
        // var_dump (PDO :: getAvailableDrivers ());
        //query 함수는 DB에 쿼리를 요청하는 것이며, 이 과정에서 오류가 뜨면 오류메시지가 나옵니다.
      
        $query = "SELECT * FROM admin WHERE admin_id = ? AND passwd = ?";
        $stmt = $this->db->query($query, array($admin_id,$admin_pw));
        // $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        // 쿼리 결과를 연관배열 형태로 화면에 뿌려준다.
        //var_dump($stmt->result());

        // 쿼리 결과를 연관배열로 가져온다.
        // $result = $stmt->result_array();
        $result = $stmt->num_rows();

        if($result > 0) {
            return true;
        } else {
            return false;
        }

    }
    // 마지막 로그인 일자를 업데이트 해준다.
    function update_last_login_date($table,$data,$session_data) {

        $this->db->update($table,$data,$session_data);
        // "UPDATE $table SET 'last_login_date' = $last_login_date WHERE 'admin_id' = $admin_id" 쿼리 실행과 동일

    }

    // 로그인 횟수를 업데이트 해준다.
    function update_login_count($table,$session_data) {
        $result = $this->db->get_where($table,$session_data)->result_array();
        $login_count = $result[0]['login_count'] + 1;

        $this->db->update($table,array('login_count' => $login_count),$session_data);
    }

}